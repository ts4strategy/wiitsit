/* ----------------------------------------------------------------------------
 * AB InBev :: Temporary class - this class will be removed
 * ----------------------------------------------------------------------------
 * Clase: TRIBE_EnabledDisabledUser.apxc
 * Version: 1.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   				Description
 * 05/02/2019     	carlos.alvarezs@gmodelo.com.mx        	Class to assign profile id for global team, this class will be removed.
 */
global class TRIBE_EnabledDisabledUser implements Schedulable {
    
	global void execute(SchedulableContext SC) {
        
        Id idReadOnly = '00e6A000001jiDOQAY';
        Id idTelesales = '00ec0000000MS4nAAG';
        
        Integer hour = System.now().hour();
        Id idProfile = hour > 21 ? idTelesales : idReadOnly;
        
        List<User> listUsers = [SELECT Id, ProfileId FROM User WHERE Id IN : new Set<Id>{'005c0000005QCc5AAG', '005c0000005QCc0AAG', '005c0000005QCbgAAG', '005c0000005QHnhAAG'}];
        if ( listUsers != null && listUsers.size() > 0 ) {
            for ( User usr: listUsers ) {
            	usr.ProfileId = idProfile;
            }
            update listUsers;
        }
    }
}