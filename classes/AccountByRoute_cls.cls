/****************************************************************************************************
    General Information
    -------------------
    author: Joseph Ceron
    email: jceron@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger Class for validate account choosen have

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       05-07-2017        Joseph Ceron (JC)            Creation Class
    1.1       05-09-2017        Daniel Peñaloza              Methods to add or remove users to Account Team
    1.2       19-04-2018        Rodrigo Resendiz             Add methods to set BDR as the Owner of Accounts W-010925
    1.3       30-04-2018        Rodrigo Resendiz             When route is for Telesales add to account team
****************************************************************************************************/
public class AccountByRoute_cls {

    private static final AccountRouteSettings__mdt accountSettings = DevUtils_cls.getAccountRouteSettings();
    private static final AccountRouteSettings__mdt accountSettingsBDR = DevUtils_cls.getAccountRouteSettings('BDR');//1.2
    private static final AccountRouteSettings__mdt accountSettingsTeleSales = DevUtils_cls.getAccountRouteSettings('Telesales');//1.3
    private static final String[] lstRouteServiceModels = accountSettings.ServiceModelsToFilter__c.split(',');
    private static final String[] lstTelesaRouteServiceModels = accountSettingsTeleSales.ServiceModelsToFilter__c.split(',');
    private static final String SUPERVISOR_ROLE = accountSettings.SupervisorTeamMemberRole__c;
    private static final String SALESAGENT_ROLE = accountSettings.SalesAgentTeamMemberRole__c;
    private static final String SALESAGENT_ROLE_TELESALES = accountSettingsTeleSales.SalesAgentTeamMemberRole__c;// 1.3

    /**
     * Filtar Cuentas con Modelo de servicio para Presales y Autosales
     * @param  lstAccounts Lista de cuentas a filtrar
     * @return             Lista de cuentas filtradas
     */
    public static AccountByRoute__c[] filterAccounts(AccountByRoute__c[] lstAccounts) {
        Set<String> setModelsToFilter = new Set<String>(accountSettings.ServiceModelsToFilter__c.split(','));
        AccountByRoute__c[] lstFilteredAccounts = (AccountByRoute__c[]) DevUtils_cls.filterSObjectList(lstAccounts, 'ServiceModel__c', setModelsToFilter);

        return lstFilteredAccounts;
    }

    /**
       @method Filter Accounts with service model set as BDR
     * @param  lstAccounts Lista de cuentas a filtrar
     * @return             Lista de cuentas filtradas
     * Rodrigo Resendiz (1.2)
     */
    public static AccountByRoute__c[] filterAccountsBDR(AccountByRoute__c[] lstAccounts) {
        Set<String> setModelsToFilter = new Set<String>(accountSettingsBDR.ServiceModelsToFilter__c.split(','));
        AccountByRoute__c[] lstFilteredAccounts = (AccountByRoute__c[]) DevUtils_cls.filterSObjectList(lstAccounts, 'ServiceModel__c', setModelsToFilter);

        return lstFilteredAccounts;
    }

    /**
    @method filterAccountsByChange: Spots differences between trigger.new and trigger.old lists regarding specific fields
    * @param newLstAccounts
    * @param oldLstAccounts
    * @param fieldsToCompare
    * @return AccountByRoute__c[] oldFilteredAccounts values of trigger.Old that have diff
    * Rodrigo Resendiz (1.2)
    */
    public static AccountByRoute__c[] filterAccountsByChange(AccountByRoute__c[] newLstAccounts, Map<Id,AccountByRoute__c> oldMapAccounts, String[] fieldsToCompare) {
        AccountByRoute__c[] lstFilteredAccounts = (AccountByRoute__c[]) DevUtils_cls.diffSObjectList(newLstAccounts, oldMapAccounts, fieldsToCompare);
        AccountByRoute__c[] oldFilteredAccounts = new List<AccountByRoute__c>();
        for(AccountByRoute__c accByRt: lstFilteredAccounts){
            oldFilteredAccounts.add(oldMapAccounts.get(accByRt.Id));
        }
        return oldFilteredAccounts;
    }

    /**
     * Add or Replace Supervisor and Sales Agent to Account Team
     * @param      lstAccountsByRoute  The list accounts by route
     */
    public static void addUsersToAccountTeam(AccountByRoute__c[] lstAccountsByRoute) {
        Set<Id> setRouteIds = new Set<Id>();
        Map<Id, AccountWithTeamWrapper> mapAccountWrappers = new Map<Id, AccountWithTeamWrapper>();

        for (AccountByRoute__c accByRoute: lstAccountsByRoute) {
            setRouteIds.add(accByRoute.Route__c);

            if (!mapAccountWrappers.containsKey(accByRoute.Account__c)) {
                AccountWithTeamWrapper accWrapper = new AccountWithTeamWrapper();
                accWrapper.routeId = accByRoute.Route__c;
                mapAccountWrappers.put(accByRoute.Account__c, accWrapper);
            }
        }
        System.debug('***mapAccountWrappers : '+mapAccountWrappers);
         System.debug('***setRouteIds : '+setRouteIds);

        // Get Routes and Account Team Members information for Accounts by Route
        Map<Id, ONTAP__Route__c> mapRoutes = new Map<Id, ONTAP__Route__c>([
            SELECT Id, RouteManager__c, Supervisor__c, ServiceModel__c
            FROM ONTAP__Route__c
            WHERE Id IN :setRouteIds
                AND ServiceModel__c IN :lstRouteServiceModels
        ]);

        System.debug('*** lstRouteServiceModels: ' + lstRouteServiceModels);
        System.debug('*** mapRoutes: ' + mapRoutes);

        AccountTeamMember[] lstTeamMembers = [ SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember  WHERE  AccountId IN :mapAccountWrappers.keySet() and   (TeamMemberRole =: SUPERVISOR_ROLE OR TeamMemberRole =: SALESAGENT_ROLE )];
		System.debug('**lstTeamMembers : '+lstTeamMembers + ' TAMAÑO : '+lstTeamMembers);

        // Iterate account wrappers and insert AccountTeamMembers
        AccountTeamMember[] lstMembersToInsert = new AccountTeamMember[]{};

        for (Id accId: mapAccountWrappers.keySet()) {
            // Add Users to Wrapper
            AccountWithTeamWrapper accWrapper = mapAccountWrappers.get(accId);

            if (!mapRoutes.containsKey(accWrapper.routeId)) {
                continue;
            }

            ONTAP__Route__c objRoute = mapRoutes.get(accWrapper.routeId);
            accWrapper.supervisorId = objRoute.Supervisor__c;
            accWrapper.salesAgentId = objRoute.RouteManager__c;

            Boolean hasSupervisor = false;
            Boolean hasSalesAgent = false;

            // Validate Account Team Member
           /* for (AccountTeamMember teamMember: lstTeamMembers) {
                if (accId == teamMember.AccountId) {
                    if (SUPERVISOR_ROLE.equalsIgnoreCase(teamMember.TeamMemberRole)
                        && teamMember.UserId == accWrapper.supervisorId) {
                        hasSupervisor = true;
                    } else if (SALESAGENT_ROLE.equalsIgnoreCase(teamMember.TeamMemberRole)
                        && teamMember.UserId == accWrapper.salesAgentId) {
                        hasSalesAgent = true;
                    }
                }
            }*/

            // Add Supervisor and Sales Agent to Account Team if they don't exist
            if (!hasSupervisor && String.isNotBlank(accWrapper.supervisorId)) {
                AccountTeamMember teamMemberToInsert = DevUtils_cls.generateAccountTeamMember(accId, accWrapper.supervisorId,
                    SUPERVISOR_ROLE);
                lstMembersToInsert.add(teamMemberToInsert);
            }

            if (!hasSalesAgent && String.isNotBlank(accWrapper.salesAgentId)) {
                AccountTeamMember teamMemberToInsert = DevUtils_cls.generateAccountTeamMember(accId, accWrapper.salesAgentId,
                    SALESAGENT_ROLE);
                lstMembersToInsert.add(teamMemberToInsert);
            }
        }

        // Insert new Team Members
        if (!lstMembersToInsert.isEmpty()) {
        	System.debug('INSERT 1  lstMembersToInsert : '+lstMembersToInsert);
           // insert lstMembersToInsert;
        }
    }

    /**
        addTelesaleToAccountTeam
     * Add or Replace Supervisor and Sales Agent to Account Team
     * @param      lstAccountsByRoute  The list accounts by route
     */
    public static void addTelesaleToAccountTeam(AccountByRoute__c[] lstAccountsByRoute) {
        Set<Id> setRouteIds = new Set<Id>();
        Map<Id, AccountWithTeamWrapper> mapAccountWrappers = new Map<Id, AccountWithTeamWrapper>();

        for (AccountByRoute__c accByRoute: lstAccountsByRoute) {
            setRouteIds.add(accByRoute.Route__c);

            if (!mapAccountWrappers.containsKey(accByRoute.Account__c)) {
                AccountWithTeamWrapper accWrapper = new AccountWithTeamWrapper();
                accWrapper.routeId = accByRoute.Route__c;
                mapAccountWrappers.put(accByRoute.Account__c, accWrapper);
            }
        }

        // Get Routes and Account Team Members information for Accounts by Route
        Map<Id, ONTAP__Route__c> mapRoutes = new Map<Id, ONTAP__Route__c>([
            SELECT Id, RouteManager__c, Supervisor__c, ServiceModel__c
            FROM ONTAP__Route__c
            WHERE Id IN :setRouteIds
                AND ServiceModel__c IN :lstTelesaRouteServiceModels
        ]);

        System.debug(LoggingLevel.DEBUG, '*** lstTelesaRouteServiceModels: ' + lstRouteServiceModels);
        System.debug(LoggingLevel.DEBUG, '*** mapRoutes: ' + mapRoutes);

        AccountTeamMember[] lstTeamMembers = [
            SELECT Id, AccountId, UserId, TeamMemberRole
            FROM AccountTeamMember
            WHERE AccountId IN :mapAccountWrappers.keySet()
        ];

        // Iterate account wrappers and insert AccountTeamMembers
        AccountTeamMember[] lstMembersToInsert = new AccountTeamMember[]{};

        for (Id accId: mapAccountWrappers.keySet()) {
            // Add Users to Wrapper
            AccountWithTeamWrapper accWrapper = mapAccountWrappers.get(accId);

            if (!mapRoutes.containsKey(accWrapper.routeId)) {
                continue;
            }

            ONTAP__Route__c objRoute = mapRoutes.get(accWrapper.routeId);
            accWrapper.salesAgentId = objRoute.RouteManager__c;

            Boolean hasSalesAgent = false;

            // Validate Account Team Member
            for (AccountTeamMember teamMember: lstTeamMembers) {
                if (accId == teamMember.AccountId) {
                    if (SALESAGENT_ROLE_TELESALES.equalsIgnoreCase(teamMember.TeamMemberRole)
                        && teamMember.UserId == accWrapper.salesAgentId) {
                        hasSalesAgent = true;
                    }
                }
            }

            if (!hasSalesAgent && String.isNotBlank(accWrapper.salesAgentId)) {
                AccountTeamMember teamMemberToInsert = DevUtils_cls.generateAccountTeamMember(accId, accWrapper.salesAgentId,
                    SALESAGENT_ROLE_TELESALES);
                lstMembersToInsert.add(teamMemberToInsert);
            }
        }

        // Insert new Team Members
        if (!lstMembersToInsert.isEmpty()) {
        	System.debug('INSERT 2  lstMembersToInsert : '+lstMembersToInsert);
           // insert lstMembersToInsert;
        }
    }

    /**
     * Remove Supervisor and Sales Agent to Account Team
     * @param      lstAccountsByRoute  The list accounts by route
     */
    public static void removeUsersFromAccountTeam(AccountByRoute__c[] lstAccountsByRoute, Map<Id, ONTAP__Route__c> mapRoutes) {
        System.debug('***lstAccountsByRoute : '+lstAccountsByRoute + '  * TAMAÑAO : '+lstAccountsByRoute.size());
        // Get Routes and Account Team Members information for Accounts by Route
        Set<Id> setRouteIds = new Set<Id>();
        Map<Id, AccountWithTeamWrapper> mapAccountWrappers = new Map<Id, AccountWithTeamWrapper>();

     	Map<Id, Map<String,String>> mapAccountRol = new	Map<Id, Map<String,String>>();
		Set<String> setValuesRol = new set<String>();
		if(SUPERVISOR_ROLE != null){
			setValuesRol.add(SUPERVISOR_ROLE);
		}
		if(SALESAGENT_ROLE != null){
			setValuesRol.add(SALESAGENT_ROLE);
		}
        for (AccountByRoute__c accByRoute: lstAccountsByRoute) {
            setRouteIds.add(accByRoute.Route__c);

            if (!mapAccountWrappers.containsKey(accByRoute.Account__c)) {
                AccountWithTeamWrapper accWrapper = new AccountWithTeamWrapper();
                accWrapper.routeId = accByRoute.Route__c;
                mapAccountWrappers.put(accByRoute.Account__c, accWrapper);
            }
        }
        System.debug('**** setRouteIds : ' +setRouteIds);
        System.debug('**** mapAccountWrappers : ' +mapAccountWrappers  + ' TAMAÑO  :'+mapAccountWrappers.size() );
        if(mapRoutes==null){
                mapRoutes = new Map<Id, ONTAP__Route__c>([
                SELECT Id, RouteManager__c, Supervisor__c, ServiceModel__c
                FROM ONTAP__Route__c
                WHERE Id IN :setRouteIds
                    AND ServiceModel__c IN :lstRouteServiceModels
            ]);
        }

        AccountTeamMember[] lstTeamMembers = [
            SELECT Id, AccountId, UserId, TeamMemberRole FROM AccountTeamMember WHERE AccountId IN :mapAccountWrappers.keySet() and TeamMemberRole IN : setValuesRol
        ];
		System.debug(' ***lstTeamMembers : '+lstTeamMembers);
		System.debug(' ***lstTeamMembers : '+lstTeamMembers.size());
        AccountTeamMember[] lstMembersToDelete = new AccountTeamMember[]{};

        // Generate wrappers for Account with all relevant records
        for (Id accId: mapAccountWrappers.keySet()) {
            // Add Users to Wrapper
            AccountWithTeamWrapper accWrapper = mapAccountWrappers.get(accId);

            if (!mapRoutes.containsKey(accWrapper.routeId)) {
            	System.debug('NO SE CONTIENE LA RUTA');
                continue;
            }

            ONTAP__Route__c objRoute = mapRoutes.get(accWrapper.routeId);
            accWrapper.supervisorId = objRoute.Supervisor__c;
            accWrapper.salesAgentId = objRoute.RouteManager__c;

            Id currentSupervisorId = null;
            Id currentSalesAgentId = null;

            // Validate Account Team Member

            for (AccountTeamMember teamMember: lstTeamMembers) {
            	System.debug('HMDH lstTeamMembers : '+lstTeamMembers);
                if (accId == teamMember.AccountId) {
                	System.debug('LA CUENTA ES IGUAL AL DE EQUIPO DE CUENTA accId : '+ accId);
                    if (SUPERVISOR_ROLE.equalsIgnoreCase(teamMember.TeamMemberRole) && teamMember.UserId == accWrapper.supervisorId) {
                        currentSupervisorId = teamMember.Id;
                        break;
                    } else if (SALESAGENT_ROLE.equalsIgnoreCase(teamMember.TeamMemberRole)&& teamMember.UserId == accWrapper.salesAgentId) {
                        currentSalesAgentId = teamMember.Id;
                        break;
                    }
                }
           }

           if (String.isNotBlank(currentSupervisorId)) {
                lstMembersToDelete.add(new AccountTeamMember(Id = currentSupervisorId));
            }

            if (String.isNotBlank(currentSalesAgentId)) {
                lstMembersToDelete.add(new AccountTeamMember(Id = currentSalesAgentId));
            }
        }

        // Delete Account Team Members
        if (!lstMembersToDelete.isEmpty()) {
        	System.debug('******** lstMembersToDelete : '+lstMembersToDelete);
        	//delete lstMembersToDelete;
        }
    }

    /**
       1.3 removeTelesalesFromAccountTeam
     * Remove Supervisor and Sales Agent to Account Team
     * @param      lstAccountsByRoute  The list accounts by route
     */
    public static void removeTelesalesFromAccountTeam(AccountByRoute__c[] lstAccountsByRoute, Map<Id, ONTAP__Route__c> mapRoutes) {
        // Get Routes and Account Team Members information for Accounts by Route
        Set<Id> setRouteIds = new Set<Id>();
        Map<Id, AccountWithTeamWrapper> mapAccountWrappers = new Map<Id, AccountWithTeamWrapper>();

        for (AccountByRoute__c accByRoute: lstAccountsByRoute) {
            setRouteIds.add(accByRoute.Route__c);

            if (!mapAccountWrappers.containsKey(accByRoute.Account__c)) {
                AccountWithTeamWrapper accWrapper = new AccountWithTeamWrapper();
                accWrapper.routeId = accByRoute.Route__c;
                mapAccountWrappers.put(accByRoute.Account__c, accWrapper);
            }
        }
        if(mapRoutes==null){
            mapRoutes = new Map<Id, ONTAP__Route__c>([
                SELECT Id, RouteManager__c, Supervisor__c, ServiceModel__c
                FROM ONTAP__Route__c
                WHERE Id IN :setRouteIds
                    AND ServiceModel__c IN :lstTelesaRouteServiceModels
            ]);
        }

        AccountTeamMember[] lstTeamMembers = [
            SELECT Id, AccountId, UserId, TeamMemberRole
            FROM AccountTeamMember
            WHERE AccountId IN :mapAccountWrappers.keySet()
        ];

        AccountTeamMember[] lstMembersToDelete = new AccountTeamMember[]{};

        // Generate wrappers for Account with all relevant records
        for (Id accId: mapAccountWrappers.keySet()) {
            // Add Users to Wrapper
            AccountWithTeamWrapper accWrapper = mapAccountWrappers.get(accId);

            if (!mapRoutes.containsKey(accWrapper.routeId)) {
                continue;
            }

            ONTAP__Route__c objRoute = mapRoutes.get(accWrapper.routeId);
            accWrapper.salesAgentId = objRoute.RouteManager__c;

            Id currentSalesAgentId = null;

            // Validate Account Team Member
            for (AccountTeamMember teamMember: lstTeamMembers) {
            /* if (accId == teamMember.AccountId) {
                    if (SALESAGENT_ROLE_TELESALES.equalsIgnoreCase(teamMember.TeamMemberRole)
                        && teamMember.UserId == accWrapper.salesAgentId) {
                        currentSalesAgentId = teamMember.Id;
                    }
                }*/
            }

            if (String.isNotBlank(currentSalesAgentId)) {
                lstMembersToDelete.add(new AccountTeamMember(Id = currentSalesAgentId));
            }
        }

        // Delete Account Team Members
        if (!lstMembersToDelete.isEmpty()) {
           // delete lstMembersToDelete;
        }
    }

    /**
     @method Set the BDR as the owner of the account in the route
     * @param  lstAccountsByRoute Account by route records that
     * @return void
     * Rodrigo Resendiz (1.2)
     */
     public static void setBDRAsAccountOwner(AccountByRoute__c[] lstAccountsByRoute, Map<Id, ONTAP__Route__c> mapRoutes){
        System.debug('**setBDRAsAccountOwner lstAccountsByRoute : '+lstAccountsByRoute + ' TAMAÑO : '+lstAccountsByRoute.size() );
        System.debug('**setBDRAsAccountOwner mapRoutes : '+mapRoutes);


        Set<Account> accountsToUpdate_set = new Set<Account>();
        List<ONTAP__Route__c> relatedRoute = new List<ONTAP__Route__c>();
        Set<Id> setRouteIds = new Set<Id>();


        List<Account> lstAccountUpdate = new List<Account>();
        Account objAcountUpdate = new Account();

        for (AccountByRoute__c accByRoute: lstAccountsByRoute) {
            setRouteIds.add(accByRoute.Route__c);

        }
        System.debug('***setBDRAsAccountOwner setRouteIds '+setRouteIds);
        if(mapRoutes == null){
            mapRoutes = new Map<Id, ONTAP__Route__c>([
            SELECT Id, RouteManager__c, Supervisor__c, ServiceModel__c
            FROM ONTAP__Route__c
            WHERE Id IN :setRouteIds
        ]);
        }

        for(AccountByRoute__c accByRoute : lstAccountsByRoute){
        	objAcountUpdate = new Account();
        	objAcountUpdate.Id =  accByRoute.Account__c;
        	objAcountUpdate.OwnerId =  mapRoutes.get(accByRoute.Route__c).Supervisor__c;
        	lstAccountUpdate.add(objAcountUpdate);

           //accountsToUpdate_set.add(new Account(Id=accByRoute.Account__c, OwnerId = mapRoutes.get(accByRoute.Route__c).Supervisor__c));

        }
       	System.debug('**********lstAccountUpdate : '+lstAccountUpdate);
   		ISSM_TriggerManager_cls.inactivate('ISSM_AssignOwnerIdAccountTeam_tgr');
       	try{
       			Database.update(lstAccountUpdate);
       	}catch(exception e){
       		System.debug('ERROR : '+e);
       	}

        //update new List<Account>(accountsToUpdate_set);
     }
     /**
     @method Remove the BDR as the owner of the account in the route
     * @param  lstAccountsByRoute Account by route records that
     * @return void
     * Rodrigo Resendiz (1.2)
     */
     public static void removeBDRAsAccountOwner(AccountByRoute__c[] lstAccountsByRoute){
         if(!lstAccountsByRoute.isEmpty()){
             Set<Account> accountsToUpdate_set = new Set<Account>();
             String uname = accountSettingsBDR.UserToAssignOrphanAccount__c;
             Organization org = [SELECT IsSandbox FROM Organization LIMIT 1];
             if(org.IsSandbox){
                 if(Test.isRunningTest()){
                     uname += '.test';
                 }else{
                     uname += '.'+UserInfo.getUserName().substringAfterLast('.');
                 }
             }
             system.debug(':::: uname: '+uname);
             Id newOwner = null;
             if(Test.isRunningTest()){
                 newOwner = UserInfo.getUserId();
             }else{
                 newOwner = DevUtils_cls.getUserFromUsername(uname).Id;
             }


             for(AccountByRoute__c accByRoute : lstAccountsByRoute){
                 accountsToUpdate_set.add(new Account(Id=accByRoute.Account__c, OwnerId = newOwner));
             }

             update new List<Account>(accountsToUpdate_set);
         }
     }
    /**
     * Wrapper class to contain information abount Account and Team Members (Supervisor and Sales Agent)
     */
    public class AccountWithTeamWrapper {
        public Id routeId      { get; set; }
        public Id supervisorId { get; set; }
        public Id salesAgentId { get; set; }
    }
}