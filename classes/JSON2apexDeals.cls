/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: JSON2apexDeals.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 07/01/2019           Heron Zurita           Creation of methods.
*/
public class JSON2apexDeals {
    /**
    * Methods class and method for model JSON2apexDeals
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    //Added drs@avx
    public String promotionNumber;
	public String title;
    public String validfrom;
    public String validto;
    //--

    	// removed drs@avx
	/*
    public String accountId;
    public String salesOrg;
    public List<Deals> deals;
    
    public class Deals {
        public String pronr;
        public Boolean proportional;
        public Boolean optional;
        public String validfrom;
        public String validto;
        public String description;
    }
	*/
 	// removed drs@avx
    
    
    
    
    public static JSON2apexDeals parse(String json) {
        return (JSON2apexDeals) System.JSON.deserialize(json, JSON2apexDeals.class);
    }
    
}