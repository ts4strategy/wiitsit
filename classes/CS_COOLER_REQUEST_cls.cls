/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_COOLER_REQUEST_cls.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 10/01/2019           Debbie Zacarias        Creacion de la clase con las propiedades para la generacion del Json
 *                                             de la integracion a SAP 
 */
public class CS_COOLER_REQUEST_cls 
{
    
    public String role {get;set;}
    public String numb {get;set;}
    public String itmNumber {get;set;}
    public String sfdcId {get;set;}
    public String salesOrg {get;set;}
    public String paymentMethod{get;set;}
    public String orderType {get;set;}
    public String orderReason {get;set;}
    public String deliveryDate {get;set;}
    public String customerOrder{get;set;} 
    public String sku {get;set;}
    public String position {get;set;}
    public Integer quantity {get;set;}
    public String measurecode {get;set;}
    public String empties{get;set;}
    
}