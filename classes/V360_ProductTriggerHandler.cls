/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_ProductTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 03/01/2019     Carlos Leal             Creation of methods.
 */
public class V360_ProductTriggerHandler extends TriggerHandlerCustom{

    private Map<Id, ONTAP__Product__c> newMap;
    private Map<Id, ONTAP__Product__c> oldMap;
    private List<ONTAP__Product__c> newList;
    private List<ONTAP__Product__c> oldList;
    
    
   /**
    * Constructor of the class
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public V360_ProductTriggerHandler() {
        
        this.newMap = (Map<Id, ONTAP__Product__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Product__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__Product__c>) Trigger.new;
        this.oldList = (List<ONTAP__Product__c>) Trigger.old;
    }
    
    /**
    * Method wich is executed every time before  product is inserted
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void beforeInsert(){
        List<ONTAP__Product__c> validProds = PreventExecutionUtil.validateProductWithOutId(this.newList);
		translatecodes(validProds);        
    }
    
   /**
    * Method wich is executed every time before product is updated
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void beforeUpdate(){
        List<ONTAP__Product__c> validProds = PreventExecutionUtil.validateProductWithId(this.newMap);
        translatecodes(validProds);
    }
    
    /**
    * Method wich is executed every time after product is updated
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void afterInsert(){
        List<ONTAP__Product__c> validProds = PreventExecutionUtil.validateProductWithId(this.newMap);
        createAproductByOrgAfterInsertProduct(validProds);
    }
    
    /**
    * Translate codes in fields of a product in salesforce
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public void translatecodes(List<ONTAP__Product__c> lstprod){
        SET<String>ProductLibraryFields = new Set<String>();
        List<V360_LibraryFieldsList__c> library = [SELECT V360_Code__c,V360_Field__c,V360_Value__c FROM V360_LibraryFieldsList__c];
        
        Map<String, Schema.SObjectField> m = Schema.SObjectType.ONTAP__Product__c.fields.getMap();

        for(Schema.SObjectField a : m.values()){
           ProductLibraryFields.add(String.valueOf(a));
        }
        for(ONTAP__Product__c prod:lstprod){
            for(V360_LibraryFieldsList__c field :library){                
                if(field.V360_Field__c != NULL && field.V360_Code__c!=NULL && field.V360_Value__c!=NULL && ProductLibraryFields.contains(field.V360_Field__c) && field.V360_Code__c == prod.get(field.V360_Field__c)){
                    String result=(String)prod.put(field.V360_Field__c,field.V360_Value__c);
                }
            }
        }
    }
    
     /**
    * Translate codes in fields of a product in salesforce
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public void createAproductByOrgAfterInsertProduct(List<ONTAP__Product__c> lstprod){
    
        List<ISSM_ProductByOrg__c> issmprod=new  List<ISSM_ProductByOrg__c> (); 
        for(ONTAP__Product__c pro :lstprod){
            
           ISSM_ProductByOrg__c pbo= new ISSM_ProductByOrg__c();
            pbo.ISSM_AssociatedProduct__c=pro.Id;
            pbo.Country_Code__c = pro.ONTAP__Country_Code__c;
       		issmprod.add(pbo);
        } 
        insert issmprod;
	
        //promo by org id
	
        List<Id> Ids= new List<Id>();
            for(ISSM_ProductByOrg__c pborg:issmprod){
            Ids.add(pborg.Id);
        }
        
        List<ISSM_ProductByOrg__c> pbyorg = new List<ISSM_ProductByOrg__c>([select Id,ISSM_Organitation_Product__c FROM ISSM_ProductByOrg__c where Id IN: Ids]); 
     	Set<String> noRepeat = new Set<String>();
        for(ISSM_ProductByOrg__c c:pbyorg){noRepeat.add(c.ISSM_Organitation_Product__c); }
        
		Id IdRtSalesOrg = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();        
        
       	List<Account>acc=[SELECT Id,ONTAP__ExternalKey__c from Account WHERE RecordTypeId =: IdRtSalesOrg AND ONTAP__ExternalKey__c=:noRepeat];
        system.debug(acc);
        for(ISSM_ProductByOrg__c prodd :pbyorg){
            for(Account ac:acc){
                if(prodd.ISSM_Organitation_Product__c ==ac.ONTAP__ExternalKey__c )
           		 prodd.ISSM_RelatedAccount__c=ac.Id; 
                }
        }
        update pbyorg;
    }
}