/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: ONCALL_SAP_OrderResponse_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Herón Zurita           Test class of ONCALL_SAP_OrderResponse
*/

@IsTest
public class ONCALL_SAP_OrderResponse_Test {
	
    
    /**
    * Test method to parse de json and verifiy the response
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */ 
	// This test method should give 100% coverage
	@isTest static void testParse() {
		String json = '{'+
		'  \"id\": \"\",'+
		'  \"etReturn\": {'+
		'    \"item\": {'+
		'      \"type\": \"E\",'+
		'      \"number\": \"211\",'+
		'      \"message\": \"El destinatario de mercancías 100000001 no está asignado an ningún solicitante\"'+
		'    }'+
		'  }'+
		'}';
		ONCALL_SAP_OrderResponse r = ONCALL_SAP_OrderResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ONCALL_SAP_OrderResponse.Item objItem = new ONCALL_SAP_OrderResponse.Item(System.JSON.createParser(json));
		System.assert(objItem != null);
		System.assert(objItem.type_Z == null);
		System.assert(objItem.number_Z == null);
		System.assert(objItem.message == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ONCALL_SAP_OrderResponse objJSON2Apex = new ONCALL_SAP_OrderResponse(System.JSON.createParser(json));
		System.assert(objJSON2Apex != null);
		System.assert(objJSON2Apex.id == null);
		System.assert(objJSON2Apex.etReturn == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		ONCALL_SAP_OrderResponse.EtReturn objEtReturn = new ONCALL_SAP_OrderResponse.EtReturn(System.JSON.createParser(json));
		System.assert(objEtReturn != null);
		System.assert(objEtReturn.item == null);
	}
}