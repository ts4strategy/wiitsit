/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_ANSWERCASE_CLASS.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 01/02/2019      Gabriel E Garcia         Class controller of component CS_ANSWERCASE_LIGHTNING_COMPONENT
*                                          Get info about question typification matrix and answer question.
*/
public with sharing class CS_AnswerCase_Class 
{
    
    /**
    * Method that get info about questions and answers
    * @author: gabriel.e.garcia@accenture.com
    * @param Id case
    * @return Map with lists of questions and answers
    */
    @AuraEnabled
    public static Map<String, List<Object>> getQuestionsAnswers(Id IdCase)
    {
        Map<String, List<Object>> result = new Map<String, List<Object>>();
        result.put('listQuestions', new List<CS_Question_Typification_Matrix__c>());
        result.put('listAnswers', new List<CS_AnswerCase__c>());
        result.put('listCase', new List<Case>());
        
        List<CS_Question_Typification_Matrix__c> listQuestions = new List<CS_Question_Typification_Matrix__c>();
        List<CS_AnswerCase__c> listAnswers = new List<CS_AnswerCase__c>();
        Map<Id, CS_AnswerCase__c> mapAnswer = new Map<Id, CS_AnswerCase__c>();
        
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id, Status, ISSM_TypificationNumber__c FROM Case WHERE Id =: IdCase];
        
        if(!listCase.isEmpty())
        {
            result.get('listCase').addAll(listCase);
        	listQuestions = [SELECT Id, CS_Question__c, CS_Question_Type__c, CS_Options__c, CS_Typification_Matrix_Number__c 
                             FROM CS_Question_Typification_Matrix__c 
                             WHERE CS_Typification_Matrix_Number__c =: listCase[0].ISSM_TypificationNumber__c 
                             AND CS_Typification_Matrix_Number__c != null ORDER BY NAME ]; 
                         
        	listAnswers = [SELECT Id, Answer__c, Case__c, TypificationMatrixQuestions__c FROM CS_AnswerCase__c WHERE Case__c =: IdCase];
            
            
            for(CS_AnswerCase__c answer : listAnswers)
            {
                mapAnswer.put(answer.TypificationMatrixQuestions__c, answer);
            }
            
            for(CS_Question_Typification_Matrix__c question : listQuestions )
            {
                if(mapAnswer.containsKey(question.Id))
                {
                    result.get('listQuestions').add(question);
                    result.get('listAnswers').add(mapAnswer.get(question.Id));
                }
                else
                {
                    result.get('listQuestions').add(question);
                    result.get('listAnswers').add(new CS_AnswerCase__c(TypificationMatrixQuestions__c = question.Id, Case__c = IdCase));
                }
            }
        } 

        return result;
    }
    
    /**
    * Method that save list of answers
    * @author: gabriel.e.garcia@accenture.com
    * @param List of answer case
    * @return Map with result of save answers
    */
    @AuraEnabled
    public static Map<String, Object> saveAnswers(String listAnswers)
    {        
        List<CS_AnswerCase__c> listAnsw = new List<CS_AnswerCase__c>();
        Map<String, Object> result = new Map<String, Object>();
        try
        {
            listAnsw = (List<CS_AnswerCase__c>)JSON.deserialize(listAnswers, List<CS_AnswerCase__c>.class);
            upsert listAnsw;
            
            result.put('Status', 'Success');
            result.put('listAnswer', listAnsw);
            UpdateAnswerCaseForce(listAnsw[0].Case__c);
            
        }
        catch(Exception ex)
        {
            System.debug('CS_ANSWERCASE_CLASS.saveAnswers Message: ' + ex.getMessage());   
            System.debug('CS_ANSWERCASE_CLASS.saveAnswers Cause: ' + ex.getCause());   
            System.debug('CS_ANSWERCASE_CLASS.saveAnswers Line number: ' + ex.getLineNumber());   
            System.debug('CS_ANSWERCASE_CLASS.saveAnswers Stack trace: ' + ex.getStackTraceString()); 
            result.put('Status', 'Fail Error saveAnswers message: ' + ex.getMessage() + ', line: ' + ex.getLineNumber() + ', cause: ' + ex.getCause());
            result.put('listAnswer', listAnsw);
        }
     
        return result;
    }
    
    /**
    * Method to save the questionnaire in Case Force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case
    * @return Void
    */
    private static void UpdateAnswerCaseForce(Id idCase)
    {
        string questionnaire = '';
        ONTAP__Case_Force__c oUpdateCaseForce = new ONTAP__Case_Force__c();
        Case oCase = new Case();
        Map<String, List<Object>> MapQuestions = new Map<String, List<Object>>();
        List<CS_Question_Typification_Matrix__c> listQuestions = new List<CS_Question_Typification_Matrix__c>();
        List<CS_AnswerCase__c> listAnswers = new List<CS_AnswerCase__c>();
        
        try
        {
            oCase = [SELECT ISSM_CaseForceNumber__c FROM Case WHERE Id =: idCase];
            
            if(oCase.ISSM_CaseForceNumber__c != null)
            {
                oUpdateCaseForce.Id = oCase.ISSM_CaseForceNumber__c;
                MapQuestions = getQuestionsAnswers(idCase);
                
                listQuestions = (List<CS_Question_Typification_Matrix__c>)MapQuestions.get('listQuestions');
                listAnswers = (List<CS_AnswerCase__c>)MapQuestions.get('listAnswers');
                
                for(CS_Question_Typification_Matrix__c oQuestion : listQuestions)
                {
                    for(CS_AnswerCase__c oAnswer : listAnswers)
                    {
                        if(oQuestion.Id == oAnswer.TypificationMatrixQuestions__c)
                            questionnaire = questionnaire + oQuestion.CS_Question__c + ' : ' + (oAnswer.Answer__c == null ? '' : oAnswer.Answer__c) + '\n';
                    }
                }
            
                oUpdateCaseForce.CS_Questionnaire__c = questionnaire;
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                Update oUpdateCaseForce;
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_ANSWERCASE_CLASS.UpdateAnswerCaseForce Message: ' + ex.getMessage());   
            System.debug('CS_ANSWERCASE_CLASS.UpdateAnswerCaseForce Cause: ' + ex.getCause());   
            System.debug('CS_ANSWERCASE_CLASS.UpdateAnswerCaseForce Line number: ' + ex.getLineNumber());   
            System.debug('CS_ANSWERCASE_CLASS.UpdateAnswerCaseForce Stack trace: ' + ex.getStackTraceString());  
        }
    }
}