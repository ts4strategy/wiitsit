/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_DeleteInvoicesBatch_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2018     Carlos Leal        Creation of methods.
*/
@isTest
public class V360_DeleteInvoicesBatch_Test {
    
    /**
* Method test for delete all the invoices in V360_DeleteInvoicesBatch.
* @author: c.leal.beltran@accenture.com
* @param void
* @return void
*/
    @isTest static void testBatch(){
        ONCALL__Invoice__c obj = New ONCALL__Invoice__c();
        insert obj;    
        V360_DeleteInvoicesBatch obj01 = new V360_DeleteInvoicesBatch();
        Database.executeBatch(obj01, 200);
    }
}