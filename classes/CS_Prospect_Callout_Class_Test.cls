/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_Prospect_Callout_Class_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 25/04/2019      Jose Luis Vargas        Creation of the class test for CS_Prospect_Callout_Class
 *                                             
*/

@isTest
public class CS_Prospect_Callout_Class_Test 
{
    /**
    * method for test method SendProspectMuleSoft
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return Void
    */ 
    @isTest
    public static void test_SendProspectMuleSoft()
    {
        Id RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        
        /* Se inserta la configuracion*/
        CS_Prospect_Settings__c oSettingProspect = new CS_Prospect_Settings__c();
        oSettingProspect.Prospect_Url__c = 'https://wiit-hndesv-prospects-sfdc-to-mdweb.de-c1.cloudhub.io/prospectsTable';
        oSettingProspect.Prospect_Token__c = '262fd69b-18c8-4172-9157-33d3de2917e4';
        Insert oSettingProspect;
        
        /* Se inserta una cuenta */
        Account oAccount = new Account( Name='AccountTest', ONTAP__Latitude__c= '25.65653', ONTAP__Longitude__c='-100.39177');
        Insert oAccount;
        
        /* Se inserta el prospecto */
        Account oProspect = new Account();
        oProspect.Name = 'Prospect Test';
        oProspect.CS_Last_Name__c = 'Test';
        oProspect.CS_Second_Last_Name__c = 'Test';
        oProspect.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspect.ISSM_ProspectType__c = 'PN';
        oProspect.CS_Tipo_Identificacion__c = '00';
        oProspect.HONES_IdentificationNumber__c = '10411692';
        oProspect.ONTAP__Mobile_Phone__c = '5549001010';
        oProspect.CS_Tipo_Via__c = 'CL';
        oProspect.ONTAP__Street__c = 'Romero';
        oProspect.ONTAP__Street_Number__c = '12';
        oProspect.CS_Tipo_Poblacion__c = 'Urbano';
        oProspect.CS_Poblacion__c = '06 - Choluteca';
        oProspect.CS_Distrito__c = 'ALUBAREN';
        oProspect.V360_ReferenceClient__c = oAccount.Id;
        oProspect.RecordTypeId = RecordTypeProspect;
        Insert oProspect;
        
        /* Se inserta el caso del prospecto */
        Case oCaseProspect = new Case();
        oCaseProspect.AccountId = oProspect.Id;
        Insert oCaseProspect;
        
        Test.setMock(HttpCalloutMock.class, new CS_MockResponse_Prospect());
        List<Id> lstProspectId = new List<Id>();
        lstProspectId.add(oProspect.Id);
               
        CS_Prospect_Callout_Class.SendProspectMuleSoft(lstProspectId);
    }
    
    /**
    * method for test method SendProspectMuleSoft with error
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return Void
    */
    @isTest
    public static void test_SendProspectMuleSoft_Error()
    {
        Id RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        
        /* Se inserta la configuracion*/
        CS_Prospect_Settings__c oSettingProspect = new CS_Prospect_Settings__c();
        oSettingProspect.Prospect_Url__c = 'https://wiit-hndesv-prospects-sfdc-to-mdweb.de-c1.cloudhub.io/prospectsTable';
        oSettingProspect.Prospect_Token__c = '262fd69b-18c8-4172-9157-33d3de2917e4';
        Insert oSettingProspect;
        
        /* Se inserta una cuenta */
        Account oAccount = new Account( Name='AccountTest', ONTAP__Latitude__c= '25.65653', ONTAP__Longitude__c='-100.39177');
        Insert oAccount;
        
        /* Se inserta el prospecto */
        Account oProspect = new Account();
        oProspect.Name = 'Prospect Test';
        oProspect.CS_Last_Name__c = 'Test';
        oProspect.CS_Second_Last_Name__c = 'Test';
        oProspect.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspect.ISSM_ProspectType__c = 'PN';
        oProspect.CS_Tipo_Identificacion__c = '00';
        oProspect.HONES_IdentificationNumber__c = '10411692';
        oProspect.ONTAP__Mobile_Phone__c = '5549001010';
        oProspect.CS_Tipo_Via__c = 'CL';
        oProspect.ONTAP__Street__c = 'Romero';
        oProspect.ONTAP__Street_Number__c = '12';
        oProspect.CS_Tipo_Poblacion__c = 'Urbano';
        oProspect.CS_Poblacion__c = '06 - Choluteca';
        oProspect.CS_Distrito__c = 'ALUBAREN';
        oProspect.V360_ReferenceClient__c = oAccount.Id;
        oProspect.RecordTypeId = RecordTypeProspect;
        Insert oProspect;
        
        /* Se inserta el caso del prospecto */
        Case oCaseProspect = new Case();
        oCaseProspect.AccountId = oProspect.Id;
        Insert oCaseProspect;
        
        Test.setMock(HttpCalloutMock.class, new CS_MockResponse_Prospect_Error());
        List<Id> lstProspectId = new List<Id>();
        lstProspectId.add(oProspect.Id);
               
        CS_Prospect_Callout_Class.SendProspectMuleSoft(lstProspectId);
    }
}