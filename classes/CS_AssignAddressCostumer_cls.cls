/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_AssignAddressCostumer_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 07/01/2019           Debbie Zacarias        Creation of the class for control and assignment 
* 											  of customer reference 											  
*/

public without sharing class CS_AssignAddressCostumer_cls 
{
    // Variables   
    public Id RegistroId { get; set; } 
    public ApexPages.StandardController stdCntrlr { get; set;}
    
    public CS_AssignAddressCostumer_cls(ApexPages.StandardController controller) 
    {
        
        stdCntrlr = controller;
        // Get record Id for current case
        RegistroId = (Id)controller.getId();
    }
    
    public CS_AssignAddressCostumer_cls(Id idCase) 
    {
        RegistroId = idCase;
    }
    
    /**
	* Method for consulting  the detail of current case and get the Customer Reference
	* @author: d.zacarias.cantillo@accenture.com
	* @param void
	* @return void 
	*/    
    public void getCostumerReference()
    {
        List<Case> currentCaseList = [SELECT Id, AccountId ,CS_Costumer_reference__c, ISSM_LocalStreet__c, CS_Longitude__c, CS_Latitud__c,ISSM_LocalNumber__c, CS_Postal_Code__c,ISSM_PostalCode__c, ISSM_LocalColony__c, ISSM_LocalCity__c, HONES_Case_Country__c FROM Case WHERE Id =:RegistroId LIMIT 1];
        
        if(currentCaseList.size() > 0)
        {
            Case currentCase = currentCaseList.get(0);
            String street = currentCase.ISSM_LocalStreet__c;
            String postalCode = currentCase.CS_Postal_Code__c;
            String colony = currentCase.ISSM_LocalColony__c;
            String account = currentCase.AccountId;
        
            if(street == null || postalCode == null || colony == null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please verify that Street, Colony and Postal Code are populated.'));
            }
            else
            {
                List<Account> lstAccCalle = new List<Account>([SELECT id, Name, ONTAP__Latitude__c, ONTAP__Longitude__c, ONTAP__Street__c, ONTAP__PostalCode__c, ONTAP__Neighborhood__c FROM Account WHERE ONTAP__Street__c =:street AND Id !=:account LIMIT 1]);
                List<Account> lstAccColonia = new List<Account>([SELECT id, Name, ONTAP__Latitude__c, ONTAP__Longitude__c, ONTAP__Street__c, ONTAP__PostalCode__c, ONTAP__Neighborhood__c FROM Account WHERE ONTAP__Neighborhood__c =:colony AND Id !=:account LIMIT 1]);
                List<Account> lstAccPostal = new List<Account>([SELECT id, Name, ONTAP__Latitude__c, ONTAP__Longitude__c, ONTAP__Street__c, ONTAP__PostalCode__c, ONTAP__Neighborhood__c FROM Account WHERE ONTAP__PostalCode__c =:postalCode AND Id !=:account LIMIT 1]);
            
                if(!lstAccPostal.isEmpty())
                {
                    List<Account> accColon = new List<Account>();
                    for(Account acc:lstAccPostal)
                    {
                        if(acc.ONTAP__Neighborhood__c.equals(colony))
                        {
                            accColon.add(acc);
                        }
                    }
                    
                    if(!accColon.isEmpty())
                    {
                        List<Account> accStreet = new List<Account>();
                        for(Account acc:accColon)
                        {
                            if(acc.ONTAP__Street__c.equals(street))
                            {
                                accStreet.add(acc);
                            }
                        }
                        
                        if(!accStreet.isEmpty())
                        {
                            currentCase.CS_Costumer_reference__c = accStreet[0].Name;
                            currentCase.CS_Latitud__c = accStreet[0].ONTAP__Latitude__c;
                            currentCase.CS_Longitude__c = accStreet[0].ONTAP__Longitude__c;
                        }
                        else
                        {
                            currentCase.CS_Costumer_reference__c = accColon[0].Name;
                            currentCase.CS_Latitud__c = accColon[0].ONTAP__Latitude__c;
                            currentCase.CS_Longitude__c = accColon[0].ONTAP__Longitude__c;
                        }
                    }
                    else
                    {
                        currentCase.CS_Costumer_reference__c = lstAccPostal[0].Name;
                        currentCase.CS_Latitud__c = lstAccPostal[0].ONTAP__Latitude__c;
                        currentCase.CS_Longitude__c = lstAccPostal[0].ONTAP__Longitude__c;
                    }    
                }
                else if(!lstAccColonia.isEmpty())
                {
                    List<Account> accStreet = new List<Account>();
                    for(Account acc:lstAccColonia)
                    {
                        if(acc.ONTAP__Street__c.equals(street))
                        {
                            accStreet.add(acc);
                        }
                    }
                
                    if(!accStreet.isEmpty())
                    {
                        currentCase.CS_Costumer_reference__c = accStreet[0].Name;
                        currentCase.CS_Latitud__c = accStreet[0].ONTAP__Latitude__c;
                        currentCase.CS_Longitude__c = accStreet[0].ONTAP__Longitude__c;
                    }
                    else
                    {
                        currentCase.CS_Costumer_reference__c = lstAccColonia[0].Name;
                        currentCase.CS_Latitud__c = lstAccColonia[0].ONTAP__Latitude__c;
                        currentCase.CS_Longitude__c = lstAccColonia[0].ONTAP__Longitude__c;
                    }
                }
                else if(!lstAccCalle.isEmpty())
                {
                    currentCase.CS_Costumer_reference__c = lstAccCalle[0].Name;
                    currentCase.CS_Latitud__c = lstAccCalle[0].ONTAP__Latitude__c;
                    currentCase.CS_Longitude__c = lstAccCalle[0].ONTAP__Longitude__c;
                }
                else
                {
                    //String refNotFound = Label.CS_Customer_Ref;
                    //currentCase.CS_Costumer_reference__c = refNotFound;
                    currentCase.CS_Costumer_reference__c = 'Customer Reference not found';
                }
            
                ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
                update currentCase;
            }  
        }
    }
}