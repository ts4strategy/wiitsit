public class Flexible2 {
    
    @AuraEnabled
    public static  List<List<v360_FlexibleData__c>> getFlexibleDataGeneral (String recordId){
        String types;
        System.debug('recordId '  + recordId);
        
        List<List<v360_FlexibleData__c>> general = new List<List<v360_FlexibleData__c>>();
        List<String> daataType = new List<String>();
        List<v360_FlexibleData__c> data = [SELECT Name, v360_Type__c,v360_Value__c FROM v360_FlexibleData__c WHERE v360_Account__c =:recordId  AND v360_ValidTo__c <=: System.TODAY() ORDER BY v360_Sort__c ASC LIMIT 20
                                           ];
        //fill list with types
        for (v360_FlexibleData__c dta :data){
            types=dta.v360_Type__c;
            if(!daataType.contains(types))
            {daataType.add(types);}
            
        }
         //separate list by types
        for(String b:daataType)
        {
         List<v360_FlexibleData__c> general2 = new List<v360_FlexibleData__c>();
            for(v360_FlexibleData__c a:data) {
                if(a.v360_Type__c == b ){
                   general2.add(a);
                }
            }
            general.add(general2);
        }
        return general;
    }
}