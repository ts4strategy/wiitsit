/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_KPITriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_KPITriggerHandler extends TriggerHandlerCustom{
 	private Map<Id, ONTAP__KPI__c> newMap;
    private Map<Id, ONTAP__KPI__c> oldMap;
    private List<ONTAP__KPI__c> newList;
    private List<ONTAP__KPI__c> oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public V360_KPITriggerHandler() {
        
        this.newMap = (Map<Id, ONTAP__KPI__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__KPI__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__KPI__c>) Trigger.new;
        this.oldList = (List<ONTAP__KPI__c>) Trigger.old;
    }
    
    /**
    * Method wich is executed every before insert
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeInsert(){
         //List<ONTAP__KPI__c> lst = PreventExecutionUtil.validateKPIWithOutId(this.newList);
         //relateAccountsToKPI(lst);
    }
    
        /**
    * method wich is executed every time a coverage is inserted
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterInsert(){   
        if(!System.isFuture()){
        	relateAccountsToKPI(this.newMap.keySet());
        }
    }
    
    /**
    * Method wich is executed every before update
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeUpdate(){
        //List<ONTAP__KPI__c> lst = PreventExecutionUtil.validateKPIWithId(this.newMap);
        //relateAccountsToKPI(lst);
    }
    
         /**
    * method wich is executed every time a coverage is updated
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterUpdate(){
        if(!System.isFuture()){
        	relateAccountsToKPI(this.newMap.keySet());
        }
    }
    
    /** 
    * Method wich releates every KPI to a Customer
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @future
    public static void relateAccountsToKPI(Set<Id> setIds){
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        List<ONTAP__KPI__c> newlist = new List<ONTAP__KPI__c>();
        List<ONTAP__KPI__c> listtoUpdate = new List<ONTAP__KPI__c>();
        newlist = [SELECT Id, Account_Sap_Id__c FROM ONTAP__KPI__c WHERE Id IN: setIds];
        
        for (ONTAP__KPI__c accKPI:newlist){
            if(accKPI.Account_Sap_Id__c!=NULL){
                SAPNumsToRelate.add(accKPI.Account_Sap_Id__c);
            }
        }
        System.debug('SAPNumsToRelate'+SAPNumsToRelate);
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
      
        
        for (ONTAP__KPI__c finalKPI:newlist){
           Account acc = accReference.get(finalKPI.Account_Sap_Id__c);
            if(acc!= NULL){           
                finalKPI.ONTAP__Account_id__c = acc.Id;
                listtoUpdate.add(finalKPI);
			}                               
        }
        
        if(!listtoUpdate.isEmpty()){
            update listtoUpdate;
        }
    }
}