/* ----------------------------------------------------------------------------
 * AB InBev :: Trigger Contact Handler
 * ----------------------------------------------------------------------------
 * Clase: CS_TriggerContactHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 23/01/2019      Gabriel García          Creation of methods.
 */
public class CS_TriggerContactHandler extends TriggerHandlerCustom{
	private Map<Id, Contact> newMap;
    private Map<Id, Contact> oldMap;
    private List<Contact> newList;
    private List<Contact> oldList;
    
    public CS_TriggerContactHandler() {
        
        this.newMap = (Map<Id, Contact>) Trigger.newMap;
        this.oldMap = (Map<Id, Contact>) Trigger.oldMap;
        this.newList = (List<Contact>) Trigger.new;
        this.oldList = (List<Contact>) Trigger.old;
    }
    
    /**
    * method wich start every time after record is insert 
    * @author: gabriel.e.garcia@accenture.com
    * @param void
    * @return Void
    */
    public override void afterInsert(){
        assignePrincipalContact(this.newlist, null);
    }
    
    /**
    * method wich start every time after record is update 
    * @author: gabriel.e.garcia@accenture.com
    * @param void
    * @return Void
    */
    public override void afterUpdate(){
        assignePrincipalContact(this.newlist, this.oldMap);
    }
            
    /**
    * method to assign main contact 
    * @author: gabriel.e.garcia@accenture.com
    * @param list with Trigger.new values, old map with Trigger.oldMap values
    * @return Void
    */
    public void assignePrincipalContact(List<Contact> newList, Map<Id, Contact> oldMap){
        Set<Id> setCuentas = new Set<Id>();
        List<Contact> listContacts = new List<Contact>();
        
        if(oldMap == null){
            for(Contact contacto : newList){
                if(contacto.CS_Contacto_Principal__c == true && contacto.AccountId != null){
                    setCuentas.add(contacto.AccountId);
                }
            }
            
            if(!setCuentas.isEmpty()){
                listContacts = [SELECT Id, CS_Contacto_Principal__c FROM Contact WHERE CS_Contacto_Principal__c = true AND AccountId IN: setCuentas AND Id !=: newList];
            }
            
            if(!listContacts.isEmpty()){
                for(Contact cto : listContacts){
                    cto.CS_Contacto_Principal__c = false;
                }
	
                try{
                	update listContacts;
                }catch(Exception e){
                    System.debug('Error to update Contacts ' + e.getMessage() + ' line ' + e.getLineNumber());
                }                   
            }
            
        }else{
            for(Contact contacto : newList){
                if(contacto.CS_Contacto_Principal__c == true && oldMap.get(contacto.Id).CS_Contacto_Principal__c == false && contacto.AccountId != null){
                    setCuentas.add(contacto.AccountId);
                }
            }
            
            if(!setCuentas.isEmpty()){
                listContacts = [SELECT Id, CS_Contacto_Principal__c FROM Contact WHERE CS_Contacto_Principal__c = true AND AccountId IN: setCuentas AND Id !=: newList];
            }
            
            if(!listContacts.isEmpty()){
                for(Contact cto : listContacts){
                    cto.CS_Contacto_Principal__c = false;
                }
	
                try{
                	update listContacts;
                }catch(Exception e){
                    System.debug('Error to update Contacts ' + e.getMessage() + ' line ' + e.getLineNumber());
                }                   
            }
        }
    }    
}