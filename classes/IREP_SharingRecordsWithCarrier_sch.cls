global class IREP_SharingRecordsWithCarrier_sch implements Schedulable {
	global void execute(SchedulableContext sc) {
		IREP_BatchControlExecution__mdt conf = [
            Select Id, IREP_BatchSize__c
            From IREP_BatchControlExecution__mdt
            Where Label = 'IREP_SharingRecordsWithCarrier_bch'
            Limit 1
        ];

        Integer intBatchSize = Integer.valueOf(conf.IREP_BatchSize__c);

		IREP_SharingRecordsWithCarrier_bch b = new IREP_SharingRecordsWithCarrier_bch();
		database.executebatch(b, intBatchSize);
	}
}