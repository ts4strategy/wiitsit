/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_CASE_COOLER_BATCH_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 24/01/2019           Debbie Zacarias       Creation of batch class
*/

global class CS_CASE_COOLER_BATCH_cls implements Database.Batchable<sObject>, Database.AllowsCallouts
{
    //Variables
    String countryESV = Label.CS_Country_El_Salvador;
    String countryHN = Label.CS_Country_Honduras;
    String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
    String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
    String query = 'SELECT Id, ISSM_NumeroSAP__c, AccountId, CS_skuProduct__c,ISSM_InterncalComments__c, CS_Coolers_Logs__c, HONES_Case_Country__c, RecordType.DeveloperName FROM Case WHERE CS_Send_To_Mule__c = false AND (RecordType.DeveloperName =:rtInstallationCooler OR RecordType.DeveloperName =:rtRetirementCooler) AND (HONES_Case_Country__c =:countryHN OR HONES_Case_Country__c =:countryESV)';

    global CS_CASE_COOLER_BATCH_cls(){}
    
    /**
    * method to collect the records to pass to the interface method execute
    * @author: d.zacarias.cantillo@accenture.com
    * @param Batchable context to track the progress of the batch
    * @return Database.QueryLocator to pass the records to method execute
    */ 
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    /**
    * method to do the required processing for data
    * @author: d.zacarias.cantillo@accenture.com
    * @param Batchable Context to track the progress of the batch
    * @param List Case that collect the records in method start
    * @return void
    */ 
    global void execute(Database.BatchableContext BC, List<Case> scope)
    {  
        try
        {   	
            for(Case item: scope)
            {
                Boolean flagAttempt = false;
                Integer attempt = 1;
                while(attempt <= 2 && !flagAttempt)
                {
                    flagAttempt = CS_CASE_COOLERS_CALLOUT_cls.sendRequest(item);
                    attempt++;
                }
            }
                
            List<Case> lstcasesToUpdate = new List<Case>();
            for(Id key: CS_CASE_COOLERS_CALLOUT_cls.cases.keySet())
            {
                lstcasesToUpdate.add(CS_CASE_COOLERS_CALLOUT_cls.cases.get(key));
            }
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            update lstcasesToUpdate;
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_BATCH_cls.execute  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_BATCH_cls.execute  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLER_BATCH_cls.execute  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_BATCH_cls.execute  Stack trace: ' + ex.getStackTraceString());
        }                
    }
    
    /**
    * method finish
    * @author: d.zacarias.cantillo@accenture.com
    * @param BatchableContext
    * @return void
    */
    global void finish (Database.BatchableContext BC)
    {
        
    }
}