/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: Lookup_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class Lookup_Test {
    
	/**
    * Test method for create the Query for the object
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void Lookup_UseCase1(){
        ONTAP__Order__c objInst = new ONTAP__Order__c();
        objInst.ONCALL__name__c= 'A';
        insert objInst;
        String test = Lookup.searchDB('ONTAP__Order__c', 'ISSM_Autonumber__c', 'ONCALL__Account_Name__c', 
                                  5, 'ONCALL__name__c', 'A' );
    }
    
    /**
    * Test method for set values in the object
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void Lookup_UseCase2(){
        Lookup.ResultWrapper testwrap = new Lookup.ResultWrapper();
        testwrap.objName = 'Test';
        testwrap.text = 'Test';
        testwrap.val = 'Test';
    }
}