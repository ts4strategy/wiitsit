/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_CASE_COOLER_cls_test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 11/02/2019           Debbie Zacarias        Creation of the test class for CS_CASE_COOLER_cls
*/

@isTest
private class CS_Case_Cooler_Class_Test
{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @TestSetup
    static void setup()
    {
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        
        User currentUser = [select Id, username, Country__c from User where Id = :UserInfo.getUserId()];
        currentUser.Country__c = 'El Salvador';
        update currentUser;
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        caseTest.CS_skuProduct__c = product.ONTAP__ProductCode__c;
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        insert caseTest;
    }
    
    /**
    * Method for test the method getPickListValues
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getPickListValues()
    {
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Bogota',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_US',
            Country__c = 'Honduras',
            ManagerId = UserInfo.getUserId()
        );
        insert u;
        
        ONTAP__Product__c productHN = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686', ONTAP__Country_Code__c = 'HN', ONTAP__ProductType__c='ZTRC');
        insert productHN;
        
        ONTAP__Product__c productSV = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686', ONTAP__Country_Code__c = 'SV', ONTAP__ProductType__c='ZTRC');
        insert productSV;
        
        CS_Case_Cooler_Class.getPickListValues();
    }
    
    /**
    * Method for test the method GetProductCase
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest 
    static void test_GetProductCase()
    {
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        caseTest.CS_skuProduct__c = product.ONTAP__ProductCode__c;
        insert caseTest;
        
        Case caseTes = ([SELECT id FROM Case LIMIT 1]); 
        String caset = caseTes.Id;
        CS_CASE_COOLER_Class.GetProductCase(caset,'Product Test');
    }
    
    /**
    * Method for test the method SetProductCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest  
    static void test_SetProductCase()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        insert caseTest;

        CS_Case_Cooler_Class.SetProductCase(caseTest.Id);
    }
    
    /**
    * Method for test the method UpdateCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_UpdateCase()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        
        ONTAP__Case_Force__c oCaseForceTest = new ONTAP__Case_Force__c();
        oCaseForceTest.ONTAP__Account__c = accountTest.Id;
        Insert oCaseForceTest;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        caseTest.ISSM_CaseForceNumber__c = oCaseForceTest.Id;
        insert caseTest;
        
        CS_Case_Cooler_Class.UpdateCase(caseTest.Id, product.ONTAP__ProductCode__c, product.Id, 1);
    }
}