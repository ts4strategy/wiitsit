/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_CASEASSETACCOUNT_CLS.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 28/01/ 2018     Debbie Zacarias	      Creation of the class used for the Case Lightning component for account assets 
*/
public class CS_CASEASSETACCOUNT_CLS {
    
	/**
	* method for find brands of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return List<String> that controls the picklist values
	*/    
    @AuraEnabled
    public static List<String> getBrand(String idCase)
    {
        List<String> pickListValuesList= new List<String>();
        Set<String> values = new Set<String>();
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            
            List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>([SELECT ONTAP__Account__c, ONTAP__Brand__c 
                                                                                   FROM ONTAP__Account_Asset__c 
                                                                                   WHERE ONTAP__Account__c =: caso.Accountid]);
            pickListValuesList.add('');
            for(ONTAP__Account_Asset__c item:acc)
            {
                values.add(item.ONTAP__Brand__c);
            }
            
            pickListValuesList.addAll(values);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Stack trace: ' + ex.getStackTraceString());
        }
        return pickListValuesList;
    }
    
    /**
	* method for find Models of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @param Brand for filter results in query
	* @return List<String> that controls the picklist values
	*/ 
    @AuraEnabled
    public static List<String> getModel(String idCase, String Brand)
    {
        List<String> pickListValuesList= new List<String>();
        Set<String> values = new Set<String>();
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            
            List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>([SELECT ONTAP__Account__c, HONES_Asset_Model__c 
                                                                                   FROM ONTAP__Account_Asset__c 
                                                                                   WHERE ONTAP__Brand__c=:Brand 
                                                                                   AND ONTAP__Account__c=:caso.AccountId]);
            pickListValuesList.add('');
            for(ONTAP__Account_Asset__c item:acc)
            {
                values.add(item.HONES_Asset_Model__c);
            }
            pickListValuesList.addAll(values);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETMODEL Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETMODEL Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETMODEL Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETMODEL Stack trace: ' + ex.getStackTraceString());
        } 
        return pickListValuesList;
    }
    
    /**
	* method for find Serial numbers of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @param Brand for filter results in query
	* @param Model for filter results in query
	* @return List<String> that controls the picklist values
	*/
    @AuraEnabled
    public static List<String> getSerialNumber(String idCase, String Brand, String Model)
    {
        List<String> pickListValuesList= new List<String>();
        Set<String> values = new Set<String>();
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            
            List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>([SELECT ONTAP__Account__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c 
                                                                                   FROM ONTAP__Account_Asset__c 
                                                                                   WHERE HONES_Asset_Model__c =:Model 
                                                                                   AND ONTAP__Brand__c=:Brand 
                                                                                   AND ONTAP__Account__c=:caso.AccountId]);
            pickListValuesList.add('');
            for(ONTAP__Account_Asset__c item:acc)
            {
                values.add(item.ONTAP__Serial_Number__c);
            }
            pickListValuesList.addAll(values);
        }catch(Exception ex){
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Stack trace: ' + ex.getStackTraceString());
        } 
        return pickListValuesList;
    }
    
    /**
	* method for find Equipment numbers of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @param Brand for filter results in query
	* @param Model for filter results in query
	* @param SerialNumber for filter results in query
	* @return List<String> that controls the picklist values
	*/
    @AuraEnabled
    public static List<String> getEquipmentNumber(String idCase, String Brand, String Model, String SerialNumber)
    {
        List<String> pickListValuesList= new List<String>();
        Set<String> values = new Set<String>();
        try{
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>([SELECT ONTAP__Account__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c 
                                                                                   FROM ONTAP__Account_Asset__c 
                                                                                   WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                                                                   AND HONES_Asset_Model__c =:Model 
                                                                                   AND ONTAP__Brand__c=:Brand 
                                                                                   AND ONTAP__Account__c=:caso.AccountId]);
            pickListValuesList.add('');
            for(ONTAP__Account_Asset__c item:acc)
            {
                values.add(item.HONES_EquipmentNumber__c);
            }
            pickListValuesList.addAll(values);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETEQUIPMENTNUMBER Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETEQUIPMENTNUMBER Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETEQUIPMENTNUMBER Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETEQUIPMENTNUMBER Stack trace: ' + ex.getStackTraceString());
        } 
        return pickListValuesList;
    }
    
    /**
	* method for find Description of assets for account cases and update the current case with the information 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @param Brand for filter results in query
	* @param Model for filter results in query
	* @param SerialNumber for filter results in query
	* @param EquipmentNumber for filter results in query
	* @return String that controls the description value to show
	*/
    @AuraEnabled
    public static String getDescription(String idCase, String Brand, String Model, String SerialNumber, String EquipmentNumber)
    {
        String textAreaValue = '';
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            ONTAP__Account_Asset__c acc = ([SELECT ONTAP__Account__c,ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND HONES_Asset_Model__c =:Model 
                                            AND ONTAP__Brand__c=:Brand 
                                            AND ONTAP__Account__c=:caso.AccountId]);
            
            textAreaValue = acc.ONTAP__Asset_Description__c;
            
            //UpdateCase(idCase, Brand, Model, SerialNumber, EquipmentNumber,textAreaValue, acc.V360_MaterialNumber__c);           
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETDESCRIPTION Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETDESCRIPTION Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETDESCRIPTION Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETDESCRIPTION Stack trace: ' + ex.getStackTraceString());
        } 
        return textAreaValue;
    }
    
    /*
	* method for find Info of assets for current case
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return Case that get the info
	*/
    @AuraEnabled
    public static Case SetAssetCaseInfo(String idCase)
    {
        Case caso = ([SELECT id, Status, AccountId,ISSM_BrandProduct__c, ISSM_CaseReason__c ,Equipment_model__c, ISSM_SerialNumber__c, CS_Serial_number_of_the_equipment__c 
                      FROM Case 
                      WHERE id =:idCase]);
        
        return caso;        
    }
    
    /**
	* method for update current case with the information passed trough GetDescription method 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case for update
	* @param Brand for update 
	* @param Model for update
	* @param SerialNumber for update
	* @param EquipmentNumber for update
	* @param Description for update
	* @return Boolean if record was updated
	*/
    @AuraEnabled
    public static Boolean UpdateCase(String idCase, String Brand, String Model, String SerialNumber, String EquipmentNumber, String Description, String recType)
    {
        Boolean success = false;
        String recordTypeRetire = Label.CS_RT_Retirement_Cooler;
        try
        {
            
            Case caso = ([SELECT id,AssetId, AccountId, ISSM_BrandProduct__c, ISSM_CaseReason__c ,Equipment_model__c, ISSM_SerialNumber__c, CS_skuProduct__c, CS_Serial_number_of_the_equipment__c 
                          FROM Case 
                          WHERE id =:idCase]);
                        
            ONTAP__Account_Asset__c acc = ([SELECT ONTAP__Account__c,ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND HONES_Asset_Model__c =:Model 
                                            AND ONTAP__Brand__c=:Brand 
                                            AND ONTAP__Account__c=:caso.AccountId]);
            
            caso.ISSM_BrandProduct__c = Brand;
            caso.Equipment_model__c = Model;
            caso.ISSM_SerialNumber__c = SerialNumber;
            caso.CS_Serial_number_of_the_equipment__c = EquipmentNumber;
            caso.ISSM_CaseReason__c = Description;
            caso.CS_skuProduct__c = acc.V360_MaterialNumber__c;
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            update caso;
            
            if(recType.equals(recordTypeRetire))
            {
                CS_CASE_COOLERS_CALLOUT_cls.sendRequest(caso.id);
            }
            success = true;
        }
        catch(Exception ex)
        {
            success = false;
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Stack trace: ' + ex.getStackTraceString());
        } 
        return success;
    }
}