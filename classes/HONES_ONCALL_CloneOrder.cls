/* ----------------------------------------------------------------------------
 * AB InBev :: OnCall
 * ----------------------------------------------------------------------------
 * Clase: HONES_CloneOrder.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 23/09/2019           Fanny Becerra      	Creación de métodos para clonación manual de órdenes
 */

public class HONES_ONCALL_CloneOrder {
    @AuraEnabled
    public static List<ONCALL__Call__c> getCalls(String accountValue){
        System.debug(accountValue);
        //New lists
        List<ONCALL__Call__c> calls = new List<ONCALL__Call__c>();

        //Trae el Id de las llamadas relacionadas al Id de la cuenta ingresada
        List<ONCALL__Call__c> allCalls = [SELECT Id, Name FROM ONCALL__Call__c WHERE ONCALL__POC__c = : accountValue];

        System.debug(allCalls);
        return allCalls;
    }
    
    @AuraEnabled
    public static List<ONTAP__Order__c> getOrders(String callValue){
        //New lists
        List<ONTAP__Order__c> orders = new List<ONTAP__Order__c>();

        //Trae el Id de las ordenes relacionadas al Id de la llamada ingresada
        List<ONTAP__Order__c> allOrders = [SELECT Id, Name FROM ONTAP__Order__c WHERE ONCALL__Call__c = : callValue];

        return allOrders;
    }

    @AuraEnabled
    public static List<ONTAP__Order_Item__c> getItems(String orderValue){
        //New lists
        List<ONTAP__Order_Item__c> items = new List<ONTAP__Order_Item__c>();

        //Trae el Id, nombre, cantidad, productId de los items relacionados al Id de la orden ingresada
        List<ONTAP__Order_Item__c> allItems = [SELECT Id, Name, ONTAP__Product_Description__c, ONTAP__ActualQuantity__c, ONTAP__ProductId__c FROM ONTAP__Order_Item__c WHERE ONTAP__CustomerOrder__c = : orderValue];

        return allItems;
    }

}