/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_COOLERS_CALLOUT_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 22/01/2019           Debbie Zacarias        Creation of the class for lightning component control
*/

public class CS_CASE_COOLER_cls {
    
    /**
	* method to find materials list for lightning.
	* @author: d.zacarias.cantillo@accenture.com
	* @param void
	* @return List of materials
	*/ 
    @AuraEnabled
    public static List<String> getPickListValues()
    {
        List<String> pickListValuesList= new List<String>();
        String prodType = Label.CS_Cooler_Filter;
        User currentUser = [select Id, username, Country__c from User where Id = :UserInfo.getUserId()];
        
        String country = '';
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        
        if(currentUser.Country__c.equals(countryESV))
        {
            country = Label.CS_CountryCodeSV;
        }
        else if(currentUser.Country__c.equals(countryHN))
        {
            country = Label.CS_CountryCodeHN;
        }

        try
        {            
            List<ONTAP__Product__c> acc = [SELECT ONTAP__MaterialProduct__c, ONTAP__ProductType__c  
                                           FROM ONTAP__Product__c 
                                           WHERE ONTAP__ProductType__c =:prodType AND ONTAP__Country_Code__c =: country];
            
            pickListValuesList.add('');
            
            for(ONTAP__Product__c item : acc)
            {
                pickListValuesList.add(item.ONTAP__MaterialProduct__c);
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.getPickListValues  Stack trace: ' + ex.getStackTraceString());
        } 
        return pickListValuesList;
    }
    
    /**
	* method for gets material for case 
	* @author: d.zacarias.cantillo@accenture.com
	* @param String idCase to query product code
	* @return Product with the info to display 
	*/ 
    @AuraEnabled
    public static ONTAP__Product__c SetProductCase(String idCase)
    {
        Case cas = ([SELECT id, CS_skuProduct__c 
                     FROM Case 
                     WHERE id =: idCase]);
        
        ONTAP__Product__c product = ([SELECT ONTAP__MaterialProduct__c, ONTAP__ProductCode__c 
                                      FROM ONTAP__Product__c 
                                      WHERE ONTAP__ProductCode__c =: cas.CS_skuProduct__c]);
        
        return product;
    }
    
    /**
	* method for update material case.
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase that is updated 
	* @param String for query material 
	* @return product with information to display
	*/ 
    @AuraEnabled
    public static ONTAP__Product__c GetProductCase(String idCase, String product)
    {
        ONTAP__Product__c sku;
        try
        {
            Case caso = [SELECT id 
                         FROM Case 
                         WHERE id=:idCase];
            
            if(caso!=null)
            {
                sku = [SELECT ONTAP__ProductCode__c, ONTAP__MaterialProduct__c 
                       FROM ONTAP__Product__c 
                       WHERE ONTAP__MaterialProduct__c=:product];
                
                if(sku!=null)
                {
                    caso.CS_skuProduct__c = sku.ONTAP__ProductCode__c;
                }
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.GetProductCase  Stack trace: ' + ex.getStackTraceString());            
        }
		return sku;
    }
    
    /**
	* method for update material for case 
	* @author: d.zacarias.cantillo@accenture.com
	* @param String idCase to query product code
	* @return Product with the info to display 
	*/ 
    @AuraEnabled
    public static Boolean UpdateCase(String idCase, String codeProduct)
    {
        Boolean success = true;
        try
        {
            Case cas = ([SELECT id, CS_skuProduct__c 
                     FROM Case 
                     WHERE id =: idCase]);
            
            cas.CS_skuProduct__c = codeProduct;
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            update cas;
            CS_CASE_COOLERS_CALLOUT_cls.sendRequest(cas.Id);
        }
        catch(Exception ex)
        {  
            success = false;
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Cause: '   + ex.getCause());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLER_cls.UpdateCase  Stack trace: ' + ex.getStackTraceString());           
        }
        return success;
        
    }
}