/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:HONES_HeaderTelecollectionContr.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 17/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
 */
public class HONES_HeaderTelecollectionContr {
    /*
     * Method that the total number of calls returned
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return Integer 
	*/
    @AuraEnabled
    public static Integer callTotal(){
        
        Integer total = 0;
        
        User u = [select alias from user where id=:userinfo.getuserid()];
        List<ONCALL__Call__c> newUpdateIncomplete = [SELECT Id, Name, ONCALL__Call_Status__c FROM ONCALL__Call__c WHERE OwnerId =: u.Id AND ONCALL__Call_Type__c =: LABEL.TELECOLLECTION_VALIDATION];
        
        for(ONCALL__Call__c i: newUpdateIncomplete){
            total = total + 1;
        }
        return total;
    }
    /*
     * Method that the total number of calls pending returned
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return Integer 
	*/    
    @AuraEnabled
    public static Integer callPending(){
        
        Integer pending = 0;
        User u = [select alias from user where id=:userinfo.getuserid()];
        List<ONCALL__Call__c> newUpdateIncomplete = [SELECT Id, Name, ONCALL__Call_Status__c FROM ONCALL__Call__c WHERE OwnerId =: u.Id AND ONCALL__Call_Type__c =: LABEL.TELECOLLECTION_VALIDATION];
        
        for(ONCALL__Call__c i: newUpdateIncomplete){
            if(i.ONCALL__Call_Status__c == LABEL.PENDING_TO_CALL_HEAD){
                pending = pending + 1;
            }
        }
        return pending;
    }
    
    /*
     * Method that the total number of calls Incomplete
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return Integer 
	*/    
    @AuraEnabled
    public static Integer callIncomplete(){
        
        Integer incomplete = 0;
        User u = [select alias from user where id=:userinfo.getuserid()];
        
        List<ONCALL__Call__c> newUpdateIncomplete = [SELECT Id, Name, ONCALL__Call_Status__c FROM ONCALL__Call__c WHERE OwnerId =: u.Id AND ONCALL__Call_Type__c =: LABEL.TELECOLLECTION_VALIDATION];
        System.debug(newUpdateIncomplete);
        Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Status__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getLabel(),v.getValue());
            }
        for(ONCALL__Call__c i: newUpdateIncomplete){
            if(i.ONCALL__Call_Status__c == Call_StatusPick_values.get(LABEL.INCOMPLETE_HEAD)){
                incomplete = incomplete + 1;
            }
        }
        
        return incomplete;
    }
    
    /*
     * Method that the total number of calls complete
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return Integer 
	*/    
    @AuraEnabled
    public static Integer callComplete(){
        
        Integer complete = 0;
        User u = [select alias from user where id=:userinfo.getuserid()];
        
        List<ONCALL__Call__c> newUpdateIncomplete = [SELECT Id, Name, ONCALL__Call_Status__c FROM ONCALL__Call__c WHERE OwnerId =: u.Id AND ONCALL__Call_Type__c = : LABEL.TELECOLLECTION_VALIDATION];
       
        
        for(ONCALL__Call__c i: newUpdateIncomplete){
            if(i.ONCALL__Call_Status__c == LABEL.COMPLETE_HEAD){
                complete = complete + 1;
            }
        }
        
        return complete;
    }
    
}