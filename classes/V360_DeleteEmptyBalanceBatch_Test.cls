/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteEmptyBalanceBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
public class V360_DeleteEmptyBalanceBatch_Test {
    /**
   * Method test for delete all the empty balance in V360_DeleteEmptyBalanceBatch.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testBatch(){
    Id V360_EmptyBalanceRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
    ONTAP__EmptyBalance__c emp = new ONTAP__EmptyBalance__c (RecordTypeId=V360_EmptyBalanceRT);
    insert emp;
    V360_DeleteEmptyBalanceBatch obj01 = new V360_DeleteEmptyBalanceBatch();
    Database.executeBatch(obj01, 200);
}
}