/* Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 26/09/2019           Rogelio Jiménez          Creation of methods.
*/
public  class HONES_ONCALL_ProductsJsonBatch {
     /**
    * Methods getters and setters for entity HONES_ONCALL_ProductsJsonBatch
    * Created By: rjimenez@ts4.mx
    * @param void
    * @return void
    */
    public List<HONES_ONCALL_ProductsJson> orders {get; set;}

    public HONES_ONCALL_ProductsJsonBatch(){
        orders = new  List<HONES_ONCALL_ProductsJson>();
    }
}