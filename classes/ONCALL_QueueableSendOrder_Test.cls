/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase:ONCALL_QueueableSendOrder_Test.apxc
* Versión: 1.0.0.0
* 
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 15/05/2019    Gabriel Garcia	  gabriel.e.garcia@accenture.com     Test class of ONCALL_QueueableSendOrder
*/
@isTest
public class ONCALL_QueueableSendOrder_Test{
     /*
    * Test method that invoke a Queueable
    * Created By:gabriel.e.garcia@accenture.com
    * @params Database.BatchableContext c
    * @return void 
    */
    @isTest
    public static void test_Queue() {
        
        Account acc = new Account(Name ='Ricardo Milos Test', ONTAP__SalesOgId__c = 'CS01');
        insert acc;
        
        List<ONTAP__Order__c> lstOrd = new List<ONTAP__Order__c>();        
        ONTAP__Order__c ord = new ONTAP__Order__c();
        ord.ONCALL__SAP_Order_Item_Counter__c = 2;
        ord.ONCALL__Total_Order_Item_Quantity__c = 2;
        ord.ONCALL__OnCall_Status__c = 'Closed';
        ord.ISSM_PaymentMethod__c = 'CASH';
        ord.ONTAP__DocumentationType__c = 'ZSV1';
        ord.ONTAP__OrderAccount__c = acc.Id;
        ord.ONTAP__DeliveryDate__c = System.now() + 3;   
        
        Test.startTest();        
        
        ONCALL_SendOrder_Batch obj01 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC;
	    obj01.execute(BC, lstOrd);
  		Database.executeBatch(obj01, 200);
        
        ONCALL_SendOrder_Batch obj02 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC2;
	    obj02.execute(BC2, lstOrd);
  		Database.executeBatch(obj02, 200);
        
        ONCALL_SendOrder_Batch obj03 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC3;
	    obj03.execute(BC3, lstOrd);
  		Database.executeBatch(obj03, 200);
        
        ONCALL_SendOrder_Batch obj04 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC4;
	    obj04.execute(BC4, lstOrd);
  		Database.executeBatch(obj04, 200);
        
        ONCALL_SendOrder_Batch obj05 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC5;
	    obj05.execute(BC5, lstOrd);
  		Database.executeBatch(obj05, 200);
                
        System.enqueueJob(new ONCALL_QueueableSendOrder());
        Test.stopTest();
                
    }
    
    @isTest
    public static void test_Queue2() {               
        Test.startTest();                                
        System.enqueueJob(new ONCALL_QueueableSendOrder());
        Test.stopTest();
                
    }
}