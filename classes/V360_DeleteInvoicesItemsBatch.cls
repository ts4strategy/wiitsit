/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteInvoicesItemsBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteInvoicesItemsBatch implements Database.Batchable<sObject>{
    
    global Boolean hasErrors {get; private set;}
	
    /**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteInvoicesItemsBatch()
    {  
        this.hasErrors = false;
    }
    
    /**
   * Execute the Query for the Invoice Item objects.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {        
        Id V360_RT = Schema.SObjectType.ONCALL__Invoice_Item__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.INVOICE_ITEM_V360_RECORDTYPE_NAME).getRecordTypeId();

        system.debug('Processing start method'); 

        // Query string for batch Apex
        String query = ''; 
        Date todayDate = System.Today();

        if(System.Test.isRunningTest()){
            query += 'SELECT Id FROM ONCALL__Invoice_Item__c';
        }else{
            query += 'SELECT Id FROM ONCALL__Invoice_Item__c WHERE RecordTypeId =:V360_RT AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Method for delete all the Invoice Item objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<ONCALL__Invoice_Item__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<ONCALL__Invoice_Item__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}