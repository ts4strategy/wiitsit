/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_TYPIFICATION_TRIGGERHANDLER_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 21/01/2019     Carlos Leal             Creation of methods.
 */
@isTest
public class CS_CASE_TYPIFICATION_TRIGGERHANDLER_Test {

    /**
    * Test method for set up data
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void setUpTestData(){
        
    //Test.startTest();
    Account acc = new Account();
    acc.Name = 'Test';
    insert acc;
    Entitlement ent = new Entitlement();
    ent.Name = 'Test';
    ent.AccountId = acc.Id;
   	insert ent;
    System.debug('Ent ' +  ent);
    ISSM_TypificationMatrix__c typ = new ISSM_TypificationMatrix__c();
    typ.CS_Entitlement_Name__c = 'Test';
    typ.CS_Days_to_End__c = 1.00;
    insert typ;
    System.debug('Typ: ' + typ);
    //Test.stopTest();
  }
    
    @isTest static void test_addRelatedEntitElement(){
        Account acc = new Account();
    	acc.Name = 'Test';
    	insert acc;
        List<Entitlement> enList = new List<Entitlement>();
        Entitlement en = new Entitlement(Name='Test',AccountId = acc.Id);
        enList.add(en);
        insert enList;
        List<ISSM_TypificationMatrix__c> lstprod = new List<ISSM_TypificationMatrix__c>();
        ISSM_TypificationMatrix__c typ = new ISSM_TypificationMatrix__c(CS_Entitlement_Name__c = 'Test', CS_Days_to_End__c = 1.00);
        lstprod.add(typ);
        insert typ;
        CS_CASE_TYPIFICATION_TRIGGERHANDLER obj01 = new CS_CASE_TYPIFICATION_TRIGGERHANDLER();
        obj01.addRelatedEntitlements(lstprod);
    }
}