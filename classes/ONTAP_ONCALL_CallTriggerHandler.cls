/****************************************************************************************************
   General Information
   -------------------
   author: Rogelio Jiménez
   email: rjimenez@ts4.mx
   company: TS4
   Project: I
   Customer: AbInBev Grupo Modelo
   Description: Class to control events in object  ONTAP__ONCALL_Call__C

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       25-09-2019        Rogelio Jiménez (RJ)       Creation Class
****************************************************************************************************/
public with sharing class ONTAP_ONCALL_CallTriggerHandler  extends TriggerHandlerCustom{
   
    private Map<Id, ONTAP__ONCALL_Call__C> newMap;
    private Map<Id, ONTAP__ONCALL_Call__C> oldMap;
    private List<ONTAP__ONCALL_Call__C> newList;
    private List<ONTAP__ONCALL_Call__C> oldList;
    
    public ONTAP_ONCALL_CallTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__ONCALL_Call__C>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__ONCALL_Call__C>) Trigger.oldMap;
        this.newList = (List<ONTAP__ONCALL_Call__C>) Trigger.new;
        this.oldList = (List<ONTAP__ONCALL_Call__C>) Trigger.old;        
    }
    
    public override void afterInsert(){
        updateCall(newList);              
    }
    
    public override void afterUpdate(){
    }
  
    public override void beforeInsert(){
        
    }
    
    public override void beforeUpdate(){
        
    }

    /**
    * Copy values from Object ONTAP__ONCALL_Call__C to ONCALL__Call__c
    * @param    newList    A new list of the inserted ojects
    * @return   void
    * @since    1.0
    */
    public void updateCall(list<ONTAP__ONCALL_Call__C> newList){
        set<id> idCalls = new set<Id>();
        list<ONCALL__Call__c> callsUpdate = new list<ONCALL__Call__c>();
        list<ONTAP__ONCALL_Call__C> onTapCallDel = new list<ONTAP__ONCALL_Call__C>();
        map<Id, ONCALL__Call__c> mapCall = new map<Id, ONCALL__Call__c>();
        for(ONTAP__ONCALL_Call__C onTapCall : newList){
            idCalls.add(onTapCall.ONTAP__Task_Id__c);
        }
        
        for(ONCALL__Call__c call : [SELECT Id,ISSM_CallResult__c FROM ONCALL__Call__c WHERE Id IN: idCalls]){
            if(!mapCall.containsKey(call.Id)){
                mapCall.put(call.Id, call);
            }
        }

        for(ONTAP__ONCALL_Call__C onTapCall : newList){
            if(mapCall.containsKey(onTapCall.ONTAP__Task_Id__c)){
                ONCALL__Call__c callUpdate = mapCall.get(onTapCall.ONTAP__Task_Id__c);
                callUpdate.ISSM_CallResult__c = onTapCall.Call_Status__c;
                callsUpdate.add(callUpdate);
                ONTAP__ONCALL_Call__C auxOnTapCall = new ONTAP__ONCALL_Call__C(Id = onTapCall.Id);
                onTapCallDel.add(auxOnTapCall);
            }
        }

        try{
            update callsUpdate;
        }catch(exception e){
                System.debug('ONTAP_ONCALL_CallTriggerHandler:updateCall::error::' + e.getMessage());
        }finally{
            delete onTapCallDel;
        }
    }
}