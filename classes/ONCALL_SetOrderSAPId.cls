/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONCALL_SetOrderSAPId.apxc
 * Versión: 1.0.0.0
 * 
 * Clase destinada a guardar el valor del ID de SAP en la orden
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 19/12/ 2018     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación de la clase  
 */
public class ONCALL_SetOrderSAPId {
    public static void setId(String json, String sapResponse, String orderID){
        System.debug('Set SAP Order ID Class!');
        System.debug('Order request: ' + json);
        System.debug('SAP response: ' + sapResponse);
        System.debug('Order Id: ' + orderID);
        
    }
}