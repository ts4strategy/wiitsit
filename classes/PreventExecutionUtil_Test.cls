/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: PreventExecutionUtil_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 28/01/2019       Carlos Leal            Creation of methods.
 */
@isTest
public class PreventExecutionUtil_Test {
    
   /**
   * Method test for Products
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case1(){
        
        List<ONTAP__Product__c> objList = new List<ONTAP__Product__c>();
        ONTAP__Product__c objProd = new ONTAP__Product__c();
        objProd.ISSM_Organitation_Product__c = GlobalStrings.HONDURAS_ORG_CODE;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateProductWithOutId ( objList);  
        Map<Id, ONTAP__Product__c> objMap = new Map<Id, ONTAP__Product__c>();
        for(ONTAP__Product__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateProductWithId ( objMap);
    }
    
    /**
   * Method test for Open Items
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case2(){
        
        List<ONTAP__OpenItem__c> objList = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c objProd = new ONTAP__OpenItem__c();
         Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        objProd.RecordTypeId = V360_OpenItemRT;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateOpenItemWithOutId ( objList);  
        Map<Id, ONTAP__OpenItem__c> objMap = new Map<Id, ONTAP__OpenItem__c>();
        for(ONTAP__OpenItem__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateOpenItemWithId ( objMap);
    }
    
    /**
   * Method test for Kpis
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case3(){
        
        List<ONTAP__KPI__c> objList = new List<ONTAP__KPI__c>();
        ONTAP__KPI__c objProd = new ONTAP__KPI__c();
        Id V360_KPIRT = Schema.SObjectType.ONTAP__KPI__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.KPI_V360_RECORDTYPE_NAME).getRecordTypeId();
        objProd.ONTAP__Categorie__c = 'Volume';
        objProd.Account_Sap_Id__c = 'Test1';
        objProd.RecordTypeId = V360_KPIRT;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateKPIWithOutId ( objList);  
        Map<Id, ONTAP__KPI__c> objMap = new Map<Id, ONTAP__KPI__c>();
        for(ONTAP__KPI__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateKPIWithId ( objMap);
    }
    
    /**
   * Method test for Invoices
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case4(){
        
        List<ONCALL__Invoice__c> objList = new List<ONCALL__Invoice__c>();
        ONCALL__Invoice__c objProd = new ONCALL__Invoice__c();
        Id V360_InvoiceRT = Schema.SObjectType.ONCALL__Invoice__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.INVOICE_V360_RECORDTYPE_NAME).getRecordTypeId();
        objProd.RecordTypeId = V360_InvoiceRT;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateInvoiceWithOutId ( objList);  
        Map<Id, ONCALL__Invoice__c> objMap = new Map<Id, ONCALL__Invoice__c>();
        for(ONCALL__Invoice__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateInvoiceWithId ( objMap);
    }
	
    /**
   * Method test for Empty Balance
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case5(){
        
        List<ONTAP__EmptyBalance__c> objList = new List<ONTAP__EmptyBalance__c>();
        ONTAP__EmptyBalance__c objProd = new ONTAP__EmptyBalance__c();
        Id V360_EmptyRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
        objProd.RecordTypeId = V360_EmptyRT;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateEmptyWithOutId ( objList);  
        Map<Id, ONTAP__EmptyBalance__c> objMap = new Map<Id, ONTAP__EmptyBalance__c>();
        for(ONTAP__EmptyBalance__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateEmptyWithId ( objMap);
    }
    
    /**
   * Method test for Account Assets
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case6(){
        
        List<ONTAP__Account_Asset__c> objList = new List<ONTAP__Account_Asset__c>();
        ONTAP__Account_Asset__c objProd = new ONTAP__Account_Asset__c();
        Id V360_AccountAssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
        objProd.RecordTypeId = V360_AccountAssetRT;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateAccountAssetWithOutId ( objList);  
        Map<Id, ONTAP__Account_Asset__c> objMap = new Map<Id, ONTAP__Account_Asset__c>();
        for(ONTAP__Account_Asset__c objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateAccountAssetWithId ( objMap);
    }
    
    /**
   * Method test for Calls
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case7(){
        
        List<ONCALL__Call__c> objList = new List<ONCALL__Call__c>();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id V360_RouteRT = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.RECORDTYPE_VISITCONTROL_ROUTE).getRecordTypeId();
        
        User u = [Select Id FROM User Limit 1];
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId);
        insert szz;
		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id,V360_Type__c='Presales');
        insert sz;
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,Supervisor__c=u.id,VisitControl_AgentByZone__c =sz.Id, ServiceModel__c='Presales',VisitControl_Bypass__c = true,RecordTypeId = V360_RouteRT);
        insert rt; 
        ONTAP__Tour__c tt = New ONTAP__Tour__c(Route__c = rt.Id, ONTAP__TourId__c = 'T-000001160',ONTAP__TourDate__c = Date.today());
        insert tt;       
        ONCALL__Call__c jj= New ONCALL__Call__c(ONCALL__Date__c= Date.today(), CallList__c = tt.id);
        objList.add(jj);
        PreventExecutionUtil.validateCallWithOutId ( objList); 
    }
    
    /**
   * Method test for Tours
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case8(){
        
        List<ONTAP__Tour__c> objList = new List<ONTAP__Tour__c>();
        User u = [Select Id FROM User Limit 1];
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,Supervisor__c=u.id, ServiceModel__c='Presales',VisitControl_Bypass__c = true);
        insert rt;
        ONTAP__Tour__c objProd = New ONTAP__Tour__c(Route__c = rt.Id, ONTAP__TourId__c = 'T-000001160',ONTAP__TourDate__c = Date.today());//, VisitPlan__c = vp.Id); 
        Id V360_TourBDR = Schema.SObjectType.ONTAP__Tour__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.BDR).getRecordTypeId();
        objProd.RecordTypeId = V360_TourBDR;
        objList.add(objProd);
        insert objList;
        PreventExecutionUtil.validateTourWithOutId ( objList);
    }
    
    /**
   * Method test for Accounts
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testMethod_Case9(){
        
        List<Account> objList = new List<Account>();
        Account objProd = new Account();
        objProd.Name = 'Test';
        objProd.ONCALL__Country__c = GlobalStrings.HONDURAS_COUNTRY_CODE;
        objProd.ONTAP__SalesOgId__c = GlobalStrings.HONDURAS_COUNTRY_CODE;
        objList.add(objProd);
        insert objList;
        Map<Id, Account> objMap = new Map<Id, Account>();
        for(Account objL : objList){
            objMap.put(objL.Id , objL);
        }
        PreventExecutionUtil.validateAccountWithIdByContry ( objMap);
        PreventExecutionUtil.validateAccountWithIdByORG ( objMap);
    }
}