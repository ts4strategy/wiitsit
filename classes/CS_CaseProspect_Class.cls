/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CaseProspect_Class.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 23/04/2019      Jose Luis Vargas        Creation of the class for re-send prospect to mulesoft
 *                                             
*/

public class CS_CaseProspect_Class 
{
    /**
    * method for send to mulesoft the prospect 
    * @author: jose.l.vargas.lara@accenture.com
    * @param IdCase to get Account Id
    * @return Boolean if process is correct
    */ 
    @AuraEnabled
    public static boolean SendCaseProspect(string IdCase)
    {
        List<Id> lstProspect = new List<Id>();
        /*19092019 - 16:26*/
        System.debug('CS_CaseProspect_Class::IdCase: '+IdCase);
        Case oProspectCase = new Case();
        try
        {
            /* Se obtiene el Id de Prospecto del caso */
            oProspectCase = [SELECT AccountId, Status FROM Case WHERE Id =: IdCase];
            lstProspect.add(oProspectCase.AccountId);
            /*
            Modificación 27/09/2019
            Se agrega condición para leer status (Status) 'Completed' desde el objeto Case
            y solo entonces realizar el Callout*/
			System.debug('CS_CaseProspect_Class::StatusComparision::Case|Label: '+ oProspectCase.Status+' | '+Label.CS_CaseStatus_Completed);
            if(oProspectCase.Status == Label.CS_CaseStatus_Completed) {
                //Se ejecuta el callout para envio a Mulesoft
                CS_Prospect_Callout_Class.SendProspectMuleSoft(lstProspect);
                return true;
            } else {
            	return false;    
            }
            
        }
        catch(Exception ex)
        {
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Message: ' + ex.getMessage());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Cause: ' + ex.getCause());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Stack trace: ' + ex.getStackTraceString());
            return false;
        }
    }
    
    /**
    * method to get the flag to send prospect 
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Boolean
    */ 
    @AuraEnabled
    public static boolean SendProspectMdWeb()
    {
        System.debug('SM::Llamada SendProspectMdWeb');
        CS_Prospect_Settings__c prospectSetting = CS_Prospect_Settings__c.getOrgDefaults();
        return prospectSetting.CS_Send_MdWeb__c;
    }
}