/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_PartnerList.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public with sharing class HONES_ONCALL_PartnerList {
     /**
    * Methods getters and setters for entity HONES_ONCALL_PartnerList
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String role{get; set;}
    public String numb{get; set;}
    public String itmNumber{get; set;}
    public Domicilio domicilios {get;set;}

    public class Domicilio{
        public String NAME {get;set;}
        public String NAME_3 {get;set;}
        public String STREET {get;set;}
        public String COUNTRY {get;set;}
        public String POSTL_CODE {get;set;}
        public String CITY {get;set;}
        public String DISTRICT {get;set;}
        public String REGION {get;set;}
        public String TELEPHONE {get;set;}
        public String TELEPHONE2 {get;set;}
        public String LANGU {get;set;}
        public String TRANSPZONE {get;set;}

    }
}