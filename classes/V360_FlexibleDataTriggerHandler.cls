/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_FlexibleDataTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_FlexibleDataTriggerHandler extends TriggerHandlerCustom{
	
    private Map<Id, v360_FlexibleData__c> newMap;
    private Map<Id, v360_FlexibleData__c> oldMap;
    private List<v360_FlexibleData__c> newList;
    private List<v360_FlexibleData__c> oldList;
    
    public V360_FlexibleDataTriggerHandler() {
        
        this.newMap = (Map<Id, v360_FlexibleData__c>) Trigger.newMap;
        this.oldMap = (Map<Id, v360_FlexibleData__c>) Trigger.oldMap;
        this.newList = (List<v360_FlexibleData__c>) Trigger.new;
        this.oldList = (List<v360_FlexibleData__c>) Trigger.old;
    }
    /**
    * method wich start every time before record is insert 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public override void beforeInsert(){
         //relateAccountsToFlexibleData(this.newlist);
    }
    
            /**
    * method wich is executed every time a coverage is inserted
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterInsert(){   
        if(!System.isFuture()){
        	relateAccountsToFlexibleData(this.newMap.keySet());
        }
    }
    
    /**
    * method wich start every time before record is updated 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void 
    */
    public override void beforeUpdate(){
        //relateAccountsToFlexibleData(this.newlist);
    }
    
     /**
    * method wich is executed every time a coverage is updated
    * Created By: c.leal.beltran@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Last Modify Date: 28/03/2019
    * @param New values of Product modified
    * @return void
    */
    
    public override void afterUpdate(){
        if(!System.isFuture()){
        	relateAccountsToFlexibleData(this.newMap.keySet());
        }
    }
    
    /**
   	* Method for relate an Account to flexible data.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @Future
    public static void relateAccountsToFlexibleData(Set<Id> setIds){
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        List<V360_FlexibleData__c> newlist = new List<V360_FlexibleData__c>();
        List<V360_FlexibleData__c> listtoUpdate = new List<V360_FlexibleData__c>();
        newlist = [SELECT Id, SAP_Account_ID__c FROM V360_FlexibleData__c WHERE Id IN: setIds];
        
        for (v360_FlexibleData__c accFlex:newlist){
            if(accFlex.SAP_Account_ID__c!=NULL){
                SAPNumsToRelate.add(accFlex.SAP_Account_ID__c);
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
      
        
        for (v360_FlexibleData__c finalFlex:newlist){
            Account acc = accReference.get(finalFlex.SAP_Account_ID__c);
            if(acc!= NULL){           
                finalFlex.v360_Account__c = acc.Id;
                listtoUpdate.add(finalFlex);
            }
            if(!listtoUpdate.isEmpty()){
                update listtoUpdate;
            }
        }
    }
}