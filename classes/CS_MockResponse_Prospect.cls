/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_MockResponse_Prospect.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 25/04/2019      Jose Luis Vargas        Creation of the class mock for test callout prospect
 *                                             
*/

@isTest
global class CS_MockResponse_Prospect implements HttpCalloutMock
{
    /**
    * method for generate response OK to test class for prospect
    * @author: jose.l.vargas.lara@accenture.com
    * @param lstProspectId List with Id of prospect
    * @return Void
    */ 
    global HTTPResponse respond(HTTPRequest req)
    {
        HttpResponse oResponse = new HttpResponse();
       
        oResponse.setHeader('Content-Type', 'application/json');
        oResponse.setStatus('OK');
        oResponse.setStatusCode(200);
        oResponse.setBody('{"resultSet1": [{"NumeroIdentificacion": "10411692","MensajeError": "True","CodigoError": "0"}],"updateCount1": 1,"updateCount2": 1}');
        
        return oResponse;
    }
}