/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_CopyInfoReference_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest public class VisitControl_CopyInfoReference_Test {
	
    /**
    * Test method for copy the ids from a client referenced
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void testCopyInformation(){
        VisitControl_CopyInfoReferenceClient.CopyInformation(new List<Id>());
    }
    
    /**
    * Test method for copy the information of routes from a client referenced 
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void testCopyCustomerReferenceRoutes(){
        Account acc =  new Account(Name='Test',ONTAP__SAP_Number__c='1234567890');
        insert acc;
        Account acc1 =  new Account(Name='Test',V360_ReferenceClient__c=acc.Id,ONTAP__SAP_Number__c='1234567891');
        insert acc1;
        
        
        
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;

		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id);
        insert sz;
                
        
        User u = [Select Id FROM User Limit 1];  
        
        
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,VisitControl_AgentByZone__c =sz.Id, ServiceModel__c='Presales',VisitControl_Bypass__c = true);
        insert rt;
        
        
        AccountByRoute__c accByRouteNew = new AccountByRoute__c (Route__c=rt.Id,Account__c=acc.Id,Monday__c=true);
        insert accByRouteNew;
        
        Map<Id,Account>mapOb =new Map<Id,Account>([SELECT ID,V360_ReferenceClient__c FROM ACCOUNT WHERE ID =:acc1.Id]);
        
        
        test.startTest();
        VisitControl_CopyInfoReferenceClient.copyCustomerReferenceRoutes(mapOb);
        
        acc.V360_SalesZoneAssignedBDR__c = sz.Id;
        acc.V360_SalesZoneAssignedTelesaler__c = sz.Id;
        acc.V360_SalesZoneAssignedPresaler__c = sz.Id;
        acc.V360_SalesZoneAssignedCoolers__c = sz.Id;
        acc.V360_SalesZoneAssignedCredit__c = sz.Id;
        acc.V360_SalesZoneAssignedTellecolector__c = sz.Id;
        update acc;
        
        VisitControl_CopyInfoReferenceClient.copyCustomerInterlocutors(mapOb);
        test.stopTest();
    }
}