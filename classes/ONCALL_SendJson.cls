/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONCALL_SendJson.apxc
 * Versión: 1.0.0.0
 * 
 * Clase destinada para el envío de órdenes a SAP
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 19/12/ 2018     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación de la clase  
 */
public class ONCALL_SendJson implements Database.AllowsCallouts{
	
    /**
    * Method that modify deserealize the JSON 
    * Created By: heron.zurita@accenture.com
    * @param String json_order,String orderID
    * @return void
    */
    //@Future(callout=true)
   /* public static void send_Json(String json_order, String orderID){
        string json = json_order;
        string orderSFDCID = orderID;                            
        
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt WHERE DeveloperName = 'Create_SAP_Order'];
        
        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(json);
        System.debug('json sent: ' +json);
        HTTPResponse authresp = new HTTPResponse();
        authresp = h.send(httpReq);
        System.debug('response status: ' +authresp);
        
        if(authresp.getStatusCode() == 200){
            System.debug('Success: ' + authresp.getBody());
            
            ONCALL_SAP_OrderResponse listToSave = ONCALL_SAP_OrderResponse.parse(authresp.getBody());
            
            ONTAP__Order__c order = new  ONTAP__Order__c();
            order.id = orderSFDCID;
			order.ONCALL__SAP_Order_Number__c = listToSave.id;
            if(listToSave.etReturn!=null){
                if(listToSave.etReturn.Item.type_Z!=null&&listToSave.etReturn.Item.message!=null){
                    order.ONCALL__SAP_Order_Response__c = listToSave.etReturn.Item.type_Z + ' - ' + listToSave.etReturn.Item.message;
                }
            }            
			
            order.ONCALL__OnCall_Status__c = 'Closed';
            System.debug('order to update: ' + order);
            update order;
            
        } else{
            System.debug('Connection Error message: ' + authresp.getBody());
        }
    }*/
    
    /**
    * Method that modify deserealize the JSON 
    * Created By: heron.zurita@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com, RJP
    * Modify Date 2019-03-20
    * @param String json_order,String orderID
    * @return void
    * @Comments: RJP: MERGE-HONES :  this is PANAMA Original method
    */
    public static void send_Json(Map<String, JSONGenerator> mapOrderGen){
        User u = [SELECT id, username, Country FROM User where Id= :UserInfo.getUserId()];
        system.debug('User.Country: ' + u.Country);
        
        List<End_Point__mdt> endpoint =
            [SELECT DeveloperName, MasterLabel, Token__c, URL__c, Timeout__c
            FROM End_Point__mdt 
            WHERE MasterLabel = 'Create SAP Order' //RJP rjimenez@ts4.mx changed to read MasterLabel field
            AND Country__c = :u.Country];
        system.debug('endpoint mdt: ' + endpoint);
        List<ONTAP__Order__c> listOrder = new List<ONTAP__Order__c>();            
        for(String key : mapOrderGen.keySet()){
            String json = mapOrderGen.get(key).getAsString().replaceAll('\\n','');        	   
            
            ONTAP__Order__c order = new  ONTAP__Order__c(ONCALL__SAP_Order_Response__c='');                
            order.id = key;
            order.ISSM_OrderJSONRequest__c = json; //Added 2019-AUG-05 - drs@avx. For tracing purposes, we store the JSON Request sent to the CreateOrder webservice
            
            Http h = new Http();
            HttpRequest httpReq = new HttpRequest();
            system.debug('endpoint.URL: ' + endpoint[0].URL__c);
            httpReq.setEndpoint(endpoint[0].URL__c);
            httpReq.setTimeout(endpoint[0].Timeout__c == NULL ? 15000 : (Integer)endpoint[0].Timeout__c);
            httpReq.setMethod('POST');
            httpReq.setHeader('Content-Type','application/json;charset=utf-8');
            httpReq.setHeader('bearer', endpoint[0].Token__c);
            httpReq.setBody(json);
            System.debug('ORDER CREATE json request : ' +json);
            HTTPResponse authresp = new HTTPResponse();
            try
            {
            	authresp = h.send(httpReq);
                System.debug('response status: ' +authresp);                        
            
                if(authresp.getStatusCode() == 200){
                    System.debug('ORDER CREATE json response : ' + authresp.getBody());
                    order.ISSM_OrderJSONResponse__c = authresp.getBody(); //Added 2019-AUG-05 - drs@avx. For tracing purposes, we store the JSON response we receive from the CreateOrder webservice
                    ONCALL_SAP_OrderResponse listToSave = ONCALL_SAP_OrderResponse.parse(authresp.getBody());                                
                    
                    if(listToSave.orderId != null && listToSave.orderId != ''){
                        order.ONCALL__SAP_Order_Number__c = listToSave.orderId;
                        for(ONCALL_SAP_OrderResponse.cls_etReturn etR: listToSave.etReturn ){ // ADDED, now we receive a list of messages drs@avx
                            if (etR.message.contains(order.ONCALL__SAP_Order_Number__c)) {
                                order.ONCALL__SAP_Order_Response__c = etR.type_Z + ' - ' + etR.message;
                            }
                        }
                    
                    } else { // we store every message received. Watch for 255 limit drs@avx
                        for(ONCALL_SAP_OrderResponse.cls_etReturn etR: listToSave.etReturn ){ // ADDED, now we receive a list of messages drs@avx
							order.ONCALL__SAP_Order_Response__c += etR.type_Z + ' - ' + etR.message + '; ';
                        }
                    }
                    /*if(String.isEmpty(order.ONCALL__SAP_Order_Number__c)){
                        order.ONCALL__OnCall_Status__c = 'Draft';
                    }*/
                    
                    /*if(listToSave.etReturn[0].type_Z!=null && listToSave.etReturn[0].message!=null){
					order.ONCALL__SAP_Order_Response__c = listToSave.etReturn[0].type_Z + ' - ' + listToSave.etReturn[0].message;
					}*/ //removed because now we receive a list of messages // drs@avx
                                
                    //order.ONCALL__OnCall_Status__c = 'Closed';
                    System.debug('Order to update: ' + order);
                    listOrder.add(order);
                    //update order;
                } else { // Different from a 200 OK
                    order.ONCALL__SAP_Order_Response__c = 
                        authresp != null && authresp.getStatus() != null 
                        ? authresp.getStatus() 
                        : 'Error to process transaction';
                    listOrder.add(order);
                    System.debug('Connection Error message: ' + authresp.getBody());
                }
            }
            catch (Exception ex)
            {
                System.debug('Exception - Error en peticion: ' + ex);
                order.ONCALL__SAP_Order_Response__c = 'Error: ' + ex.getTypeName() + ', ' + ex.getMessage() ;    
                listOrder.add(order);
            }                        
        }
        
        if(!listOrder.isEmpty()) 
        {
            ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            update listOrder;
        }
    }

    /**
    * Method that modify deserealize the JSON 
    * Created By: heron.zurita@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com, RJP
    * Modify Date 2019-03-20
    * @param String json_order,String orderID
    * @return void
    * @Comments: RJP: MERGE-HONES :  this is HONES Original method
    */
    public static void send_JsonHONES(Map<String, JSONGenerator> mapOrderGen){
       User user = [SELECT id, username, Country FROM User where Id= :UserInfo.getUserId()];
       List<End_Point__mdt> endpoint =
            [SELECT DeveloperName, MasterLabel, Token__c, URL__c, Timeout__c
            FROM End_Point__mdt 
            WHERE MasterLabel = 'Create SAP Order' //RJP rjimenez@ts4.mx changed to read MasterLabel field
            AND Country__c = :'SV'];
       List<ONTAP__Order__c> listOrder = new List<ONTAP__Order__c>();
       for(String key : mapOrderGen.keySet()){
           String json = mapOrderGen.get(key).getAsString().replaceAll('\\n','');              
           ONTAP__Order__c order = new  ONTAP__Order__c();
           order.id = key;
           Http h = new Http();
           HttpRequest httpReq = new HttpRequest();
           httpReq.setEndpoint(endpoint[0].URL__c);
           httpReq.setMethod('POST');
           httpReq.setHeader('Content-Type','application/json;charset=utf-8');
           httpReq.setHeader('bearer', endpoint[0].Token__c);
           httpReq.setBody(json);
           System.debug('json sent: ' +json);
           HTTPResponse authresp = new HTTPResponse();
           try
           {
               authresp = h.send(httpReq);
               System.debug('response status: ' +authresp);
               if(authresp.getStatusCode() == 200 && authresp.getBody() != null){
                   System.debug('Success: ' + authresp.getBody());
                   ONCALL_SAP_OrderResponseHONES listToSave = ONCALL_SAP_OrderResponseHONES.parse(authresp.getBody());
                   if(listToSave.id != null && listToSave.id != ''){
                       order.ONCALL__SAP_Order_Number__c = listToSave.id;
                       //RJP
                       order.ONTAP__SAP_Order_Number__c = order.ONCALL__SAP_Order_Number__c;
                   }
                   /*if(String.isEmpty(order.ONCALL__SAP_Order_Number__c)){
                       order.ONCALL__OnCall_Status__c = 'Draft';
                   }*/
                   if(listToSave.etReturn!=null){
                       if(listToSave.etReturn.Item.type_Z!=null&&listToSave.etReturn.Item.message!=null){
                           order.ONCALL__SAP_Order_Response__c = listToSave.etReturn.Item.type_Z + ' - ' + listToSave.etReturn.Item.message;
                       }
                   }
                   //order.ONCALL__OnCall_Status__c = 'Closed';
                   System.debug('order to update: ' + order);
                   listOrder.add(order);
                   //update order;
               } else{
                   order.ONCALL__SAP_Order_Response__c = authresp != null && authresp.getStatus() != null ? authresp.getStatus() : 'Error to process transaction';
                   listOrder.add(order);
                   System.debug('Connection Error message: ' + authresp.getBody());
               }
           }
           catch (Exception ex)
           {
               System.debug('Error en peticion: ' + ex);
               order.ONCALL__SAP_Order_Response__c = 'Error: ' + ex.getTypeName() + ', ' + ex.getMessage() + 'linea:' + ex.getLineNumber();
               listOrder.add(order);
           }
       }
       if(!listOrder.isEmpty())
       {
           ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
           update listOrder;
       }
   }

   public static void send_JsonBatch(String jsonBatch, set<String> idOrders){
        String endPointUrl = '';
        String bearer = '';
        try{
            List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt WHERE DeveloperName = 'Create_SAP_Order_Batch'];
            endPointUrl =  endpoint[0].URL__c;
            bearer = endpoint[0].Token__c;
        }catch(Exception e){
            System.debug('ERROR::ONCALL_SendJson::send_JsonBatch::' + e.getMessage());
        }
    
        jsonBatch.replaceAll('\\n','');        	   
        
        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endPointUrl);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', bearer);
        httpReq.setBody(jsonBatch);
        System.debug('jsonBatch sent: ' +jsonBatch);
        HTTPResponse authresp = new HTTPResponse();
        list<ONTAP__Order__c> ordersUpdate = new list<ONTAP__Order__c>();
        try
            {
            authresp = h.send(httpReq);
            System.debug('BATCH::response status: ' +authresp);                        
            ordersUpdate = [SELECT id, ONCALL__SAP_Order_Response__c FROM ONTAP__Order__c WHERE Id IN: idOrders];
            if(authresp.getStatusCode() == 200 && authresp.getBody().length() > 0){
                System.debug('BATCH::Success: ' + authresp.getBody());
                
                ONCALL_SAP_OrderResponseBatch responseBatch = ONCALL_SAP_OrderResponseBatch.parse(authresp.getBody());
                for (ONTAP__Order__c order: ordersUpdate){
                     order.ONCALL__SAP_Order_Response__c = 'SEND BY BATCH WITH RESPONSE:' + responseBatch.success + ':'+ responseBatch.message;
                }
            } else{
               for (ONTAP__Order__c order: ordersUpdate){
                     order.ONCALL__SAP_Order_Response__c = 'FAIL TO SEND BATCH WITH ESTATUS:' + authresp.getStatusCode() + ':RESPONSE:' + authresp.getBody();
                } 
            }    
        }
        catch (Exception ex)
        {
            System.debug('BATCH::Error en peticion: ' + ex);
        } 
        finally{
             ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
             update ordersUpdate;
        }      
    }
}