/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Rodrigo Resendiz
Proyecto: ISSM - DSD
Descripción: Controller class to execute and monitor massive tour deletion job
------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    21/05/2018  Rodrigo Resendiz          Created Class
*******************************************************************************/

public class MassiveTourDeletion_ctr {
    //PRIVATE CONST
    private static final Integer MASSIVE_DELETE_BATCH_SIZE = ISSM_MassiveDeleteParams__c.getValues('Tour')==null ? 50 : Integer.valueOf(ISSM_MassiveDeleteParams__c.getValues('Tour').Batch_Size__c);
    private static final Integer MAX_RECORDS_SELECTED = ISSM_MassiveDeleteParams__c.getValues('Tour')==null ? 20 : Integer.valueOf(ISSM_MassiveDeleteParams__c.getValues('Tour').MaxRecordsToDelete__c);
    private static final Integer UPPER_QUERY_LIMIT_ALERT = 50;
    private static final Id batchClassId = [SELECT Id FROM ApexClass where name='MassiveTourDeletion_bch' LIMIT 1][0].Id;

    private static final String SQL_QUERIES_LIMIT_ALERT = Label.ISSM_ManualUpdateMessage;// 'Actualice la página manualmente para verificar el estado actual del proceso.';
    private static final String SELECT_AT_LEAST_ONE_MESSAGE = Label.ISSM_MinSelectionMessage;//'Seleccione por lo menos un registro para iniciar proceso de borrado masivo.';
    private static final String ANOTHER_PROCESS_RUNNING_MESSAGE = Label.ISSM_JobNotAvailableMessage;//'Actualmente hay un trabajo en curso. Espere a que termine para ejecutar uno nuevo.';
    private static final String MAX_RECORD_LIMIT_MESSAGE = String.format(Label.ISSM_MaxSelectionMessage, new List<String> {String.valueOf(MAX_RECORDS_SELECTED)});//'No puedes seleccionar más de {0} registros'
    private static final String SAME_RECORDTYPE_TOUR = Label.ISSM_SameRTToursMessage;//'Seleccione tours del mismo tipo antes de hacer un borrado masivo';
    private static final String NO_PAST_TOURS = Label.ISSM_CannotDeletePastToursMessage;//'No puedes borrar tours con fecha anterior al día de hoy.';

    // PRIVATE VARS
    private Id runningBatchId;
    private Boolean toursOfSameRT;
    private Boolean toursOfPast;
    private String selectedRT;
    ApexPages.StandardSetController setCon;
    private Set<Id> tourIds;
    private String limitAlert;
    //PUBLIC VARS
    public AsyncApexJob runningBatch {get; private set;}
    public List<ONTAP__Tour__c> deletedtourList {get;set;}
    public List<ONTAP__Tour__c> tourList {get;set;}
    public String retURL {
        get{
            return apexpages.currentpage().getparameters().get('retURL');
           }
    }
    public String validationMessage{get;set;}
    public String jobRunningMessage{get;set;}
    public Boolean continueProcess {get;set;}
    public Decimal progress {get{
        if(runningBatch == null) return 0;
        else{
            Decimal result = 0;
            if(runningBatch.Status == 'Completed' || runningBatch.Status == 'Failed'){
                result = 1;
            } else if(runningBatch.TotalJobItems != 0){
                result = Decimal.valueOf(runningBatch.JobItemsProcessed).divide(runningBatch.TotalJobItems, 2);
            }
            if(result == 11 && (runningBatch.Status != 'Completed' || runningBatch.Status != 'Failed'))
                result = .9;
            return result*100;
        }
    }}

    /*CONSTRUCTOR*/
    public MassiveTourDeletion_ctr(ApexPages.StandardSetController controller){
        setCon = controller;
        Id recordTypeId;
        tourList = new List<ONTAP__Tour__c>();
        deletedtourList = new List<ONTAP__Tour__c>();
        toursOfSameRT = true;
        continueProcess = false;
        if(((ONTAP__Tour__c[])setCon.getSelected()).size() > 0){
            toursOfSameRT = true;
            toursOfPast = false;
            tourIds = new Set<Id>();
            for(ONTAP__Tour__c tour: (ONTAP__Tour__c[])setCon.getSelected()){
                tourIds.add(tour.Id);
            }
            for(ONTAP__Tour__c tour : [SELECT Id, Name,ONTAP__TourDate__c, RecordTypeId, RecordType.Name, ONTAP__RouteId__c, CreatedDate, Route__c
                                       FROM ONTAP__Tour__c
                                       WHERE Id IN: tourIds])
            {
                tourList.add(tour);
                if(!toursOfPast){
                    toursOfPast = tour.ONTAP__TourDate__c < System.today();
                }
            }
            deletedtourList = tourList;
            recordTypeId = tourList.get(0).RecordTypeId;
            for(ONTAP__Tour__c tour : tourList){
                toursOfSameRT &= tour.RecordTypeId==recordTypeId;
            }
            if(toursOfSameRT){
                selectedRT = tourList[0].RecordType.Name;
            }
        }
    }
    /**
     * @description Method to UPDATE statuses from VFP
     * @param
     * @return void
     * @version 1.0
     **/
    public void updateStatus(){
        if(runningBatch==null){
            getBatchJobStatus(null);
        }else if(runningBatch.Status != 'Completed' || runningBatch.Status != 'Failed'){
            getBatchJobStatus(runningBatchId);
        }else if(runningBatch.Status == 'Completed' || runningBatch.Status == 'Failed'){
            getBatchJobStatus(runningBatchId);
        }
    }
    /**
     * @description Method to valid whether the batch should run
     * @param
     * @return void
     * @version 1.0
     **/
    private void validateStartProcess(){
        getBatchJobStatus(null);
        if(runningBatch!=null){
            jobRunningMessage = ANOTHER_PROCESS_RUNNING_MESSAGE;
        }
        if(tourList.isEmpty()){
            validationMessage = SELECT_AT_LEAST_ONE_MESSAGE;
        }else if(tourList.size() > MAX_RECORDS_SELECTED){
            validationMessage = MAX_RECORD_LIMIT_MESSAGE;
        }else if(toursOfPast){
            validationMessage = NO_PAST_TOURS;
        }else if(!toursOfSameRT)  validationMessage = SAME_RECORDTYPE_TOUR;
    }
    /**
     *
	*/
    private void getBatchJobStatus(Id batchId){
        if(Limits.getQueries() < (Limits.getLimitQueries()-UPPER_QUERY_LIMIT_ALERT)){
            if(batchId==null){
                for(AsyncApexJob job: [SELECT MethodName, ExtendedStatus, TotalJobItems, NumberOfErrors, JobItemsProcessed, Status, ApexClassId, JobType, Id, LastProcessed
                                       FROM AsyncApexJob
                                       WHERE ApexClassId =: batchClassId AND (Status != 'Completed' AND Status != 'Failed' and Status != 'Aborted') LIMIT 1]){
                    runningBatch = job;
                    runningBatchId = job.Id;
                }

            }else{
                for(AsyncApexJob job: [SELECT MethodName, ExtendedStatus, TotalJobItems, NumberOfErrors,
                                       JobItemsProcessed, Status, ApexClassId, JobType, Id, LastProcessed
                                       FROM AsyncApexJob
                                       WHERE Id =: batchId
                                       LIMIT 1])
                {
                    runningBatch = job;
                }
                if(runningBatch.Status == 'Completed' || runningBatch.Status == 'Failed'){
                    /*tourList = new List<ONTAP__Tour__c>();
                    for(ONTAP__Tour__c tour : [SELECT Id, Name, RecordTypeId, RecordType.Name, ONTAP__RouteId__c, Route__c FROM ONTAP__Tour__c WHERE Id IN: tourIds]){
                        tourList.add(tour);
                    }
                    for(Integer i = 0; i<deletedtourList.size() ; i++){
                        /*for(Integer j = 0; j<tourList.size();j++){
                            if(tourList[j].id == deletedtourList[i].id){
                                deletedtourList.remove(i);
                            }
                        }
                    }*/
                    tourList = [SELECT Id, Name, RecordTypeId, RecordType.Name, ONTAP__RouteId__c, Route__c FROM ONTAP__Tour__c WHERE Id IN: tourIds];
                    Set<Id> setRemainingTourIds = new Map<Id, SObject>(tourList).keySet();
                    Map<Id, ONTAP__Tour__c> mapDeletedTourList = new Map<Id, ONTAP__Tour__c>(deletedtourList);
                    for(ONTAP__Tour__c objTour : mapDeletedTourList.values()) {
                        if(setRemainingTourIds.contains(objTour.Id)) {
                            mapDeletedTourList.remove(objTour.Id);
                        }
                    }
                    deletedtourList = mapDeletedTourList.values();
                }
            }
        }else{
            limitAlert = SQL_QUERIES_LIMIT_ALERT;
        }
    }
    /**
     *
	*/
    public void executeBatch(){
        validateStartProcess();
        if(toursOfSameRT
           && validationMessage==null
           && runningBatch==null){
               continueProcess = true;
            MassiveTourDeletion_bch btch = new MassiveTourDeletion_bch(tourIds, selectedRT);
            runningBatchId = Database.executeBatch(btch, MASSIVE_DELETE_BATCH_SIZE);
            getBatchJobStatus(runningBatchId);
               tourList = new List<ONTAP__Tour__c>();
        }
    }
}