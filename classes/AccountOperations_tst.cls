/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Test class for Account related operations

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    23/08/2017 Daniel Peñaloza            Class created
*******************************************************************************/

@isTest
private class AccountOperations_tst {
    private static final Integer NUM_ACCOUNTS = 1;

    // Monday, Wednesday and Friday
    private static final Boolean[] lstFrecuencies = new Boolean[] { true, false, true, false, true, false, false };

    @testSetup static void setup() {
        
        User u = [Select Id from User Limit 1];
        
        ISSM_PriceEngineConfigWS__c config1 = TestUtils_tst.generatePriceEngineConfig('InsertHerokuEvents',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_1,
            'application/json; charset=UTF-8',
            'POST');
        insert config1;

        ISSM_PriceEngineConfigWS__c config2 = TestUtils_tst.generatePriceEngineConfig('InsertHerokuTours',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_2,
            'application/json; charset=UTF-8',
            'POST');
        insert config2;

        ISSM_PriceEngineConfigWS__c config3 = TestUtils_tst.generatePriceEngineConfig('DeleteHerokuEvents',
            'BASIC token_here',
            TestUtils_tst.STR_ENDPOINT_3,
            'application/json; charset=UTF-8',
            'POST');
        insert config3;

        // Create route
        ONTAP__Route__c route = TestUtils_tst.generateFullRoute();
        insert route;

        // Create Accounts
        Account[] lstAccounts = new List<Account>();
        for (Integer i = 0; i < NUM_ACCOUNTS; i++) {
            Account acc = TestUtils_tst.generateAccount(null, null, i, route.ONTAP__SalesOffice__c);
            lstAccounts.add(acc);
        }
        insert lstAccounts;

        // Create Accounts by Route
        AccountByRoute__c[] lstAccountsByRoute = new List<AccountByRoute__c>();
        for (Account acc: lstAccounts) {
            // Add user to Account teams
            DevUtils_cls.addUserToAccountTeam(acc.Id, route.RouteManager__c,
                new Set<String> { TestUtils_tst.TEAM_MEMBER_ROLE_SALES_MANAGER, TestUtils_tst.TEAM_MEMBER_ROLE_PRESALES });

            // Create Account by Route
            AccountByRoute__c accByRoute = TestUtils_tst.generateAccountByRoute(route.Id, acc.Id, lstFrecuencies);
            lstAccountsByRoute.add(accByRoute);
        }
        insert lstAccountsByRoute;

        // Generate Visit Plan
        VisitPlan__c visitPlan = TestUtils_tst.generateVisitPlan(route.Id, Date.today().addDays(1), Date.today().addDays(30),
            lstFrecuencies, 'abcde12345');
        insert visitPlan;

        // Add accounts to Visit Plan
        AccountByVisitPlan__c[] lstAccountsByVisitPlan = new AccountByVisitPlan__c[]{};
        for (Account acc: lstAccounts) {
            AccountByVisitPlan__c accountByVp = TestUtils_tst.generateAccountByVisitPlan(visitPlan.Id, acc.Id, '1', lstFrecuencies);
            lstAccountsByVisitPlan.add(accountByVp);
        }
        insert lstAccountsByVisitPlan;
       
    }
    
    // Test validateAccountsToRemove
    @isTest 
    static void validateAccountsToRemove_test1() {
        
        Account new_acc1 = new Account(Name='Test 1');
        Account new_acc2 = new Account(Name='Test 2');
        Account new_acc3 = new Account(Name='Test 3');
        
        Account[] newlstAccount = new List<Account>();
        newlstAccount.add(new_acc1);
        newlstAccount.add(new_acc2);
        newlstAccount.add(new_acc3);
        insert newlstAccount;
        
        
        Map<Id, Account> newMapAcc = new Map<Id, Account>();
        
        for(Account temp : newlstAccount) {
            newMapAcc.put(temp.Id, temp);
        }
        
        ONCALL__Call__c new_call1 = new ONCALL__Call__c(Name='Call Test 1',ONCALL__POC__c=new_acc1.Id);
        ONCALL__Call__c new_call2 = new ONCALL__Call__c(Name='Call Test 2',ONCALL__POC__c=new_acc2.Id);
        ONCALL__Call__c new_call3 = new ONCALL__Call__c(Name='Call Test 3',ONCALL__POC__c=new_acc3.Id);
        
        ONCALL__Call__c[] lstCall = new List<ONCALL__Call__c>();
        lstCall.add(new_call1);
        lstCall.add(new_call2);
        lstCall.add(new_call3);
        insert lstCall;
        
        DateTime timeActivity = DateTime.now();
        /*Event new_evt1 = new Event(Description='Event Test 1',DurationInMinutes=15,ActivityDateTime=dateTime.parse('12/27/2009 12:00 AM'));
        Event new_evt2 = new Event(Description='Event Test 2',DurationInMinutes=15,ActivityDateTime=dateTime.parse('12/27/2009 12:00 AM'));
        Event new_evt3 = new Event(Description='Event Test 3',DurationInMinutes=15,ActivityDateTime=dateTime.parse('12/27/2009 12:00 AM')); */
        Event new_evt1 = new Event(Description='Event Test 1',DurationInMinutes=15,ActivityDateTime=timeActivity);
        Event new_evt2 = new Event(Description='Event Test 2',DurationInMinutes=15,ActivityDateTime=timeActivity);
        Event new_evt3 = new Event(Description='Event Test 3',DurationInMinutes=15,ActivityDateTime=timeActivity);
        
        Event[] lstEvent = new List<Event>();
        lstEvent.add(new_evt1);
        lstEvent.add(new_evt2);
        lstEvent.add(new_evt3);
        insert lstEvent;
        
        AccountOperations_cls.validateAccountsToRemove(newlstAccount);
        AccountOperations_cls.removeAccountsFromTours(newMapAcc);
        AccountOperations_cls.removeAccountsFromPlans(newMapAcc);
    }


    /**
     * Test DeletionRequestFlag__c = true in Accounts (it should delete the Accounts from Visit Plan)
     */
    @isTest static void should_remove_accounts_from_visit_plan() {
        ONTAP__Route__c route = [SELECT Id, ONTAP__SalesOffice__c, OwnerId FROM ONTAP__Route__c LIMIT 1];
        AccountByVisitPlan__c[] lstAccountsByVisitPlan = [SELECT Id FROM AccountByVisitPlan__c];
        System.assertEquals(NUM_ACCOUNTS, lstAccountsByVisitPlan.size());

        Account[] lstAccounts = [
            SELECT Id, ONCALL__SAP_Deleted_Flag__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Id IN (SELECT Account__c FROM AccountByVisitPlan__c)
            LIMIT 5
        ];
        for (Account acc: lstAccounts) {
            acc.ONCALL__SAP_Deleted_Flag__c = 'X';
        }

        // Execute Test
        Test.startTest();
        AccountOperations_cls.removeAccountsFromPlans(New Map<Id,Account>([SELECT Id, ONCALL__SAP_Deleted_Flag__c FROM Account]));
        AccountOperations_cls.removeAccountsFromTours(New Map<Id,Account>([SELECT Id, ONCALL__SAP_Deleted_Flag__c FROM Account]));
        Test.stopTest();

        // Validate results
        lstAccountsByVisitPlan = [SELECT Id FROM AccountByVisitPlan__c];

    }

    /**
     * Test CustomerFlagLegal__c = true OR OrderBlockFlag__c = true in Accounts (it should delete Account Events and Calls)
     */
    @isTest static void should_remove_account_events_and_calls() {
        ONTAP__Route__c route = [SELECT Id, ONTAP__SalesOffice__c, RouteManager__c, OwnerId FROM ONTAP__Route__c LIMIT 1];
        VisitPlan__c visitPlan = [SELECT Id, VisitPlanType__c FROM VisitPlan__c WHERE Route__c = :route.Id LIMIT 1];
        AccountByVisitPlan__c[] lstAccountsByVisitPlan = [
            SELECT Id, Account__c, Sequence__c, Account__r.ONTAP__SAPCustomerId__c
            FROM AccountByVisitPlan__c
        ];
        System.assertEquals(NUM_ACCOUNTS, lstAccountsByVisitPlan.size());

        // Set Http Response Mock
        SingleRequestMock_tst requestMock1 = new SingleRequestMock_tst('{"isSuccessful" : true}');
        MultiRequestMock_tst multiRequestMock = new MultiRequestMock_tst();
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_1, requestMock1);
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_2, requestMock1);
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_3, requestMock1);

        Test.setMock(HttpCalloutMock.class, multiRequestMock);
        
        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
        
        // Generate Tour
        ONTAP__Tour__c tour = TestUtils_tst.generateTour(visitPlan, route);
        tour.ONTAP__TourStatus__c = visitPlanSettings.CreatedTourStatus__c;
        tour.TourSubStatus__c = visitPlanSettings.CreatedTourSubStatus__c;
        insert tour;

        tour = [
            SELECT Id, Route__c, Route__r.RouteManager__c, Route__r.OwnerId, RecordTypeId
            FROM ONTAP__Tour__c
            WHERE Id = :tour.Id
        ];

        // Generate Events for Tour
        Event[] lstEvents = new Event[]{};
        Set<Id> setAccountIds = new Set<Id>();
        for (AccountByVisitPlan__c accByVp: lstAccountsByVisitPlan) {
            lstEvents.addAll(TestUtils_tst.generateEvents(1, accByVp, tour));
            setAccountIds.add(accByVp.Account__c);
        }
        insert lstEvents;

        // Verify inserted Events
        Event[] lstInsertedEvents = [
            SELECT Id
            FROM Event
            WHERE WhatId IN (SELECT Id FROM Account WHERE Id IN :setAccountIds)
        ];
        System.assertEquals(1, lstInsertedEvents.size());

        Account[] lstAccounts = [
            SELECT Id, ONCALL__SAP_Deleted_Flag__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Id IN (SELECT Account__c FROM AccountByVisitPlan__c)
            LIMIT 5
        ];
        for (Account acc: lstAccounts) {
            acc.ONCALL__Order_Block__c = 'X';
        }

        // Execute Test
        Test.startTest();
            update lstAccounts;
        Test.stopTest();

        // Validate results
        lstInsertedEvents = [SELECT Id FROM Event WHERE WhatId IN :lstAccounts];
        //System.assertEquals(0, lstInsertedEvents.size());

        Event[] lstAllEvents = [SELECT Id FROM Event];
        //System.assertEquals(10, lstAllEvents.size());
    }

    /**
     * Test prevent event deletion for excluded Tour record types
     */
    @isTest static void should_not_remove_events_and_calls_for_IREP_tour() {
        ONTAP__Route__c route = [SELECT Id, ONTAP__SalesOffice__c, RouteManager__c, OwnerId FROM ONTAP__Route__c LIMIT 1];
        VisitPlan__c visitPlan = [SELECT Id, VisitPlanType__c FROM VisitPlan__c WHERE Route__c = :route.Id LIMIT 1];
        AccountByVisitPlan__c[] lstAccountsByVisitPlan = [
            SELECT Id, Account__c, Sequence__c, Account__r.ONTAP__SAPCustomerId__c
            FROM AccountByVisitPlan__c
        ];
        System.assertEquals(NUM_ACCOUNTS, lstAccountsByVisitPlan.size());

        // Set Http Response Mock
        SingleRequestMock_tst requestMock1 = new SingleRequestMock_tst('{"isSuccessful" : true}');
        MultiRequestMock_tst multiRequestMock = new MultiRequestMock_tst();
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_1, requestMock1);
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_2, requestMock1);
        multiRequestMock.addRequestMock(TestUtils_tst.STR_ENDPOINT_3, requestMock1);

        Test.setMock(HttpCalloutMock.class, multiRequestMock);

        // Generate Tour
        ONTAP__Tour__c tour = TestUtils_tst.generateTour(visitPlan, route);
        tour.RecordTypeId = TestUtils_tst.mapTourRecordTypes.get('BDR').Id;
        tour.ONTAP__TourStatus__c = 'Not Started';
        tour.TourSubStatus__c = 'Listo para descargar al dispositivo';
        insert tour;

        tour = [
            SELECT Id, Route__c, Route__r.RouteManager__c, Route__r.OwnerId, RecordTypeId
            FROM ONTAP__Tour__c
            WHERE Id = :tour.Id
        ];

        // Generate Events for Tour
        Event[] lstEvents = new Event[]{};
        Set<Id> setAccountIds = new Set<Id>();
        for (AccountByVisitPlan__c accByVp: lstAccountsByVisitPlan) {
            lstEvents.addAll(TestUtils_tst.generateEvents(1, accByVp, tour));
            setAccountIds.add(accByVp.Account__c);
        }
        insert lstEvents;

        // Verify inserted Events
        Event[] lstInsertedEvents = [
            SELECT Id
            FROM Event
            WHERE WhatId IN (SELECT Id FROM Account WHERE Id IN :setAccountIds)
        ];
        System.assertEquals(1, lstInsertedEvents.size());

        Account[] lstAccounts = [
            SELECT Id, ONCALL__SAP_Deleted_Flag__c
            FROM Account
            WHERE ISSM_SalesOffice__c = :route.ONTAP__SalesOffice__c
                AND Id IN (SELECT Account__c FROM AccountByVisitPlan__c)
            LIMIT 5
        ];
        for (Account acc: lstAccounts) {
            acc.ONCALL__Order_Block__c = 'X';
        }

        // Execute Test
        Test.startTest();
            try {
                update lstAccounts;
            } catch (Exception e) { /* Trhrows exception */ }
        Test.stopTest();

        // Validate results
        lstInsertedEvents = [SELECT Id FROM Event WHERE WhatId IN :lstAccounts];
        System.assertEquals(1, lstInsertedEvents.size());

        Event[] lstAllEvents = [SELECT Id FROM Event];
        System.assertEquals(1, lstAllEvents.size());
        
        update lstEvents;   
        delete lstEvents; 
    }

}