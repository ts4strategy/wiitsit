/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:HONES_PaymentTelecollectionController.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario              Contacto      						Descripción
 * 15/01/2019    Heron Zurita	    heron.zurita@accenture.com     Creación de la clase  
 * 19/08/2019    Daniel Hernández   dhernandez@ts4.mx              "Modificación para cumplir requerimiento COM-063-Telecollection-Definir Reportes operativos para telecollection"
 */
public class HONES_PaymentTelecollectionController {
     /*
     * Method that returns a register ids 
     * Created By:heron.zurita@accenture.com
     * @params String recordId
     * @return String 
	*/
    @AuraEnabled
    public static String getById(String recordId){
        String newIds;
        try{
        	ONCALL__Call__c acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId LIMIT 1];    
        	newIds = acc.ONCALL__POC__c;
        }catch (exception e) {
        system.debug(e);
      }
        return newIds;
    }
	 /*
     * Method that return the values of the picklist according status data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
    @AuraEnabled
    public static List<String> getPickListValuesStatus(){
        
       List<String> noOptions= new List<String>();
       List<String> pickListValuesList= new List<String>();
        
		Schema.DescribeFieldResult fieldResult = ONCALL__Call__c.ONCALL__Call_Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.getLabel() == LABEL.Called_But){
            	noOptions.add(pickListVal.getLabel());
            }
            else{
				pickListValuesList.add(pickListVal.getLabel());
            }
		}  
        
		return pickListValuesList;
    }
    /*
     * Method that return the values of the picklist according the paymentpromise data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
    @AuraEnabled
    public static List<String> getPickListValuesPaymentPromise(){
        List<String> pickListValuesList= new List<String>();
       Schema.DescribeFieldResult fieldResults = ONCALL__Call__c.ISSM_PaymentPromise__c.getDescribe();
            List<Schema.PicklistEntry> plee = fieldResults.getPicklistValues();
            Map<String, String> valueLabelMap = new Map<String,String>();
            for( Schema.PicklistEntry v : plee) {
                valueLabelMap.put(v.getValue(), v.getLabel());
                pickListValuesList.add(v.getLabel());
            }   
		return pickListValuesList;
    }
    
    /*		*/
    
    /*
     * Method that return the values of the picklist according incomplete status data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
    @AuraEnabled
    public static List<String> getPickListValuesIncompleteStatus(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = ONCALL__Call__c.ISSM_Incomplete_Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}   
        pickListValuesList.add('--'+ Label.None + '--');
		return pickListValuesList;
    }
    /*
     * Method that return the values of the picklist according clarification request data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
       @AuraEnabled
       public static List<String> getPickListValuesClarificationRequest(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = ONCALL__Call__c.ISSM_ClarificationRequest__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}
		return pickListValuesList;
    }
    /*
     * Method that return the values of the picklist according clarifiction type data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
       @AuraEnabled
       public static List<String> getPickListValuesClarificationType(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = ONCALL__Call__c.ISSM_ClarificationType__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
            
			pickListValuesList.add(pickListVal.getLabel());
		}     
    	pickListValuesList.add('--'+ Label.None + '--');
		return pickListValuesList;
    }
    /*
     * Method that return the values of the picklist according paymentdelayreason data
     * Created By:heron.zurita@accenture.com
     * @params void
     * @return List<String>
*/    
    
       @AuraEnabled
       public static List<String> getPickListValuesPaymentDelayReason(){
       List<String> pickListValuesList= new List<String>();
		Schema.DescribeFieldResult fieldResult = ONCALL__Call__c.ISSM_PaymentDelayReason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			pickListValuesList.add(pickListVal.getLabel());
		}     
    	pickListValuesList.add('--'+ Label.None + '--');
		return pickListValuesList;
    }
    /*
     * Method that return the hours of the Delivery delay code of the account.
     * Created By:heron.zurita@accenture.com
     * @params String recordId
     * @return String
    */  
        @AuraEnabled
        public static Integer getDeliveryDateHours(String recordId){
          
          List<ONCALL__Call__c> lstAccounts = [SELECT Id, ISSM_PaymentPromise__c, ONCALL__POC__r.ONTAP__Delivery_Delay_Code__c FROM ONCALL__Call__c WHERE Id =: recordId];
          String hours = '';
          if(!lstAccounts.isEmpty()){
            hours = lstAccounts[0].ONCALL__POC__r.ONTAP__Delivery_Delay_Code__c;
          }
            Integer intHours = 0;
            if(hours!=null&&hours!=''){
		        intHours = Integer.valueOf(hours.split(' ')[0]);
                }
          return intHours;
        }
    
    /*
     * Method that inserts  all the data of the call doesn't matter the status of the call 
     * Created By:heron.zurita@accenture.com
     * @params String recordId,String idCuenta,String PayPromise,String ClarificationRequest,String ClarificationType,Decimal compromisedAmount,String todayValue,String ReasonDelay,String callStatus,String otro,String incompleteStatus
     * @return List<ONCALL__Call__c>
*/    
    @AuraEnabled
    public static List<ONCALL__Call__c> insertTelecobranza(String recordId, String idCuenta,String PayPromise,String ClarificationRequest,String ClarificationType, Decimal compromisedAmount,String todayValue, String ReasonDelay, String callStatus, String commentary, String otro, String incompleteStatus)
    {
        
        try
        {
            System.Debug('Estatus llamada: ' + callStatus);
            System.Debug('Valor etiqueta: ' + LABEL.INCOMPLETE_HEAD);
            System.debug('Razon de incompleta: ' + incompleteStatus);
            
        Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Status__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getLabel(),v.getValue());
            }
        
        Schema.DescribeFieldResult PaymentPromisePick = ONCALL__Call__c.ISSM_PaymentPromise__c.getDescribe();
            List<Schema.PicklistEntry> PaymentPromisePick_list = PaymentPromisePick.getPicklistValues();
            Map<String, String> PaymentPromisePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : PaymentPromisePick_list) {
                PaymentPromisePick_values.put(v.getLabel(),v.getValue());
            }
        
        Schema.DescribeFieldResult IncompletePick = ONCALL__Call__c.ISSM_Incomplete_Status__c.getDescribe();
            List<Schema.PicklistEntry> IncompletePick_list = IncompletePick.getPicklistValues();
            Map<String, String> IncompletePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : IncompletePick_list) {
                IncompletePick_values.put(v.getLabel(),v.getValue());
            }
        
        Schema.DescribeFieldResult ClarificationRequestPick = ONCALL__Call__c.ISSM_ClarificationRequest__c.getDescribe();
            List<Schema.PicklistEntry> ClarificationRequestPick_list = ClarificationRequestPick.getPicklistValues();
            Map<String, String> ClarificationRequestPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : ClarificationRequestPick_list) {
                ClarificationRequestPick_values.put(v.getLabel(),v.getValue());
            }
        
        
        Schema.DescribeFieldResult ClarificationTypePick = ONCALL__Call__c.ISSM_ClarificationType__c.getDescribe();
            List<Schema.PicklistEntry> ClarificationTypePick_list = ClarificationTypePick.getPicklistValues();
            Map<String, String> ClarificationTypePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : ClarificationTypePick_list) {
                ClarificationTypePick_values.put(v.getLabel(),v.getValue());
            }
        
        Schema.DescribeFieldResult PaymentDelayReasonPick = ONCALL__Call__c.ISSM_PaymentDelayReason__c.getDescribe();
            List<Schema.PicklistEntry> PaymentDelayReasonPick_list = PaymentDelayReasonPick.getPicklistValues();
            Map<String, String> PaymentDelayReasonPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : PaymentDelayReasonPick_list) {
                PaymentDelayReasonPick_values.put(v.getLabel(),v.getValue());
            }
        
		system.debug('Tamaño de call status values: '+ Call_StatusPick_values.size());
        system.debug('Lo otro: ' + Call_StatusPick_values.get(LABEL.INCOMPLETE_HEAD));
        if( Call_StatusPick_values != null && callStatus == LABEL.INCOMPLETE_HEAD){
            System.debug('Entro la condicion de incompleta');
        ONCALL__Call__c newUpdateIncomplete = [SELECT Name, ISSM_PaymentPromise__c, ISSM_ClarificationRequest__c,ISSM_ClarificationType__c,ISSM_CompromisedAmount__c, ISSM_PaymentPromiseDate__c, ISSM_PaymentDelayReason__c, ONCALL__Call_Status__c, ISSM_Other_Reason__c, ISSM_Incomplete_Status__c, ISSM_CustomerComments__c FROM ONCALL__Call__c WHERE Id =:recordId];
        
		List<ONCALL__Call__c> updateCallIncomplete = new List<ONCALL__Call__c>();
        
            newUpdateIncomplete.ISSM_PaymentPromise__c = LABEL.NO;
            newUpdateIncomplete.ISSM_ClarificationRequest__c = ClarificationRequestPick_values.get(ClarificationRequest);
            newUpdateIncomplete.ISSM_ClarificationType__c = ClarificationTypePick_values.get(ClarificationType);
            String idCase = createCase( ClarificationType, idCuenta);
            if(idCase != ''){
            	newUpdateIncomplete.ISSM_Case__c = idCase;
            }
            newUpdateIncomplete.ISSM_CompromisedAmount__c = 0;
        	newUpdateIncomplete.ISSM_PaymentDelayReason__c = '';
            if(Call_StatusPick_values.get(callStatus)!=null){
        	newUpdateIncomplete.ONCALL__Call_Status__c = Call_StatusPick_values.get(callStatus);
                }else{
                newUpdateIncomplete.ONCALL__Call_Status__c = callStatus;
            }
        	newUpdateIncomplete.ISSM_Other_Reason__c = '';            
            newUpdateIncomplete.ISSM_Incomplete_Status__c = IncompletePick_values.get(incompleteStatus);
            newUpdateIncomplete.ISSM_CustomerComments__c = commentary;

            System.Debug('Estatus llamada: ' + incompleteStatus);
            System.Debug('Valor insertado: ' + newUpdateIncomplete.ISSM_Incomplete_Status__c);
            
        updateCallIncomplete.add(newUpdateIncomplete);
		update updateCallIncomplete; 
        }
        else{
            System.debug('No entro la condicion de incompleta');
        ONCALL__Call__c newUpdate = [SELECT Name, ISSM_PaymentPromise__c, ISSM_ClarificationRequest__c,ISSM_ClarificationType__c,ISSM_CompromisedAmount__c, ISSM_PaymentPromiseDate__c, ISSM_PaymentDelayReason__c, ONCALL__Call_Status__c, ISSM_Other_Reason__c, ISSM_Incomplete_Status__c, ISSM_CustomerComments__c FROM ONCALL__Call__c WHERE Id =:recordId];
        
		List<ONCALL__Call__c> updateCall = new List<ONCALL__Call__c>();
        
            newUpdate.ISSM_PaymentPromise__c = PaymentPromisePick_values.get(PayPromise);
            newUpdate.ISSM_ClarificationRequest__c = ClarificationRequestPick_values.get(ClarificationRequest);
            newUpdate.ISSM_ClarificationType__c = ClarificationTypePick_values.get(ClarificationType);
            createCase( ClarificationType, idCuenta);
            newUpdate.ISSM_CompromisedAmount__c = compromisedAmount;
            if(todayValue!=null){
        	newUpdate.ISSM_PaymentPromiseDate__c = Date.valueOf(todayValue);
        	}
        	newUpdate.ISSM_PaymentDelayReason__c = PaymentDelayReasonPick_values.get(ReasonDelay);
        	newUpdate.ONCALL__Call_Status__c = Call_StatusPick_values.get(callStatus);
            newUpdate.ISSM_CustomerComments__c = commentary;
            newUpdate.ISSM_Incomplete_Status__c = IncompletePick_values.get(incompleteStatus);
        	newUpdate.ISSM_Other_Reason__c = otro;
        updateCall.add(newUpdate);
		update updateCall; 
        }
        
        }catch(DMLException e){
            System.debug(e.getMessage());
        }
        return null;
    }
    
     /*
     * Method that  return a register the call
     * Created By:heron.zurita@accenture.com
     * @params String recordId
     * @return ONCALL__Call__c
*/    
    @AuraEnabled
    public static ONCALL__Call__c infoCall(String recordId){
    
        ONCALL__Call__c newListInfo = [SELECT Name, ISSM_PaymentPromise__c, ISSM_ClarificationRequest__c,ISSM_ClarificationType__c,ISSM_CompromisedAmount__c, ISSM_PaymentPromiseDate__c, ISSM_PaymentDelayReason__c, ONCALL__Call_Status__c, ISSM_Other_Reason__c,ISSM_Incomplete_Status__c,ISSM_Case__r.CaseNumber, ISSM_CustomerComments__c  FROM ONCALL__Call__c WHERE Id =:recordId];
        
        Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Status__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getValue(),v.getLabel());
            }
        
        Schema.DescribeFieldResult PaymentPromisePick = ONCALL__Call__c.ISSM_PaymentPromise__c.getDescribe();
            List<Schema.PicklistEntry> PaymentPromisePick_list = PaymentPromisePick.getPicklistValues();
            Map<String, String> PaymentPromisePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : PaymentPromisePick_list) {
                PaymentPromisePick_values.put(v.getValue(),v.getLabel());
            }
        
        Schema.DescribeFieldResult IncompletePick = ONCALL__Call__c.ISSM_Incomplete_Status__c.getDescribe();
            List<Schema.PicklistEntry> IncompletePick_list = IncompletePick.getPicklistValues();
            Map<String, String> IncompletePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : IncompletePick_list) {
                IncompletePick_values.put(v.getValue(),v.getLabel());
            }
        
        Schema.DescribeFieldResult ClarificationRequestPick = ONCALL__Call__c.ISSM_ClarificationRequest__c.getDescribe();
            List<Schema.PicklistEntry> ClarificationRequestPick_list = ClarificationRequestPick.getPicklistValues();
            Map<String, String> ClarificationRequestPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : ClarificationRequestPick_list) {
                ClarificationRequestPick_values.put(v.getValue(),v.getLabel());
            }
        
        
        Schema.DescribeFieldResult ClarificationTypePick = ONCALL__Call__c.ISSM_ClarificationType__c.getDescribe();
            List<Schema.PicklistEntry> ClarificationTypePick_list = ClarificationTypePick.getPicklistValues();
            Map<String, String> ClarificationTypePick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : ClarificationTypePick_list) {
                ClarificationTypePick_values.put(v.getValue(),v.getLabel());
            }
        
        Schema.DescribeFieldResult PaymentDelayReasonPick = ONCALL__Call__c.ISSM_PaymentDelayReason__c.getDescribe();
            List<Schema.PicklistEntry> PaymentDelayReasonPick_list = PaymentDelayReasonPick.getPicklistValues();
            Map<String, String> PaymentDelayReasonPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : PaymentDelayReasonPick_list) {
                PaymentDelayReasonPick_values.put(v.getValue(),v.getLabel());
            }
        
        
        newListInfo.ONCALL__Call_Status__c = Call_StatusPick_values.get(newListInfo.ONCALL__Call_Status__c);
        newListInfo.ISSM_PaymentPromise__c = PaymentPromisePick_values.get(newListInfo.ISSM_PaymentPromise__c);
        newListInfo.ISSM_Incomplete_Status__c = IncompletePick_values.get(newListInfo.ISSM_Incomplete_Status__c);
        newListInfo.ISSM_ClarificationRequest__c = ClarificationRequestPick_values.get(newListInfo.ISSM_ClarificationRequest__c);
        newListInfo.ISSM_ClarificationType__c = ClarificationTypePick_values.get(newListInfo.ISSM_ClarificationType__c);
        newListInfo.ISSM_PaymentDelayReason__c = PaymentDelayReasonPick_values.get(newListInfo.ISSM_PaymentDelayReason__c);
        return newListInfo;
    }
     /*
     * Method that create cases for telecollection  
     * Created By:c.leal.beltran@accenture.com
     * @params String ClarificationType,String IdAccount
     * @return void
*/    
     
    public static String createCase(String ClarificationType,string IdAccount){
        
        String idCase = '';
        if(ClarificationType == GlobalStrings.TYPIF_NUMB1){
            List<ISSM_TypificationMatrix__c> typifList = [SELECT ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, Name 
                                                     	  FROM ISSM_TypificationMatrix__c WHERE Name=:GlobalStrings.TYPIF_NAME1 LIMIT 1];
            if(typifList.size()>0){
                ISSM_TypificationMatrix__c typif = typifList.get(0);
                
                CS_CASEGENERATION_CLASS obj01 = new CS_CASEGENERATION_CLASS();       
                idCase = obj01.GenerateNewCase(IdAccount, typif.ISSM_TypificationLevel1__c, typif.ISSM_TypificationLevel2__c,typif.ISSM_TypificationLevel3__c, 
                                                      typif.ISSM_TypificationLevel4__c, ClarificationType);
            }
        }
        else if(ClarificationType == GlobalStrings.TYPIF_NUMB2){
            List<ISSM_TypificationMatrix__c> typifList = [SELECT ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, Name 
                                                      FROM ISSM_TypificationMatrix__c WHERE Name=:GlobalStrings.TYPIF_NAME2 LIMIT 1];
            if(typifList.size()>0){
                ISSM_TypificationMatrix__c typif = typifList.get(0);
                
                CS_CASEGENERATION_CLASS obj01 = new CS_CASEGENERATION_CLASS();       
                idCase = obj01.GenerateNewCase(IdAccount, typif.ISSM_TypificationLevel1__c, typif.ISSM_TypificationLevel2__c,typif.ISSM_TypificationLevel3__c, 
                                                      typif.ISSM_TypificationLevel4__c, ClarificationType);
            }
        }
        return idCase;
    }
    
}