/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TriggerQuestionTMHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * 
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 07/02/2019           Gabriel E Garcia       Test class of CS_TriggerQuestionTMHandler 
 */
@isTest
private class CS_TriggerQuestionTMHandler_Test {

    /**
    * Method test TriggerContactHandler_after Insert and update
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_beforeInsertTrigger(){
        ISSM_TypificationMatrix__c tm = new ISSM_TypificationMatrix__c();
        tm.ISSM_TypificationLevel1__c = 'SAC';
        tm.ISSM_TypificationLevel2__c = 'Solicitudes'; 
        tm.ISSM_TypificationLevel3__c = 'Equipo Frío'; 
        tm.ISSM_TypificationLevel4__c = 'Retiro'; 
        tm.ISSM_Countries_ABInBev__c = 'Honduras';
        tm.CS_Days_to_End__c = 10;
        insert tm;
        
        CS_Question_Typification_Matrix__c question = new CS_Question_Typification_Matrix__c();
        question.CS_Typification_Level1__c = 'SAC';
        question.CS_Typification_Level2__c = 'Solicitudes'; 
        question.CS_Typification_Level3__c = 'Equipo Frío'; 
        question.CS_Typification_Level4__c = 'Retiro';
        question.CS_Country__c = 'Honduras';
        question.CS_Question__c = '¿Pregunta test?';
        question.CS_Question_Type__c = 'Text';
        
        insert question;
        
    }
    
}