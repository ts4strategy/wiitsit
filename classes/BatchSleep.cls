public class BatchSleep implements Database.Batchable<sObject>{
    
     public Database.QueryLocator start(Database.BatchableContext c){       
         String query = 'SELECT Id  '+
             'FROM ONTAP__Order__c '+
             'LIMIT 1';
        return DataBase.getQueryLocator(query);
    }
    
     public void execute(Database.BatchableContext c, List<ONTAP__Order__c> scope){
        
         Long startTime = DateTime.now().getTime();
         Long finishTime = DateTime.now().getTime();
         while ((finishTime - startTime) < 30000) {
             //sleep for 9s
             finishTime = DateTime.now().getTime();
         }
         System.assertEquals(Integer.valueOf((finishTime - startTime) / 1000), 30);
         System.debug('>>> Done from ' + startTime + ' to ' + finishTime);
     }
    
    public void finish(Database.BatchableContext c){}

    
    
}