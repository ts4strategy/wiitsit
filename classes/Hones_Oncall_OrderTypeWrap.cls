/**
    * Test method for set and get by the entity OrderTypeWrap
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
public class Hones_Oncall_OrderTypeWrap {
    @AuraEnabled public List<List<String>> picktype{set;get;}
}