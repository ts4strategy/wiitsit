public class CallOutOrderClassTest {
    public static HttpResponse genOrder() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://abco-priceengine.herokuapp.com/dealslist');
        req.setMethod('POST');
        Http h = new Http();
        HttpResponse response = h.send(req);
        return response;
    } 
    
    public static HttpResponse genOrder2() {
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://abco-priceengine.herokuapp.com/dealslist ');
        req.setMethod('POST');
        //req.setBody('{"token" : "43125716217827821687216871698166719217933820471113209821380291928389","accountId": "0011820869","salesOrg" : "CS01"}');
        Http h = new Http();
        HttpResponse response = h.send(req);
        return response;
    } 
    
}