/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Apex2JsonP_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 11/04/2019           Ricardo Navarrete Lozano          Creation of methods.
*/
@isTest
public class Apex2JsonP_Test {
    @isTest static void test_Apex2JsonP_UseCase1() {
        Apex2JsonP aj = new Apex2JsonP();
        aj.token = 'Test';
        aj.accountId = 'Test';
        aj.salesOrg = 'Test';
        aj.paymentMethod = 'Test';
        aj.orderType = 'Test';
        
    }
}