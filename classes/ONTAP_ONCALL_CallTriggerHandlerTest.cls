/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_ONCALL_CallTriggerHandlerTest.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 25/09/2019      Rogelio Jiménez	  rjimenez@ts4.mx     Test methods for ONTAP_ONCALL_CallTriggerHandler
 */

@isTest
public with sharing class ONTAP_ONCALL_CallTriggerHandlerTest {
    

  @isTest static void test_afterInsert_UseCase1(){
    List<ONCALL__Call__c> calls = new List<ONCALL__Call__c>();
    List<ONTAP__ONCALL_Call__C> onTapCalls = new List<ONTAP__ONCALL_Call__C>();

    Account acc = new Account(Name ='Ricardo Milos Test', ONTAP__SalesOgId__c = 'CS01');
    insert acc;
    ONCALL__Call__c call1 = new ONCALL__Call__c();
    call1.Name = 'CallTest';
    call1.ONCALL__POC__c = acc.Id;
    calls.add(call1);

    insert  calls;

    ONTAP__ONCALL_Call__C ontapCall1 = new ONTAP__ONCALL_Call__C();
    ontapCall1.ONTAP__Call_Status__c = 'Open';
    ontapCall1.Call_Status__c = 'Open';
    ontapCall1.ONTAP__Task_Id__c = call1.Id;
    onTapCalls.add(ontapCall1);

    insert ontapCall1;


  }
}