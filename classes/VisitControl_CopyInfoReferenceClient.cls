/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: VisitControl_CopyInfoReferenceClient.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Gerardo Martinez        Creation of methods.
*/
public without sharing  class VisitControl_CopyInfoReferenceClient {
    
    /**
* Method for copy the ids from a client referenced
* Created By: g.martinez.cabral@accenture.com
* @param List<Id> accounts
* @return void
*/
    @InvocableMethod
    public static void CopyInformation(List<Id> accounts) {
        
        Map<Id,Account> acNewList = New Map<Id,Account>([SELECT Id,V360_ReferenceClient__c FROM Account WHERE Id IN:accounts]);
        copyCustomerReferenceRoutes(acNewList);
    }
    
    /**
* Method for copy the information of routes from a client referenced 
* Created By: g.martinez.cabral@accenture.com
* @param Map<Id,Account> acNewList
* @return void
*/
    public static void  copyCustomerReferenceRoutes(Map<Id,Account> acNewList){  
        
        Set<String> parentAcc = new Set<String>();
        for (Account acc : acNewList.values()){          
            if(acc.V360_ReferenceClient__c!= NULL){
                parentAcc.add(acc.V360_ReferenceClient__c);
            }
        }
        
        List<AccountByRoute__c> result = new List<AccountByRoute__c>(); 
        
        Map<Id,Account> reference = New Map<Id,Account>( [SELECT ONTAP__SAP_Number__c,V360_ReferenceClient__c, (SELECT Id, Route__c, Account__c, Monday__c, Saturday__c, Sunday__c, Thursday__c, Tuesday__c,
                                                                                                                Wednesday__c, Friday__c FROM AccountsByRoutes__r) FROM Account WHERE Id IN :parentAcc]);
        System.debug('Reference:'+Reference);
        
        for(Id childAccountId :acNewList.keySet()){
            System.debug('Child:'+childAccountId);
            if(acNewList.get(childAccountId).V360_ReferenceClient__c!= null){
                
                Id accountId =  acNewList.get(childAccountId).V360_ReferenceClient__c;
                
                Account acc = reference.get(accountId);
                System.debug('Parent: '+acc);  
                
                if(acc != NULL && acc.AccountsByRoutes__r!=NULL && acc.AccountsByRoutes__r.size()>0 ){
                    List<AccountByRoute__c> aux = acc.AccountsByRoutes__r;
                    System.debug('Routes: '+acc.AccountsByRoutes__r);  
                    for(AccountByRoute__c copy:aux){
                        AccountByRoute__c accByRouteNew = new AccountByRoute__c ();
                        accByRouteNew.Route__c = copy.Route__c;
                        accByRouteNew.Account__c = childAccountId;
                        accByRouteNew.Monday__c = copy.Monday__c;
                        accByRouteNew.Saturday__c = copy.Saturday__c;
                        accByRouteNew.Sunday__c = copy.Sunday__c;
                        accByRouteNew.Thursday__c = copy.Thursday__c;
                        accByRouteNew.Tuesday__c = copy.Tuesday__c;
                        accByRouteNew.Wednesday__c = copy.Wednesday__c;
                        accByRouteNew.Friday__c = copy.Friday__c;
                        result.add(accByRouteNew);
                    }
                }
            }
            
        }
        insert result;
    }
   /**
* Method for copy the information of routes from a Interlocutors
* Created By: g.martinez.cabral@accenture.com
* @param Map<Id,Account> acNewList
* @return void
*/    
    public static void  copyCustomerInterlocutors(Map<Id,Account> acNewList){  
        
        Set<String> parentAcc = new Set<String>();
        for (Account acc : acNewList.values()){          
            if(acc.V360_ReferenceClient__c!= NULL){
                parentAcc.add(acc.V360_ReferenceClient__c);
            }
        }
        
        List<Account> result = new List<Account>(); 
        
        Map<Id,Account> reference = New Map<Id,Account>( [SELECT ONTAP__SAP_Number__c,V360_ReferenceClient__c,V360_SalesZoneAssignedBDR__c, V360_SalesZoneAssignedTelesaler__c, V360_SalesZoneAssignedPresaler__c,
                                                          V360_SalesZoneAssignedCoolers__c, V360_SalesZoneAssignedCredit__c, V360_SalesZoneAssignedTellecolector__c FROM Account WHERE Id IN :parentAcc]);
        System.debug('Reference:'+Reference);
        
        for(Id childAccountId :acNewList.keySet()){
            System.debug('Child:'+childAccountId);
            if(acNewList.get(childAccountId).V360_ReferenceClient__c!= null){
                
                Id accountId =  acNewList.get(childAccountId).V360_ReferenceClient__c;
                
                Account acc = reference.get(accountId);
                System.debug('Parent: '+acc);  
				Account accToUpdate = new Account();
                
                if(acc != NULL && acc.V360_SalesZoneAssignedBDR__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedBDR__c =  acc.V360_SalesZoneAssignedBDR__c;
                    
                }
                
                 if(acc != NULL && acc.V360_SalesZoneAssignedTelesaler__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedTelesaler__c =  acc.V360_SalesZoneAssignedTelesaler__c;
                    
                }
                
                if(acc != NULL && acc.V360_SalesZoneAssignedPresaler__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedPresaler__c =  acc.V360_SalesZoneAssignedPresaler__c;
                    
                }
                
                if(acc != NULL && acc.V360_SalesZoneAssignedCoolers__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedCoolers__c =  acc.V360_SalesZoneAssignedCoolers__c;
                    
                }
                
                 if(acc != NULL && acc.V360_SalesZoneAssignedCredit__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedCredit__c =  acc.V360_SalesZoneAssignedCredit__c;
                    
                }
                
                if(acc != NULL && acc.V360_SalesZoneAssignedTellecolector__c!=NULL ){
                    accToUpdate.Id = childAccountId;
                    accToUpdate.V360_SalesZoneAssignedTellecolector__c =  acc.V360_SalesZoneAssignedTellecolector__c;
                    
                }
                
                if(accToUpdate.Id!=NULL){
                    result.add(accToUpdate);
                }
            }
        }
        update result;
    }
}