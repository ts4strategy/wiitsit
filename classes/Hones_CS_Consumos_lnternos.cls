public class Hones_CS_Consumos_lnternos {
    @InvocableMethod 
    public static void Hones_CS_Consumos_lnternos(List<String> Order) {
        String listOrd = [SELECT Id FROM ONTAP__Order__c WHERE Name =:Order].Id;
        Consumos_lnternos(listOrd);
    }
    @future
    public static void Consumos_lnternos(String OrderId) {
        string idorder = OrderId;
        system.debug('Consulta de lo que tiene que aparecer');	
        system.debug('::::::::::::::::::::::::::::::::::::::');	
        List<ONTAP__Order_Item__c> listOrd_item = [SELECT Id,ONTAP__ActualQuantity__c,ISSM_Uint_Measure_Code__c FROM ONTAP__Order_Item__c WHERE ONTAP__CustomerOrder__c =:OrderId];
        system.debug('listOrd_item' + listOrd_item);
        Decimal cantidades = 0;
        for(ONTAP__Order_Item__c order_item : listOrd_item){
            system.debug('records::::' + order_item);            
            if(order_item.ISSM_Uint_Measure_Code__c !='UN'){
                system.debug('records.ONTAP__ActualQuantity__c::::' + order_item.ONTAP__ActualQuantity__c);
                cantidades += order_item.ONTAP__ActualQuantity__c;
            }
        }
        system.debug('cantidades::::' + cantidades);            
        List<Approvals_level__mdt> level_mtd = new List<Approvals_level__mdt>();
        
        level_mtd = [SELECT Numero_de_nivel__c, Rango__c, Rango_Maximo__c 
                     FROM Approvals_level__mdt 
                     WHERE Rango__c <=: cantidades AND Rango_Maximo__c >=: cantidades];
        system.debug('level_mtd::::' + level_mtd);            
        
        List<Approval.ProcessSubmitRequest> submitRequestList = new List<Approval.ProcessSubmitRequest>();
        List<ONTAP__Order__c> OrdOrigen = [SELECT ISSM_OriginText__c, ONTAP__OrderAccount__c  FROM ONTAP__Order__c WHERE Id =:OrderId]; 
        List<Account> AccountOrder= [SELECT V360_SalesZoneAssignedTelesaler__c,V360_SalesZoneAssignedPresaler__c FROM Account WHERE Id =:OrdOrigen[0].ONTAP__OrderAccount__c]; 
        String cuentas = '';
        String approval = '';
        if(OrdOrigen[0].ISSM_OriginText__c == 'OCAL'){
            List<V360_SalerPerZone__c> SalesZone = [SELECT V360_SalesZone__c FROM V360_SalerPerZone__c  WHERE Id =: AccountOrder[0].V360_SalesZoneAssignedTelesaler__c];  
            cuentas = SalesZone[0].V360_SalesZone__c;
        }
        if(OrdOrigen[0].ISSM_OriginText__c == 'OTAP'){  
            List<V360_SalerPerZone__c> SalesZone = [SELECT V360_SalesZone__c FROM V360_SalerPerZone__c  WHERE Id =: AccountOrder[0].V360_SalesZoneAssignedPresaler__c];  
            cuentas = SalesZone[0].V360_SalesZone__c;
        } 
        
        approval = [SELECT ParentId FROM Account WHERE Id =:cuentas].ParentId; 
        System.debug('cuentas 1:' + approval);
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(idorder);
        req1.setSubmitterId(approval);
        req1.setProcessDefinitionNameOrId('Approval_Intentional_Products ');
        req1.setSkipEntryCriteria(true);
        System.debug('req1' + req1);
        
        ONTAP__Order__c OrderUpd = new ONTAP__Order__c();
        OrderUpd.Id = OrderId;
        OrderUpd.Approvals_level__c = 1;
        OrderUpd.Max_approval_lavel__c  = Decimal.valueOf(level_mtd[0].Numero_de_nivel__c);
		System.debug('OrderUpd' + OrderUpd);  
    }
}