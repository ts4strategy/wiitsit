/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Class for generate visit plans

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       05-07-2017        Nelson Sáenz Leal (NSL)      Creation Class
****************************************************************************************************/
public with sharing class DailyVisitplan_cls
{
    public static list<VisitPlanWrapper> lstVisPl                    = new list<VisitPlanWrapper>();
    public static map<id, list<VisitPlanWrapper>> mapIdAccListPlanVs = new map<id, list<VisitPlanWrapper>>();

    // Visit Plan Settings
    private static VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
    private static Integer VISIT_PERIOD 	  = Integer.valueOf(SyncHerokuParams__c.getAll().get('SyncToursEvents').VisitPeriodConfig__c);
    private static Integer VISIT_PERIOD_CONF  = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;

    /**
        * @description Method in charge of generating the list of visits
        * @author Nelson Sáenz Leal
    **/
    public static void generateDailyVisitplan(list<VisitPlanWrapper> lstVsPlan, map<id, set<string>> mapIdAccServModels, set<Id> setIdRoutes)
    {
        System.debug('lstVsPlan===>>'+lstVsPlan);
        // Obtain the list of metadata parameters to validate the service models that has prevalence
        list<ParametersVisitControl__mdt> lstParamVsControl = [SELECT CanCoexist__c, DeveloperName, Id, Label,
                                                                    Language, MasterLabel, NamespacePrefix,
                                                                    QualifiedApiName, SameFrequency__c, SameWeeklyPeriod__c,
                                                                    ServiceModel2__c, ServiceModel__c, WhoPrevails__c
                                                                FROM ParametersVisitControl__mdt
                                                                WHERE CanCoexist__c = true
                                                                AND SameFrequency__c = true];
        // I generate a map with the id of the route with the associated tours to validate if it is allowed to generate as "Assigned" or "Created"
        map<id, ONTAP__Route__c>  mapIdRoute  =   new map<id, ONTAP__Route__c>([SELECT Id, ONTAP__TourId__c,
                                                                                        (SELECT Id FROM Tours__r
                                                                                        //WHERE ONTAP__TourStatus__c = :visitPlanSettings.AssignedTourStatus__c)
                                                                                        WHERE ONTAP__IsActive__c = true)
                                                                                    FROM ONTAP__Route__c WHERE Id IN: setIdRoutes]);
        // Go through the list of metadata parameters and create a map with the prevailing service model
        System.debug('mapIdRoute========>'+mapIdRoute);
        map<string, string> mapstrModelWhoPrev  = new map<string, string>();
        for(ParametersVisitControl__mdt objParamVCtrl : lstParamVsControl)
        {
            mapstrModelWhoPrev.put(objParamVCtrl.ServiceModel__c +','+objParamVCtrl.ServiceModel2__c, objParamVCtrl.WhoPrevails__c);
        }

        System.debug('mapIdAccServModels===>>>'+mapIdAccServModels);
        System.debug('mapstrModelWhoPrev===>>>'+mapstrModelWhoPrev);
        /*
        * I go through the visit plans to obtain the accounts that are in two plans of visit
        * AND that they fulfill the criteria of period to realize visit
         */
        for(VisitPlanWrapper planWrapper : lstVsPlan)
        {
            if((planWrapper.objVisitPlan.Tours__r == null || planWrapper.objVisitPlan.Tours__r.isEmpty())
            && planWrapper.lstAccounts.size() > 0 )
            {
                for(AccountByVisitPlan__c objAccByVisit: planWrapper.lstAccounts)
                {
                  if(objAccByVisit.LastVisitDate__c == null || getVisitPeriod(objAccByVisit.LastVisitDate__c) == Integer.valueOf(objAccByVisit.WeeklyPeriod__c) || getVisitPeriod(objAccByVisit.LastVisitDate__c) == 0)
                  {
                      lstVisPl = mapIdAccListPlanVs.get(objAccByVisit.Account__c);
                      if(lstVisPl == null)
                      {
                        lstVisPl = new list<VisitPlanWrapper>();
                        mapIdAccListPlanVs.put(objAccByVisit.Account__c,  lstVisPl);
                      }
                      lstVisPl.add(planWrapper);
                  }
                }
            }
        }

         System.debug('mapIdAccListPlanVs==>'+mapIdAccListPlanVs);
        // With the account map by valid visit plans which accounts are in two visit plans and have more than one service model
        // and call the validaServiceModel method to obtain which service model prevails
        for(Id idAcc: mapIdAccListPlanVs.keyset())
        {
          if(mapIdAccListPlanVs.get(idAcc).size() > 1)
          {
            System.debug('modelservice ** ANTES ===>>>'+mapIdAccServModels.get(idAcc));
            if(mapIdAccServModels != null && !mapIdAccServModels.isEmpty())
            {
              validaServiceModel(idAcc, mapIdAccServModels.get(idAcc), mapstrModelWhoPrev);
            }
            System.debug('modelservice ** DESPUES ===>>>'+mapIdAccServModels.get(idAcc));
          }
        }

        System.debug('lstVsPlan===>>>'+lstVsPlan);
        System.debug('mapIdAccServModels===>>>'+mapIdAccServModels);
        ID jobID = System.enqueueJob(new DailyVisitplan_qbl(lstVsPlan, mapIdAccServModels, mapIdRoute,  1));
    }
    /**
        * @description Method in charge of validating the visit period configured for the accounts
        * @author Nelson Sáenz Leal
    **/
    public static Decimal getVisitPeriod(Date lastVisitDate)
    {
        Date dtStartWeek        = lastVisitDate.toStartOfWeek();
        Decimal intVisitPeriod  = Math.floor(Decimal.valueOf(dtStartWeek.daysBetween(System.today().addDays(VISIT_PERIOD_CONF)) / 7));

        System.debug('dtStartWeek=====>>'+lastVisitDate.addDays(VISIT_PERIOD_CONF*-1).toStartOfWeek());
        System.debug('intVisitPeriod==>>>'+intVisitPeriod);
        return intVisitPeriod;
    }
     /**
        * @description Method in charge of validating the prevailing service model
        * @author Nelson Sáenz Leal
    **/
    public static void validaServiceModel(Id idAccount , set<String> setServiceModel, map<String, string> mapServModelWhoPrev)
    {
      string strServModel1 = '';
      string strServModel2 = '';

      for(String objServModel : mapServModelWhoPrev.keyset())
      {
          strServModel1 = objServModel.substringAfter(',');
          strServModel2 = objServModel.substringBefore(',');
          if(setServiceModel.contains(strServModel1) && setServiceModel.contains(strServModel2))
          {
            if(mapServModelWhoPrev.get(objServModel) != visitPlanSettings.NAServiceModel__c)
            {
                if(mapServModelWhoPrev.get(objServModel) == strServModel1)
                    setServiceModel.remove(strServModel2);

                else if(mapServModelWhoPrev.get(objServModel) == strServModel2)
                    setServiceModel.remove(strServModel1);
            }
          }
          if(setServiceModel.size() == 1)
          {
              break;
          }
      }
    }
}