/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_CASE_COOLER_cls_test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 11/02/2019           Debbie Zacarias        Creation of the test class for CS_CASE_COOLER_cls
*/

@isTest
private class CS_CASE_COOLER_cls_test{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @TestSetup
    static void setup()
    {
        test.startTest();
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686', ONTAP__ProductType__c = Label.CS_Cooler_Filter, ONTAP__Country_Code__c = 'SV' );
        insert product;
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        caseTest.CS_skuProduct__c = product.ONTAP__ProductCode__c;
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        insert caseTest;
        
        test.stopTest();
    }
    
    /**
    * Method for test the method getPickListValues
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getPickListValues()
    {
        CS_CASE_COOLER_cls.getPickListValues();
    }
    
    /**
    * Method for test the method getPickListValues with another user with different country code
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getPickListValuesHN()
    {
    Profile p = [Select  Id from Profile limit 1];
    User usr = new User(Country__c = 'Honduras', Username= 'rezznov@co.ab-inbev.com.irep.wiitsit', LastName = 'wiit', Email = 'r.wiit@gmdodelo.com', Alias = 'Rwiit', CommunityNickname = 'apiwwiitt', TimeZoneSidKey = 'America/Bogota', LocaleSidKey = 'es_US', EmailEncodingKey= 'ISO-8859-1', ProfileId = p.id, LanguageLocaleKey = 'es');
        System.runAs(usr){
             CS_CASE_COOLER_cls.getPickListValues();
        
        }
    }
    
    /**
    * Method for test the method GetProductCase
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest 
    static void test_GetProductCase(){
        Case caseTes = ([SELECT id FROM Case LIMIT 1]); 
        String caset = caseTes.Id;
        CS_CASE_COOLER_cls.GetProductCase(caset,'Product Test');
    }
    
    /**
    * Method for test the method SetProductCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest  
    static void test_SetProductCase(){
        Case caseTes = ([SELECT id FROM Case LIMIT 1]); 
        String caset = caseTes.Id;
        CS_CASE_COOLER_cls.SetProductCase(caset);
    }
    
    
    /**
    * Method for test the method updateCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_UpdateCase(){
        
        try{
        Case caseTes = ([SELECT id FROM Case LIMIT 1]);
        String caset = '';
        if(caseTes != null){
            caset = caseTes.Id;
        }
        CS_CASE_COOLER_cls.UpdateCase(caset, 'test product');
        }catch(DMLException e){
            system.debug(e);
        }
    
    }
}