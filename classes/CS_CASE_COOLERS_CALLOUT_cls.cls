/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_CASE_COOLERS_CALLOUT_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 23/01/2019           Debbie Zacarias       Creacion de la clase para Callout
*/

public class CS_CASE_COOLERS_CALLOUT_cls 
{
    /**
    * Method for send a request to SAP
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with case id
    * @return Void
    */
    @future(callout=true)
    public static void sendRequest(List<string> lstIdCase)
    {   
        List<Case> lstCaseDetail = new List<Case>();
        List<Case> lstCaseUpdate = new  List<Case>();
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        End_Point__mdt mdt = ([SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt Where DeveloperName = 'Create_SAP_Order']);
        
        string countryESV = Label.CS_Country_El_Salvador;
        string countryHN = Label.CS_Country_Honduras;
        string endPoint = cool.URL__c;
        string jsonString  = '';
        string errorMess = '';
        string messageStatus = '';
        string successMessage = '';
        integer attempt = 0;
        Integer attmpLab = Integer.valueOf(Label.CS_Cooler_Attemps);
        Integer statusCode = Integer.valueOf(Label.CS_Status_Code);
        boolean flagAttempt = false;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
                
        Map<String, Object> bodyResponse = new Map<String, Object>();
        Map<String, Object> etReturn = new Map<String, Object>();
        Map<String, Object> item = new MAp<String, Object>();
               
        try
        {
            /* Se realiza la consulta del detalle de los casos con los id */
            lstCaseDetail = [SELECT Id, ISSM_NumeroSAP__c, AccountId, CS_skuProduct__c,ISSM_InterncalComments__c, CS_Coolers_Logs__c, HONES_Case_Country__c, RecordType.DeveloperName 
                             FROM Case 
                             WHERE Id IN :  lstIdCase 
                             AND CS_Send_To_Mule__c = false 
                             AND (HONES_Case_Country__c =: countryHN OR HONES_Case_Country__c =: countryESV) 
                             AND CS_skuProduct__c != null];
            
            for(Case oSendCase : lstCaseDetail)
            {
                jsonString = generateJSONResponse(oSendCase);            
                request.setEndpoint(endPoint);
                request.setMethod('POST');
                request.setHeader('Content-Type', 'application/json;charset=UTF-8'); 
                request.setHeader('bearer', mdt.Token__c);
                request.setBody(jsonString);
                
                while(attempt < attmpLab && !flagAttempt)
                {
                    response = http.send(request);
                    if(response.getStatusCode() == statusCode)
                    {
                        bodyResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());                   
                        if(bodyResponse.get('id') == '')
                        {
                            errorMess = Label.CS_ERROR_CASE;
                            etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                            item = (Map<String,Object>) etReturn.get('item');
                            messageStatus = (String)item.get('message');

                            oSendCase.CS_Coolers_Logs__c = errorMess + ' : ' + messageStatus;              
                            oSendCase.CS_Send_To_Mule__c = false;
 
                           attempt++;
                           flagAttempt = false;
                        }
                        else
                        { 
                            etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                            item = (Map<String,Object>) etReturn.get('item');
                            successMessage = (String)item.get('message');

                            oSendCase.CS_Send_To_Mule__c = true;
                            oSendCase.CS_Coolers_Logs__c = successMessage;

                            attempt+=2;
                            flagAttempt = true; 
                         }                    
                     }
                     else
                     {
                         oSendCase.CS_Coolers_Logs__c = String.valueOf(response.getStatusCode());
                         attempt++;
                         flagAttempt = false;
                     }
                    
                     if(!lstCaseUpdate.Contains(oSendCase))
                         lstCaseUpdate.add(oSendCase);
                 }
             }
            
             /* Se actualiza la informacion de los casos */
             ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
             Update lstCaseUpdate;
         }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Stack trace: ' + ex.getStackTraceString());
        }
    }
       
    /**
    * method that send cases requests for coolers.
    * @author: d.zacarias.cantillo@accenture.com
    * @param Case that is sent to WS
    * @param Integer that controls attemps to send
    * @return Integer that controls attemps to send
    */ 
    @future(callout=true)
    public static void sendRequest(String caseCooler)
    { 
        //Variables        
        List<case> lstId = new List<Case>();
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        End_Point__mdt mdt = ([SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt Where DeveloperName = 'Create_SAP_Order']);     
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        Map<String, Object> bodyResponse = new Map<String, Object>();
        Map<String, Object> etReturn = new Map<String, Object>();
        Map<String, Object> item = new MAp<String, Object>();
        
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        String endPoint = cool.URL__c;
        String messageStatus = '';        
        String jsonString = '';
        String errorMess = '';
        String successMessage = '';
            
        Boolean flagAttempt = false;
        Integer attempt = 0; 
        Integer attmpLab = Integer.valueOf(Label.CS_Cooler_Attemps);
        Integer statusCode = Integer.valueOf(Label.CS_Status_Code);
        
        List<Case> casoForSendLst = ([SELECT Id, ISSM_NumeroSAP__c, AccountId, CS_skuProduct__c,ISSM_InterncalComments__c, CS_Coolers_Logs__c, HONES_Case_Country__c, RecordType.DeveloperName 
                                   FROM Case 
                                   WHERE Id=:caseCooler 
                             	   AND CS_Send_To_Mule__c = false 
                             	   AND (HONES_Case_Country__c =:countryHN OR HONES_Case_Country__c =:countryESV) 
                             	   AND CS_skuProduct__c != null]);
        Case casoForSend = new Case();
        if(!casoForSendLst.isEmpty()){
            casoForSend = casoForSendLst[0];
        }
        try
        {	            
            jsonString = generateJSONResponse(casoForSend);            
            request.setEndpoint(endPoint);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8'); 
            request.setHeader('bearer', mdt.Token__c);
            request.setBody(jsonString);
            
            while(attempt < attmpLab && !flagAttempt)
            {
                response = http.send(request);
                
                if(response.getStatusCode() == statusCode)
                {
                    bodyResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());                   
                    if(bodyResponse.get('id') == '')
                    {
                        errorMess = Label.CS_ERROR_CASE;
                        etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                        item = (Map<String,Object>) etReturn.get('item');
                        messageStatus = (String)item.get('message');                
                        casoForSend.CS_Coolers_Logs__c = errorMess + ' : ' + messageStatus;                     
                        casoForSend.CS_Send_To_Mule__c = false;
                        
                        flagAttempt = false;  
                        attempt++;
                    }
                    else
                    { 
                        etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                        item = (Map<String,Object>) etReturn.get('item');
                        successMessage = (String)item.get('message');
                        casoForSend.CS_Send_To_Mule__c = true;
                        casoForSend.CS_Coolers_Logs__c = successMessage;
                        
                        flagAttempt = true;
                        attempt+=2;
                    }                    
                }
                else
                {
                    casoForSend.CS_Coolers_Logs__c = String.valueOf(response.getStatusCode());
                    flagAttempt = false;	
                    attempt++;
                }
            }
            lstId.add(casoForSend);
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            ISSM_TriggerManager_cls.inactivate('CALLOUT');
            update lstId;
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Stack trace: ' + ex.getStackTraceString());
            
        }
        
    }  
    
    /**
    * method to generate the json to send.
    * @author: d.zacarias.cantillo@accenture.com
    * @param Case that is serialized to json
    * @return String json generated
    */   
    public static String generateJSONResponse(Case caseCooler)
    {
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        Map<String,Object> mapToSerialize = new Map<String,Object>();
        String json = '';
        
        CS_COOLER_REQUEST_cls partner = new CS_COOLER_REQUEST_cls();
        Map<String, List<Object>> mapPartner = new Map<String, List<Object>>();
        
        partner.role = cool.role__c;
        partner.numb = caseCooler.ISSM_NumeroSAP__c;
        partner.itmNumber = '';        
        
        mapPartner.put('partner',new List<Object>{partner});
        
        CS_COOLER_REQUEST_cls iwHeader = new CS_COOLER_REQUEST_cls();
        iwHeader.sfdcId = caseCooler.Id;
        if(caseCooler.HONES_Case_Country__c.equals(countryHN))
        {
            if(caseCooler.RecordType.DeveloperName.equals(rtInstallationCooler))
            {
                iwHeader.orderType = cool.orderTypeHNInstall__c;
            }
            else if(caseCooler.RecordType.DeveloperName.equals(rtRetirementCooler))
            {
                iwHeader.orderType = cool.orderTypeHNRetire__c;
            }
            
        }
        else if(caseCooler.HONES_Case_Country__c.equals(countryESV))
        {
            if(caseCooler.RecordType.DeveloperName.equals(rtInstallationCooler))
            {
                iwHeader.orderType = cool.orderTypeESVInstall__c;
            }
            else if(caseCooler.RecordType.DeveloperName.equals(rtRetirementCooler))
            {
                iwHeader.orderType = cool.orderTypeESVRetire__c;
            }
            
        }
        iwHeader.salesOrg = '';
        iwHeader.deliveryDate = '';
        iwHeader.paymentMethod = cool.paymentMethod__c;                     
        iwHeader.orderReason = '';
        iwHeader.customerOrder = cool.customOrder__c;
        
        CS_COOLER_REQUEST_cls items = new CS_COOLER_REQUEST_cls();
        items.sku = caseCooler.CS_skuProduct__c;
        items.position = '';
        items.quantity = (Integer)cool.quantity__c;
        items.measurecode = cool.measurecode__c;
        items.empties = '';
        
        Map<String, List<Object>> mapItems = new Map<String, List<Object>>();
        mapItems.put('items',new List<Object>{items});        
        CS_COOLER_REQUEST_cls obj4 = new CS_COOLER_REQUEST_cls();
        Map<String, List<Object>> mapDeals = new Map<String, List<Object>>();
        mapDeals.put('deals', new List<Object>());
        
        mapToSerialize.put('iwHeader',iwHeader);        
        mapToSerialize.putAll(mapPartner);
        mapToSerialize.putAll(mapItems);
        mapToSerialize.putAll(mapDeals);
        
        json = SYSTEM.JSON.serialize(mapToSerialize,true);
        
        return json;
    }
}