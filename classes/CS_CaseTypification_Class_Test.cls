/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASETYPIFICATION_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_CASETYPIFICATION_CLASS 
 */

@isTest
private class CS_CaseTypification_Class_Test
{
    /**
    * Method for test the method GetTypificationN1
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetTypificationN1()
    {
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Days_to_End__c = 0.0;
        Insert oNewTypification;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GetTypificationN1();
    }
    
    /**
    * Method for test the method GetTypificationN2
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetTypificationN2()
    {
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Days_to_End__c = 0.0;
        Insert oNewTypification;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GetTypificationN2('SAC');
    }
    
    /**
    * Method for test the method GetTypificationN3
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetTypificationN3()
    {
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Days_to_End__c = 0.0;
        Insert oNewTypification;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GetTypificationN3('SAC', 'Solicitudes');
    }
  
    /**
    * Method for test the method GetTypificationN4
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetTypificationN4()
    {
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Days_to_End__c = 0.0;
        Insert oNewTypification;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GetTypificationN4('SAC', 'Solicitudes', 'Equipo Frío');
    }
  
    /**
    * Method for test the method GetAccountDetail
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GetAccountDetail()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GetAccountDetail(oNewAccount.Id);
    }
            
    /**
    * Method for test the method GenerateNewCase
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_GenerateNewCase()
    {
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        CS_CASETYPIFICATION_CLASS obj01 = new CS_CASETYPIFICATION_CLASS();
        CS_CASETYPIFICATION_CLASS.GenerateNewCase(oNewAccount.Id, 'SAC', 'Solicitudes', 'Equipo Frío', 'Reparación', 'Caso de prueba desde clase Test');
    }
}