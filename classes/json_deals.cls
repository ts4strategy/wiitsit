/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: json_deals.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Herón Zurita          Creation of methods.
*/

public class json_deals {    
    /**
    * Methods getters and setters for entity json_deals
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    @AuraEnabled public String pronr{set;get;}
    @AuraEnabled public String posex{set;get;}
    @AuraEnabled public String condcode{set;get;}
    @AuraEnabled public Decimal amount{set;get;}
    @AuraEnabled public Decimal percentage{set;get;}
    @AuraEnabled public String description{set;get;}
    @AuraEnabled public String scaleid{set;get;}
}