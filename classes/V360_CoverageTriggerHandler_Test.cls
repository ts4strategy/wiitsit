/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_CoverageTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 03/01/2019     Carlos Leal             Creation of methods.
 */

@isTest
private class V360_CoverageTriggerHandler_Test{
    
    /**
    * method wich test the before insert method, insert an account for the test
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    @isTest static void test_beforeInsert_UseCase1() {
        V360_CoverageTriggerHandler obj01 = new V360_CoverageTriggerHandler();
        Account acc= new Account();
        acc.ONTAP__SAP_Number__c='1234567890';
        acc.Name='Test';
        insert acc;
        v360_Coverage__c obj02 = new v360_Coverage__c();
        obj02.V360_Id_client_SAP__c='1234567890';  
        obj02.V360_Last__c=true;
        
        insert obj02;
        update obj02;
    }
    
	/**
     * Method wich test after insert method, insert an account for the test
     * Created By: r.navarrete.lozano@accenture.com
     * Modify By:
     * Last Modify Date: 02/05/2019
     * @param 
     * @return void
     */
    @isTest static void test_afterInsert_UseCase1() {
        boolean validation = System.isFuture();
        System.assertEquals(false, validation);
        // Maps
    }
    
    /**
    * method wich test the before update method, insert an account for the test
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */

    @isTest static void test_beforeUpdate_UseCase1(){
        V360_CoverageTriggerHandler obj01 = new V360_CoverageTriggerHandler();
        Account obj02= new Account();
        obj02.ONTAP__SAP_Number__c='001';
        obj02.Name='Test';
        insert obj02;
        update obj02;
    }
    

}