/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_TYPIFICATION_TRIGGERHANDLER.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 21/01/2019     Carlos Leal             Creation of methods.
 */
public class CS_CASE_TYPIFICATION_TRIGGERHANDLER extends TriggerHandlerCustom{

    private Map<Id, ISSM_TypificationMatrix__c> newMap;
    private Map<Id, ISSM_TypificationMatrix__c> oldMap;
    private List<ISSM_TypificationMatrix__c> newList;
    private List<ISSM_TypificationMatrix__c> oldList;
    
    /**
    * Constructor of the class
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public CS_CASE_TYPIFICATION_TRIGGERHANDLER() {
        
        this.newMap = (Map<Id, ISSM_TypificationMatrix__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ISSM_TypificationMatrix__c>) Trigger.oldMap;
        this.newList = (List<ISSM_TypificationMatrix__c>) Trigger.new;
        this.oldList = (List<ISSM_TypificationMatrix__c>) Trigger.old;
    }
    
    /**
    * Method wich is executed every time before  product is inserted
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public override void beforeInsert(){
		addRelatedEntitlements(this.newList);        
    }
    
    /**
    * Translate codes in fields of a product in salesforce
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public void addRelatedEntitlements(List<ISSM_TypificationMatrix__c> lstprod){
        List<Entitlement> entitles = [SELECT Id,Name FROM Entitlement];
        for(ISSM_TypificationMatrix__c typifi : lstprod){
            for(Entitlement entitle : entitles){
                if(typifi.CS_Entitlement_Name__c==String.valueOf(entitle.Name)){
                    typifi.ISSM_Entitlement__c = entitle.ID;
                }
            }
        }
    }

}