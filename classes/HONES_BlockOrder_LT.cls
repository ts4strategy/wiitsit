public class  HONES_BlockOrder_LT {
   
    @AuraEnabled
    public static Boolean hasRelatedOrders(String recordId){
        Boolean hasRelatedOrder = false;
        list<ONTAP__Order__c> ordersRelated  = new list<ONTAP__Order__c>();
       
        ONTAP__Order__c order = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c  FROM ONTAP__Order__c WHERE Id =: recordId];
        if(order.ONCALL__Call__c != null){
            ordersRelated = [SELECT Id FROM ONTAP__Order__c WHERE ONCALL__Call__c =: order.ONCALL__Call__c];
        }else if (order.ONTAP__Event_Id__c != null){
            ordersRelated = [SELECT Id FROM ONTAP__Order__c WHERE ONTAP__Event_Id__c =: order.ONTAP__Event_Id__c];
        }
        
        if(ordersRelated.size() > 0){
            hasRelatedOrder = true;
        }
        return hasRelatedOrder;
    }

    @AuraEnabled
    public static CustomResponse blockOrder(String recordId){
          CustomResponse response = new CustomResponse();
          list<ONTAP__Order__c> orders = new list<ONTAP__Order__c>();
          ONTAP__Order__c order = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c, ONCALL__OnCall_Status__c,
                                          ONCALL__SAP_Order_Response__c, ONTAP__SAP_Order_Number__c
                                   FROM ONTAP__Order__c
                                   WHERE Id =: recordId];
          
          orders.add(order);

          BlockOrder blockOrder = new BlockOrder();
          blockOrder.id = order.ONTAP__SAP_Order_Number__c;
          blockOrder.sfdcId = order.Id; 
          ArrayBlockOrder abo = new ArrayBlockOrder();
          abo.ordersToBlock.add(blockOrder);

          JSONGenerator generator = JSON.createGenerator(true);
          generator.writeObject(abo);
          String jsonBlockOrder = generator.getAsString().replaceAll('\\n','');  
          response = sentJsonBlockOrder (jsonBlockOrder, orders,true);
          System.debug('HONES_BlockOrder_LT::blockOrder' + response);
          return response;

           
    }

    public static CustomResponse sentJsonBlockOrder(String json ,list<ONTAP__Order__c> orders, Boolean isTest ){
        
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt WHERE DeveloperName = 'Block_SAP_Order'];
        CustomResponse cResponse= new CustomResponse();

        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(json);
        System.debug('sentJsonBlockOrder::json sent: ' +json);
        HTTPResponse authresp = new HTTPResponse();
        try{
            authresp = h.send(httpReq);
            if(authresp.getStatusCode() == 200 && authresp.getBody() != null){
                 /*String responseText = '{"resultOrders":[{ "id": 12345, "sfdcId": "005d0000005fFkp", "success": true,  "message": "Order cancelled"}]}';
                 ResponseBlockOrderList rbol = new ResponseBlockOrderList(); //No se necesitaria si creamos otra clase aparte y colcoamos el metodo de parse como estatico
                 ResponseBlockOrderList response = rbol.parse(responseText);*/
                 System.debug('sentJsonBlockOrder::Success: ' + authresp.getBody());
                 ResponseBlockOrderList rbol = new ResponseBlockOrderList(); //No se necesitaria si creamos otra clase aparte y colcoamos el metodo de parse como estatico
                 ResponseBlockOrderList response = rbol.parse(authresp.getBody());
                 System.debug('sentJsonBlockOrder::response: ' + response);
                 for(ResponseBlockOrder bOrder : response.resultOrders){
                    for(ONTAP__Order__c order : orders){
                        if(order.Id == bOrder.sfdcId){
                            order.ONCALL__SAP_Order_Response__c = bOrder.message;
                            cResponse.status = 'OK';
                            cResponse.success = bOrder.success;
                            cResponse.message = bOrder.message;
                        }else if(isTest){
                            orders[0].ONCALL__SAP_Order_Response__c = bOrder.message;
                            cResponse.status = 'OK';
                            cResponse.success = bOrder.success;
                            cResponse.message = bOrder.message;
                        }
                    }
                 }
            }else{
                 cResponse.status = 'ERROR';
                 cResponse.message = 'CONSULT ADMIN 1:' + authresp.getBody() + ':: StatusCode:: ' + authresp.getStatusCode();
            }
        }catch(Exception e){
            cResponse.status = 'ERROR';
            cResponse.message = 'CONSULT ADMIN 2:' + e.getMessage();
        }finally{
            ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            update orders;
        }
        return cResponse;
    }

    public class BlockOrder {
        String id {get; set;}
        String sfdcId {get; set;}
    }

    public class ArrayBlockOrder{
        public list<BlockOrder> ordersToBlock {get;set;}

        public ArrayBlockOrder(){
            ordersToBlock = new list<BlockOrder>();
        }
    }

    public class ResponseBlockOrderList{
        public list<ResponseBlockOrder> resultOrders {get;set;}

        public  ResponseBlockOrderList parse(String json){
		    return (ResponseBlockOrderList) System.JSON.deserialize(json, ResponseBlockOrderList.class);
	    }
    }

    public class ResponseBlockOrder{
        public Integer id {get;set;}
		public String sfdcId {get;set;}
		public boolean success {get;set;}
		public String message {get;set;}	
    }

    
    public class CustomResponse{
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public boolean success {get;set;}
        public CustomResponse(){}
    }
}