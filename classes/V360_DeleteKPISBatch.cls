/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteKPISBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteKPISBatch implements Database.Batchable<sObject>{
    
    global Boolean hasErrors {get; private set;}

   /**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteKPISBatch()
    {  
        this.hasErrors = false;
    }
    
    /**
   * Execute the Query for the Kpi objects.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug('Processing start method'); 
        Id V360_RT = Schema.SObjectType.ONTAP__KPI__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.KPI_V360_RECORDTYPE_NAME).getRecordTypeId();

        // Query string for batch Apex
        String query = '';
        Date todayDate = System.Today();

        if(System.Test.isRunningTest()){
            query += 'SELECT Id FROM ONTAP__KPI__c';
        }else{
            query += 'SELECT Id FROM ONTAP__KPI__c WHERE RecordTypeId =:V360_RT AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Metod for delete all the Kpi objects. 
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<ONTAP__KPI__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<ONTAP__KPI__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Metod finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}