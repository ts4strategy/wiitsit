/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: HONES_campaignSchedule.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 05/03/2018     Carlos Leal  		      Creation of methods.
 */
global class HONES_campaignSchedule implements schedulable{

    /**
   * Execute a Schudeler for execute the batch for create the daily campaing. 
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global void execute(SchedulableContext sc)
    {
    HONES_TelecollectioncampaignBatch b = new HONES_TelecollectioncampaignBatch();
      database.executebatch(b);
    }

}