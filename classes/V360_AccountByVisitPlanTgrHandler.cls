public class V360_AccountByVisitPlanTgrHandler extends TriggerHandlerCustom {
    /* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_AccountByVisitPlanTgrHandler.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 03/01/2019     Gerardo Martinez             Creation of methods.
*/

    private Map<Id,AccountByVisitPlan__c> newMap;
    private Map<Id,AccountByVisitPlan__c> oldMap;
    private List<AccountByVisitPlan__c> newList;
    private List<AccountByVisitPlan__c> oldList;
    
    
   /**
    * Constructor of the class
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public V360_AccountByVisitPlanTgrHandler() {  
        this.newMap = (Map<Id, AccountByVisitPlan__c>) Trigger.newMap;
        this.oldMap = (Map<Id, AccountByVisitPlan__c>) Trigger.oldMap;
        this.newList = (List<AccountByVisitPlan__c>) Trigger.new;
        this.oldList = (List<AccountByVisitPlan__c>) Trigger.old;
    }
    
    public override void beforeDelete(){
         deleteOldFrecuencyAtAccountLevel(this.oldMap.keySet());
    }
    
    public override void afterInsert(){
        setVisitDaysFrequencyAtAccountLevel(this.newMap.keySet());
    }
    
    public override void afterUpdate(){
        setVisitDaysFrequencyAtAccountLevel(this.newMap.keySet());
    }
    
    
    
    /**
    * Method wich Set the Frequency At Account Level
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */

    public static void setVisitDaysFrequencyAtAccountLevel(Set<Id> newSet){
        Set<Id> accs = new Set<Id>();
        
        for( List<AccountByVisitPlan__c> accsByPlan :  [SELECT Id, Account__c, PlanType__c, VisitDays__c FROM AccountByVisitPlan__c WHERE Id IN: newSet] ){
            
            for(AccountByVisitPlan__c accByPlan : accsByPlan){
                if(accByPlan.Account__c != NULL){
                    accs.add(accByPlan.Account__c);
                }
            }
        }
        
        Map<Id, Account> accMap = new Map<Id, Account> ([SELECT Id, V360_CallFrequency__c, V360_VisitFrequency__c From Account  WHERE Id IN:accs]);
        
        for( List<AccountByVisitPlan__c> accsByPlan :  [SELECT Id, Account__c, PlanType__c, VisitDays__c FROM AccountByVisitPlan__c WHERE Id IN: newSet ] ){
            
            for(AccountByVisitPlan__c accByPlan : accsByPlan){
                
                if(accByPlan.PlanType__c != NULL && accByPlan.VisitDays__c != NULL &&  accByPlan.PlanType__c == GlobalStrings.TELESALES){
                    
                    accMap.get(accByPlan.Account__c).V360_CallFrequency__c=concatNewFrequency(  accMap, accByPlan );
                    
                }else if(accByPlan.PlanType__c != NULL && accByPlan.VisitDays__c != NULL && accByPlan.PlanType__c == GlobalStrings.BDR || accByPlan.PlanType__c == GlobalStrings.PRESALES ){
                    accMap.get(accByPlan.Account__c).V360_VisitFrequency__c=concatNewFrequency(  accMap, accByPlan );
                }
            }
        }
        
        update accMap.values();
 
    }
    
    
     /**
    * Method wich calculate the Frequency by each record of account by visit plan
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public static String concatNewFrequency(  Map<Id, Account> accMap,AccountByVisitPlan__c accByPlan ){
        String Result = '';
        String source = '';
        
        Account acc = accMap.get(accByPlan.Account__c);
        
        if (accByPlan.PlanType__c == GlobalStrings.TELESALES && acc != NULL)
        {
            source = (acc.V360_CallFrequency__c!=NULL?acc.V360_CallFrequency__c:NULL);
        }else{
            source = (acc.V360_VisitFrequency__c!=NULL?acc.V360_VisitFrequency__c:NULL);
        }
        
        String newValues = accByPlan.VisitDays__c;
        
        if (source != NULL && !source.contains(newValues)){
            
            String[] charsSource = source.split(''); 
            String[] charsNewValues = newValues.split('');
            
            for(String letter: charsNewValues){
                
                Boolean isNotRepeated = true;
                for(String letter2:charsSource){
                    
                    isNotRepeated = (letter==letter2?false:true);
                }
                source = (isNotRepeated? source + letter:source);
            }
            result = source;
            
        }else if (source == NULL){
            
            result=newValues;
        }
        return result;
    }
    
   /**
    * Method wich delete the Frequency At Account Level
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
    
    public static void deleteOldFrecuencyAtAccountLevel(Set<Id> oldSet){
        
        Set<Id> accs = new Set<Id>();
        
        for( List<AccountByVisitPlan__c> accsByPlan :  [SELECT Id, Account__c, PlanType__c, VisitDays__c FROM AccountByVisitPlan__c WHERE Id IN: oldSet] ){
            
            for(AccountByVisitPlan__c accByPlan : accsByPlan){
                if(accByPlan.Account__c != NULL){
                    accs.add(accByPlan.Account__c);
                }
            }
        }
        
        Map<Id, Account> accMap = new Map<Id, Account> ([SELECT Id, V360_CallFrequency__c, V360_VisitFrequency__c From Account  WHERE Id IN:accs]);
        
        for( List<AccountByVisitPlan__c> accsByPlan :  [SELECT Id, Account__c, PlanType__c, VisitDays__c FROM AccountByVisitPlan__c WHERE Id IN: oldSet ] ){
            
            for(AccountByVisitPlan__c accByPlan : accsByPlan){
                if(accByPlan.PlanType__c != NULL && accByPlan.VisitDays__c != NULL &&  accByPlan.PlanType__c == GlobalStrings.TELESALES){
                    
                    accMap.get(accByPlan.Account__c).V360_CallFrequency__c=removeOldFrequency(  accMap, accByPlan );
                    
                }else if(accByPlan.PlanType__c != NULL && accByPlan.VisitDays__c != NULL && accByPlan.PlanType__c == GlobalStrings.BDR || accByPlan.PlanType__c == GlobalStrings.PRESALES ){
                    accMap.get(accByPlan.Account__c).V360_VisitFrequency__c=removeOldFrequency(  accMap, accByPlan );
                }
            }
        }
        System.debug('Acc: '+accMap.values());
        update accMap.values(); 
    }

     /**
    * Method wich remove the Frequency by each record of account by visit plan
    * Created By: c.leal.beltran@accenture.com
    * @param New values of Product modified
    * @return void
    */
 	public static String removeOldFrequency(  Map<Id, Account> accMap,AccountByVisitPlan__c accByPlan ){
        String result = '';
        String source = '';
        
        Account acc = accMap.get(accByPlan.Account__c);
        
        if (accByPlan.PlanType__c == GlobalStrings.TELESALES&& acc != NULL)
        {
            source = (acc.V360_CallFrequency__c!=NULL?acc.V360_CallFrequency__c:NULL);
        }else{
            source = (acc.V360_VisitFrequency__c!=NULL?acc.V360_VisitFrequency__c:NULL);
        }
        
        String oldValues = accByPlan.VisitDays__c;
        
        System.debug('Old Values:'+oldValues);
        System.debug('source:'+source);
        
        if (source != NULL && source.contains(oldValues)){
            
            String[] charsSource = source.split(''); 
            String[] charsOldValues = oldValues.split('');
            for(String letter: charsSource){        
                
                Boolean needsToBeDeleted = false;
                for(String letter2:charsOldValues){
                    
                    if(letter!=letter2){
                        needsToBeDeleted = false;
                    }else{
                        needsToBeDeleted = true;
                        break;
                    }                    
                    System.debug(needsToBeDeleted + ' ' + letter +' '+  letter2);
                }
                result = (needsToBeDeleted? result:result + letter);
            }            
        }
        System.debug('RRR: '+result);
        return result;
     
    }    
}