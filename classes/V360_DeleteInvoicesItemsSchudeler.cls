/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteInvoicesItemsSchudeler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteInvoicesItemsSchudeler  implements schedulable
{
   /**
   * Execute a Schudeler for execute the batch for delete all the Invoice items.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global void execute(SchedulableContext sc)
    {
    V360_DeleteInvoicesItemsBatch c = new V360_DeleteInvoicesItemsBatch();
      database.executebatch(c,200);
    }
}