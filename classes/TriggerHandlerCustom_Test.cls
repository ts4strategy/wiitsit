/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: TriggerHandlerCustom_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
private class TriggerHandlerCustom_Test {

  private static final String TRIGGER_CONTEXT_ERROR = 'Trigger handler called outside of Trigger execution';

  private static String lastMethodCalled;

  private static TriggerHandlerCustom_Test.TestHandler handler;

  static {
    handler = new TriggerHandlerCustom_Test.TestHandler();
    // override its internal trigger detection
    handler.isTriggerExecuting = true;
  }

  /***************************************
   * unit tests
   ***************************************/

  // contexts tests
 
  /**
  * Method for test the before insert.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testBeforeInsert() {
    beforeInsertMode();
    handler.run();
    System.assertEquals('beforeInsert', lastMethodCalled, 'last method should be beforeInsert');
  }
    
  /**
  * Method for test the before update.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testBeforeUpdate() {
    beforeUpdateMode();
    handler.run();
    System.assertEquals('beforeUpdate', lastMethodCalled, 'last method should be beforeUpdate');
  }

  /**
  * Method for test the before delete.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testBeforeDelete() {
    beforeDeleteMode();
    handler.run();
    System.assertEquals('beforeDelete', lastMethodCalled, 'last method should be beforeDelete');
  }

  /**
  * Method for test the after insert.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testAfterInsert() {
    afterInsertMode();
    handler.run();
    System.assertEquals('afterInsert', lastMethodCalled, 'last method should be afterInsert');
  }

  /**
  * Method for test the after update.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testAfterUpdate() {
    afterUpdateMode();
    handler.run();
    System.assertEquals('afterUpdate', lastMethodCalled, 'last method should be afterUpdate');
  }

  /**
  * Method for test the after delete.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testAfterDelete() {
    afterDeleteMode();
    handler.run();
    System.assertEquals('afterDelete', lastMethodCalled, 'last method should be afterDelete');
  }

  /**
  * Method for test the after undelete.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testAfterUndelete() {
    afterUndeleteMode();
    handler.run();
    System.assertEquals('afterUndelete', lastMethodCalled, 'last method should be afterUndelete');
  }

  /**
  * Method for test the trigger for non context.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest 
  static void testNonTriggerContext() {
    try{
      handler.run();
      System.assert(false, 'the handler ran but should have thrown');
    } catch(TriggerHandlerCustom.TriggerHandlerException te) {
      System.assertEquals(TRIGGER_CONTEXT_ERROR, te.getMessage(), 'the exception message should match');
    } catch(Exception e) {
      System.assert(false, 'the exception thrown was not expected: ' + e.getTypeName() + ': ' + e.getMessage());
    }
  }

  /**
  * Method for test the trigger bypass api.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testBypassAPI() {
    afterUpdateMode();

    // test a bypass and run handler
    TriggerHandlerCustom.bypass('TestHandler');
    handler.run();
    System.assertEquals(null, lastMethodCalled, 'last method should be null when bypassed');
    System.assertEquals(true, TriggerHandlerCustom.isBypassed('TestHandler'), 'test handler should be bypassed');
    resetTest();

    // clear that bypass and run handler
    TriggerHandlerCustom.clearBypass('TestHandler');
    handler.run();
    System.assertEquals('afterUpdate', lastMethodCalled, 'last method called should be afterUpdate');
    System.assertEquals(false, TriggerHandlerCustom.isBypassed('TestHandler'), 'test handler should be bypassed');
    resetTest();

    // test a re-bypass and run handler
    TriggerHandlerCustom.bypass('TestHandler');
    handler.run();
    System.assertEquals(null, lastMethodCalled, 'last method should be null when bypassed');
    System.assertEquals(true, TriggerHandlerCustom.isBypassed('TestHandler'), 'test handler should be bypassed');
    resetTest();

    // clear all bypasses and run handler
    TriggerHandlerCustom.clearAllBypasses();
    handler.run();
    System.assertEquals('afterUpdate', lastMethodCalled, 'last method called should be afterUpdate');
    System.assertEquals(false, TriggerHandlerCustom.isBypassed('TestHandler'), 'test handler should be bypassed');
    resetTest();
  }

  /**
  * Method for test the trigger of instance method.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testLoopCount() {
    beforeInsertMode();
    
    // set the max loops to 2
    handler.setMaxLoopCount(2);

    // run the handler twice
    handler.run();
    handler.run();

    // clear the tests
    resetTest();

    try {
      // try running it. This should exceed the limit.
      handler.run();
      System.assert(false, 'the handler should throw on the 3rd run when maxloopcount is 3');
    } catch(TriggerHandlerCustom.TriggerHandlerException te) {
      // we're expecting to get here
      System.assertEquals(null, lastMethodCalled, 'last method should be null');
    } catch(Exception e) {  
      System.assert(false, 'the exception thrown was not expected: ' + e.getTypeName() + ': ' + e.getMessage());
    }

    // clear the tests
    resetTest();

    // now clear the loop count
    handler.clearMaxLoopCount();

    try {
      // re-run the handler. We shouldn't throw now.
      handler.run();
      System.assertEquals('beforeInsert', lastMethodCalled, 'last method should be beforeInsert');
    } catch(TriggerHandlerCustom.TriggerHandlerException te) {
      System.assert(false, 'running the handler after clearing the loop count should not throw');
    } catch(Exception e) {  
      System.assert(false, 'the exception thrown was not expected: ' + e.getTypeName() + ': ' + e.getMessage());
    }
  }

  /**
  * Method for test the trigger of loop count.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testLoopCountClass() {
    TriggerHandlerCustom.LoopCount lc = new TriggerHandlerCustom.LoopCount();
    System.assertEquals(5, lc.getMax(), 'max should be five on init');
    System.assertEquals(0, lc.getCount(), 'count should be zero on init');

    lc.increment();
    System.assertEquals(1, lc.getCount(), 'count should be 1');
    System.assertEquals(false, lc.exceeded(), 'should not be exceeded with count of 1');

    lc.increment();
    lc.increment();
    lc.increment();
    lc.increment();
    System.assertEquals(5, lc.getCount(), 'count should be 5');
    System.assertEquals(false, lc.exceeded(), 'should not be exceeded with count of 5');

    lc.increment();
    System.assertEquals(6, lc.getCount(), 'count should be 6');
    System.assertEquals(true, lc.exceeded(), 'should not be exceeded with count of 6');
  }

  /**
  * Method for test the trigger for get handler name method.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest 
  static void testGetHandlerName() {
    System.assertEquals('TestHandler', handler.getHandlerName(), 'handler name should match class name');
  }

  /**
  * Method for test the trigger of virtual method.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest
  static void testVirtualMethods() {
    TriggerHandlerCustom h = new TriggerHandlerCustom();
    h.beforeInsert();
    h.beforeUpdate();
    h.beforeDelete();
    h.afterInsert();
    h.afterUpdate();
    h.afterDelete();
    h.afterUndelete();
  }

  /***************************************
   * testing utilities
   ***************************************/
  
  /**
  * Method for reset the test.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void resetTest() {
    lastMethodCalled = null;
  }

  /**
  * Method for before insert mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void beforeInsertMode() {
  handler.setTriggerContext('before insert', true);
  }

  /**
  * Method for before update mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void beforeUpdateMode() {
    handler.setTriggerContext('before update', true);
  }

  /**
  * Method for before delete mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void beforeDeleteMode() {
    handler.setTriggerContext('before delete', true);
  }

  /**
  * Method for after insert mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void afterInsertMode() {
    handler.setTriggerContext('after insert', true);
  }

  /**
  * Method for after update mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void afterUpdateMode() {
    handler.setTriggerContext('after update', true);
  }

  /**
  * Method for after delete mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void afterDeleteMode() {
    handler.setTriggerContext('after delete', true);
  }

  /**
  * Method for after undelete mode.
  * @author: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  private static void afterUndeleteMode() {
    handler.setTriggerContext('after undelete', true);
  }

  // test implementation of the TriggerHandlerCustom

  private class TestHandler extends TriggerHandlerCustom {
	
    /**
    * Method for before insert.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeInsert() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'beforeInsert';
    }
      
	/**
    * Method for before update.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void  beforeUpdate() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'beforeUpdate';
    }

    /**
    * Method for before delete.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeDelete() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'beforeDelete';
    }
	
    /**
    * Method for after insert.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void afterInsert() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'afterInsert';
    }

    /**
    * Method for after update.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void afterUpdate() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'afterUpdate';
    }

    /**
    * Method for after delete.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void afterDelete() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'afterDelete';
    }

    /**
    * Method for after undelete.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void afterUndelete() {
      TriggerHandlerCustom_Test.lastMethodCalled = 'afterUndelete';
    }
  }	
}