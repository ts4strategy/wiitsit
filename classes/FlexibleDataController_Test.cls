/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: FlexibleDataController_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
private class FlexibleDataController_Test{
  
    /**
    * Test method for set parameters 
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupTestData(){
        test.startTest();
        
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id finishProduct = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByName().get('Finished Product').getRecordTypeId();
        Id emptiesIntroduction = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByName().get('Empties Introduction').getRecordTypeId();
        
        User u = [Select Id FROM User Limit 1];  
        Account a = New Account(ONTAP__ExternalKey__c='SV',Name ='ABC', RecordTypeId=salesZonedevRecordTypeId, ISSM_CreditLimit__c = 1200.00,ONTAP__SalesOgId__c='CS02',ONTAP__SAP_Number__c='TESTSAP');        
        insert a;
        V360_FlexibleData__c flexObj= new V360_FlexibleData__c(V360_Account__c = a.Id, Name = 'test', V360_Type__c= 'test type', V360_Value__c = 'test value', V360_ValidTo__c = Date.today());
        insert flexObj;        
        ONTAP__Order__c orderObj = new ONTAP__Order__c(ONCALL__OnCall_Account__c = a.Id);
        insert orderObj;        
        ONCALL__Call__c callObj = new ONCALL__Call__c(ONCALL__POC__c = a.Id);
        insert callObj;                    
        test.stopTest();
    }
    
    /**
    * Test to get all the promos for a user by Id    
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_getpromosId_UseCase1(){
    Account accTest = new Account(Name = 'test account');
    insert accTest;
    ONCALL__Call__c callTest = new ONCALL__Call__c(ONCALL__POC__c = accTest.Id);
    insert callTest;
    FlexibleDataController obj01 = new FlexibleDataController();    
    FlexibleDataController.getpromosId(callTest.Id);
          
  }
    
    @isTest static void test_getpromosId_UseCase1_1(){
    Account accTest = new Account(Name = 'test account');
    insert accTest;
    ONCALL__Call__c callTest = new ONCALL__Call__c(ONCALL__POC__c = accTest.Id);
    insert callTest;
    FlexibleDataController obj01 = new FlexibleDataController();    
    FlexibleDataController.getpromosId(null);
          
  }
    
  /**
  * Test method promos Use  
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getpromos_UseCase1(){
    FlexibleDataController obj01 = new FlexibleDataController();
         FlexibleDataController.getpromos('test');
      
  }
    
  /**
  * Test method Account from call type 1  
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getAccountFromCall_UseCase1(){
    FlexibleDataController obj01 = new FlexibleDataController();
    FlexibleDataController.getAccountFromCall('test data');
  }
  
  /**
  * Test method Account from call type 2  
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getAccountFromCall2_UseCase1(){
    FlexibleDataController obj01 = new FlexibleDataController();
    FlexibleDataController.getAccountFromCall2('test data');
  }
    
  /**
  * Test method from flexible data Account 
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getFlexibleIdAccount_UseCase1(){
      List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account];
      List<Account> lstAcc2 = null;
    FlexibleDataController obj01 = new FlexibleDataController();
    FlexibleDataController.getFlexibleIdAccount(lstAcc[0].Id);
      FlexibleDataController.getFlexibleIdAccount(lstAcc[0].Id);
  }
    
  /**
  * Test method to get a specific flexible data  from an account
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getFlexibleData_UseCase1(){
    List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account];
      
    FlexibleDataController obj01 = new FlexibleDataController();
    FlexibleDataController.getFlexibleData(lstAcc[0].Id+'1'); 
  }
    
  /**
  * Test method from list of all flexible data
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */
  @isTest static void test_getGroupedLists_UseCase1(){
    FlexibleDataController obj01 = new FlexibleDataController();
    List<V360_FlexibleData__c> lstV = [SELECT Name, V360_Type__c,V360_Value__c FROM V360_FlexibleData__c ];
    List<V360_FlexibleData__c> lstV2 =null;
    FlexibleDataController.getGroupedLists(lstV);
    FlexibleDataController.getGroupedLists(lstv2);
  }
    
  /**
  * Test method to test the creation of an order from an account
  * Created By: g.martinez.cabral@accenture.com
  * @param void
  * @return void
  */

  @isTest static  void  test_genOrder(){
              
     	Account lstAcc = new Account();
      	lstAcc.ONTAP__SAP_Number__c='93487';
        lstAcc.ONTAP__Credit_Condition__c='Credit';
     	lstAcc.Name='teste';
        lstAcc.ONTAP__ExternalKey__c='SVCGH';
      	insert lstAcc;
      	String ids=lstAcc.Id;     
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorFlexibleData());
        test.startTest();
        FlexibleDataController obj01 = new FlexibleDataController();
        FlexibleDataController.genOrder(ids);       
        test.stopTest(); 
       
  }
}