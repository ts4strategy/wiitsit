/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteEmptyBalanceBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteEmptyBalanceBatch implements Database.Batchable<sObject>{
    
    global Boolean hasErrors {get; private set;}

    /**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteEmptyBalanceBatch()
    {  
        this.hasErrors = false;
    }
    
 /**
   * Execute the Query for the account empty balance objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Id V360_EmptyBalanceRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        Date todayDate = System.Today();
        if(System.Test.isRunningTest()){
            query += 'SELECT Id FROM ONTAP__EmptyBalance__c WHERE RecordTypeId=:V360_EmptyBalanceRT';
        }else{
        	query += 'SELECT Id FROM ONTAP__EmptyBalance__c WHERE RecordTypeId=:V360_EmptyBalanceRT AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
  /**
   * Method for delete all the account empty balance objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<ONTAP__EmptyBalance__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<ONTAP__EmptyBalance__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}