/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_CASEASSETACCOUNT_CLS_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 11/02/2019           Debbie Zacarias        Creation of the test class for CS_CASEASSETACCOUNT_CLS
*/

@isTest
private class CS_CASEASSETACCOUNT_CLS_Test{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupTestData()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        RecordType rectype = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtInstallationCooler]);
        RecordType rectypeSV = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtRetirementCooler]);
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Subject';
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        caseTest.HONES_Case_Country__c = 'Honduras';
        caseTest.CS_Send_To_Mule__c = false;
        caseTest.RecordTypeId = rectype.id;
        insert caseTest;
        
        ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c();
        assetAcc.ONTAP__Asset_Description__c = 'Description';
        assetAcc.HONES_EquipmentNumber__c='1234567890';
        assetAcc.HONES_Asset_Model__c='model';
        assetAcc.ONTAP__Serial_Number__c='0987654321';
        assetAcc.V360_MaterialNumber__c='1029384756';
        assetAcc.ONTAP__Brand__c='Brand';
        assetAcc.ONTAP__Account__c = accountTest.id;
        Insert assetAcc;
        
    }
    
    /**
    * Method for test the method getBrand
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getBrand()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        CS_CASEASSETACCOUNT_CLS.getBrand(caso.Id);
    }
    
    /**
    * Method for test the method getModel
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getModel()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        ONTAP__Account_Asset__c asstAcc= ([SELECT ONTAP__Brand__c 
                                           FROM ONTAP__Account_Asset__c LIMIT 1]);
        CS_CASEASSETACCOUNT_CLS.getModel(caso.Id,asstAcc.ONTAP__Brand__c);
    }
    
    /**
    * Method for test the method getSerialNumber
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getSerialNumber()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        ONTAP__Account_Asset__c asstAcc= ([SELECT ONTAP__Brand__c, HONES_Asset_Model__c
                                           FROM ONTAP__Account_Asset__c LIMIT 1]);
        CS_CASEASSETACCOUNT_CLS.getSerialNumber(caso.Id,asstAcc.ONTAP__Brand__c,asstAcc.HONES_Asset_Model__c);
    }
    
    /**
    * Method for test the method getEquipmentNumber
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getEquipmentNumber()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        ONTAP__Account_Asset__c asstAcc= ([SELECT ONTAP__Brand__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c
                                           FROM ONTAP__Account_Asset__c LIMIT 1]);
        
        CS_CASEASSETACCOUNT_CLS.getEquipmentNumber(caso.Id,asstAcc.ONTAP__Brand__c,asstAcc.HONES_Asset_Model__c,asstAcc.ONTAP__Serial_Number__c);
    }
    
    /**
    * Method for test the method getDescription
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getDescription()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        ONTAP__Account_Asset__c asstAcc= ([SELECT ONTAP__Brand__c, HONES_Asset_Model__c, HONES_EquipmentNumber__c, ONTAP__Serial_Number__c
                                           FROM ONTAP__Account_Asset__c LIMIT 1]);
        
        CS_CASEASSETACCOUNT_CLS.getDescription(caso.Id,asstAcc.ONTAP__Brand__c,asstAcc.HONES_Asset_Model__c,asstAcc.ONTAP__Serial_Number__c, asstAcc.HONES_EquipmentNumber__c);
    }
    
    /**
    * Method for test the method SetAssetCaseInfo
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_SetAssetCaseInfo()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo(caso.Id);
    }
    
    /**
    * Method for test the method UpdateCase
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_UpdateCase()
    {
        
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        ONTAP__Account_Asset__c asstAcc= ([SELECT ONTAP__Brand__c, HONES_Asset_Model__c, HONES_EquipmentNumber__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c, ONTAP__Asset_Description__c
                                           FROM ONTAP__Account_Asset__c LIMIT 1]);
        
        CS_CASEASSETACCOUNT_CLS.UpdateCase(caso.Id,asstAcc.ONTAP__Brand__c,asstAcc.HONES_Asset_Model__c,asstAcc.ONTAP__Serial_Number__c, asstAcc.HONES_EquipmentNumber__c, asstAcc.ONTAP__Asset_Description__c,asstAcc.V360_MaterialNumber__c);
        
    }
}