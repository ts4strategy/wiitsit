/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_DeleteInvoicesItemsBatch_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2018     Carlos Leal        Creation of methods.
*/
@isTest
private class V360_DeleteInvoicesItemsBatch_Test{
    /**
* Method test for delete all the invoice items data in V360_DeleteInvoicesItemsBatch.
* @author: c.leal.beltran@accenture.com
* @param void
* @return void
*/
    @isTest static void testBatch(){
        ONCALL__Invoice__c ob1 = new ONCALL__Invoice__c();
        insert ob1;
        
		ONCALL__Invoice_Item__c ob2 = new ONCALL__Invoice_Item__c(ONCALL__Invoice__c=ob1.Id);
        insert ob2;
        V360_DeleteInvoicesItemsBatch obj01 = new V360_DeleteInvoicesItemsBatch();
        Database.executeBatch(obj01, 200);
    }
}