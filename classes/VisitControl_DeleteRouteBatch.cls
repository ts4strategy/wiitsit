/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_DeleteRouteBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
global class VisitControl_DeleteRouteBatch implements Database.Batchable<sObject>{
   
    // Persistent variables
    global Boolean hasErrors {get; private set;}
	
    /**
    * Clean the class.
    * @author: g.martinez.cabral@accenture.com
    */
    global VisitControl_DeleteRouteBatch()
    {  
        this.hasErrors = false;
    }
    
    /**
   * Execute the Query for the Retrace and anticipate Route objects.
   * @author:  g.martinez.cabral@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        query += 'SELECT Id FROM VisitControl_RetraceAndAnticipateRoute__c WHERE VisitControl_Deleted__c = TRUE';
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Method for delete all the Retrace and anticipate Route objects.
   * @author: g.martinez.cabral@accenture.com
   * @param Database.BatchableContext BC, List<ONTAP__Account_Asset__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<VisitControl_RetraceAndAnticipateRoute__c> objectBatch)
    { 
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Method finish for close Retrace and anticipate Route process
   * @author: g.martinez.cabral@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}