/*
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 08/01/2019           Heron Zurita           Creation of methods.
*/
public class Lookup {
	/*
	*Method  for search in Database according to the parameters entered
	*By:heron.zurita@accenture.com
    * @param String objectName,String fld_API_Text,String fld_API_Val,Integer lim,String fld_API_Search,String searchText
    * @return JSON(List<ResultWrapper>)
	*/
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText ){
        
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';

        
        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
            			' FROM '+objectName+
            				' WHERE '+fld_API_Search+' LIKE '+searchText+ 
            			' LIMIT '+lim;
        
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            lstRet.add(obj);
        } 
         return JSON.serialize(lstRet) ;
    }
    
    public class ResultWrapper{
    /**
    * Methods getters and setters for entity ResultWrapper
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
    }
}