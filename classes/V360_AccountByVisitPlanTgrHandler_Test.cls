@isTest public class V360_AccountByVisitPlanTgrHandler_Test {
    
    @TestSetup static void buildData(){
            Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
            Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
            Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
            Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
            Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
            
            User u = [Select Id FROM User Limit 1];  
            
            Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
            insert og;
            Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
            insert off;
            Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
            insert cg;
            Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id, V360_TeamLead__c = u.Id);
            insert sg;
            Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
            insert szz;
    
            V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id,V360_Type__c=GlobalStrings.PRESALES);
            insert sz;
			
        	V360_SalerPerZone__c sz2 = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id,V360_Type__c=GlobalStrings.TELESALES);
			insert sz2;
        	
            Account acc = new Account(Name='1',V360_SalesZoneAssignedBDR__c=sz.Id,V360_SalesZoneAssignedTelesaler__c=sz2.Id,V360_SalesZoneAssignedPresaler__c=sz.Id,V360_SalesZoneAssignedCredit__c=sz.Id,V360_SalesZoneAssignedTellecolector__c=sz.Id,V360_SalesZoneAssignedCoolers__c=sz.Id);           
         	insert acc;
        
            Id routeRTypeId = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.VISIT_CONTROL_ROUTE).getRecordTypeId();
            ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,Supervisor__c=u.id,VisitControl_SalesZone__c=szz.Id, VisitControl_AgentByZone__c =sz.Id, ServiceModel__c=GlobalStrings.PRESALES,VisitControl_Bypass__c = true,RecordTypeId=routeRTypeId);
            insert rt;
        
        	ONTAP__Route__c rt2 = New ONTAP__Route__c(RouteManager__c = u.Id,Supervisor__c=u.id,VisitControl_SalesZone__c=szz.Id,VisitControl_AgentByZone__c =sz2.Id, ServiceModel__c=GlobalStrings.TELESALES,VisitControl_Bypass__c = true,RecordTypeId=routeRTypeId);
            insert rt2;
                        
            VisitPlan__c vp= New VisitPlan__c(Route__c = rt.Id,Wednesday__c =true, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7, VisitPlanId__c='1');
            insert vp;
        
            VisitPlan__c vp2= New VisitPlan__c(Route__c = rt.Id, Monday__c=True, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7,VisitPlanId__c='2');
            insert vp2;
        
        	VisitPlan__c vp3= New VisitPlan__c(Route__c = rt2.Id,Wednesday__c =true, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7, VisitPlanId__c='1');
            insert vp3;
        
            VisitPlan__c vp4= New VisitPlan__c(Route__c = rt2.Id, Monday__c=True, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7,VisitPlanId__c='2');
            insert vp4;
        	
    }

    @isTest
    public static void testSetFrequencyAtAccountLeveL(){
        test.startTest();
			
        List<Account> lst = [SELECT Id FROM Account WHERE Name = '1'];
        List<ONTAP__Route__c> lstR = [SELECT Id,ServiceModel__c FROM ONTAP__Route__c];
        
        for (ONTAP__Route__c Route :lstR){
            
            if(GlobalStrings.TELESALES == Route.ServiceModel__c){
                
                AccountByRoute__c accby1 = new AccountByRoute__c(Route__c=Route.Id, Account__c=lst.get(0).Id, Monday__c=True,Wednesday__c=True);
                insert accby1;
                                                	
            }else{
                AccountByRoute__c accby1 = new AccountByRoute__c(Route__c=Route.Id, Account__c=lst.get(0).Id, Monday__c=True,Wednesday__c=True);
                insert accby1;
            }
        }
        
     
        
        System.debug('TEST: VISIT PLAN: '+[SELECT Id, Account__c, PlanType__c, VisitDays__c FROM AccountByVisitPlan__c]);
        
        Map<Id, AccountByVisitPlan__c> oldMap=new Map<Id, AccountByVisitPlan__c>([SELECT Id FROM AccountByVisitPlan__c]);
        V360_AccountByVisitPlanTgrHandler.setVisitDaysFrequencyAtAccountLevel(oldMap.keySet());
        
        test.stopTest();
        
            
        	
    }
    
    @isTest
    public static void testDeleteFrequencyAtAccountLeveL(){
       Test.startTest();
			
        List<Account> lst = [SELECT Id FROM Account WHERE Name = '1'];
        List<ONTAP__Route__c> lstR = [SELECT Id,ServiceModel__c FROM ONTAP__Route__c];
        
        for (ONTAP__Route__c Route :lstR){
            
            if(GlobalStrings.TELESALES == Route.ServiceModel__c){
                
                AccountByRoute__c accby1 = new AccountByRoute__c(Route__c=Route.Id, Account__c=lst.get(0).Id, Monday__c=True,Wednesday__c=True);
                insert accby1;
                                                	
            }else{
                AccountByRoute__c accby1 = new AccountByRoute__c(Route__c=Route.Id, Account__c=lst.get(0).Id, Monday__c=True,Wednesday__c=True);
                insert accby1;
            }
        }
        lst.get(0).V360_CallFrequency__c = 'LR';
        lst.get(0).V360_VisitFrequency__c = 'LR';
        update lst;
        
        Map<Id, AccountByVisitPlan__c> oldMap=new Map<Id, AccountByVisitPlan__c>([SELECT Id FROM AccountByVisitPlan__c]);
        V360_AccountByVisitPlanTgrHandler.deleteOldFrecuencyAtAccountLevel(oldMap.keySet());
        test.stopTest();
    }

        
}