/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_OpenItemsTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
private class V360_OpenItemsTriggerHandler_Test{
    
    /**
    * Test method wich is executed every time is inserted
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_beforeInsert_UseCase1(){
        V360_OpenItemsTriggerHandler obj01 = new V360_OpenItemsTriggerHandler();
        Account acc = new Account(Name = '1234567890', ONTAP__SAP_Number__c='1234567890');
        insert acc;
        
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        ONTAP__OpenItem__c newOpen = new ONTAP__OpenItem__c();
        newOpen.V360_ClvCT__c='Y';
        newOpen.ONTAP__SAPCustomerId__c='1234567890';
        newOpen.RecordTypeId=V360_OpenItemRT;
        insert(newOpen);
    }
    
    /**
    * Test method wich is executed every time is updated
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_beforeUpdate_UseCase1(){
        V360_OpenItemsTriggerHandler obj01 = new V360_OpenItemsTriggerHandler();
        ONTAP__OpenItem__c newOpen = new ONTAP__OpenItem__c();
        newOpen.V360_ClvCT__c='Y';
        insert(newOpen);
        update(newOpen);
    }
    
     /**
    * Test Method for relate openitems to accounts
    * Created By: g.martinez.cabral@accenture.com
    * @param List<ONTAP__OpenItem__c> newlist
    * @return void
    */
    @isTest static void test_relateAccountsToOpenItems_UseCase1(){
        V360_OpenItemsTriggerHandler obj01 = new V360_OpenItemsTriggerHandler();
        List<ONTAP__OpenItem__c> newlist = [SELECT Id, ONTAP__SAPCustomerId__c, RecordTypeId, ONTAP__Account__c FROM ONTAP__OpenItem__c Limit 5];
        
        List<ONTAP__OpenItem__c> validops = PreventExecutionUtil.validateOpenItemWithOutId(newlist);
        Set<Id> setIdsOpen = new Set<Id>();
        
        for(ONTAP__OpenItem__c open : validops){
            if(!setIdsOpen.contains(open.Id) && open.ONTAP__Account__c == null){setIdsOpen.add(open.Id);}
        }
        if(!setIdsOpen.isEmpty()){
        	V360_OpenItemsTriggerHandler.relateAccountsToOpenItems(setIdsOpen);
        }
    }
}