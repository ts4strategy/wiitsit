/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:ItemTestJson.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 17/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
 */

public class ItemTestJson {
    
    /*
     * Structure of the entity which declare its variables with their respective setters and getters.
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
	@AuraEnabled public String sku{set;get;}

    @AuraEnabled public String freegood{set;get;}

    @AuraEnabled public Double price{set;get;}

    @AuraEnabled public Double unitprice{set;get;}

    @AuraEnabled public String unit{set;get;}

    @AuraEnabled public Decimal quantity{set;get;}
    
    @AuraEnabled public Double subtotal{set;get;}

    @AuraEnabled public Decimal discount{set;get;}

    @AuraEnabled public Double tax{set;get;}

    @AuraEnabled public Double total{set;get;}
    
    @AuraEnabled public List<DealsTestJson> appliedDeals{set;get;}
}