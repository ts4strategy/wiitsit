global class TC_DeleteCallsTelecollection implements Database.Batchable<sObject>{
    // Persistent variables

    global Boolean hasErrors {get; private set;}
	/**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global TC_DeleteCallsTelecollection()
    {  
        this.hasErrors = false;
    }
    
    /**
   * Execute the Query for the account open items objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        String typee = 'Telecollection';
        Date todayDate = System.Today();
        if(System.Test.isRunningTest()){
           query += 'SELECT Id FROM ONTAP__OpenItem__c WHERE RecordTypeId=:V360_OpenItemRT';
        }else{
           //Query for delete campaigns of certain Date
           query +=  'SELECT CreatedDate,Id,Name FROM Telecollection_Campaign__c WHERE CreatedDate >= 2019-05-07T00:00:00.000Z' ;
            
           //Query for delete calls of telecollection and certain Date
           //query += 'SELECT CreatedDate,Id,Name FROM ONCALL__Call__c WHERE CreatedDate >= 2019-05-07T00:00:00.000Z AND ONCALL__Call_Type__c =:typee';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Method for delete all the account open items objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<SObject> objlist
   * @return void
   */
    global void execute(Database.BatchableContext d, List<SObject> objlist){
        delete objlist;
        DataBase.emptyRecycleBin(objlist);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}