/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_EventTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class VisitControl_EventTriggerHandler extends TriggerHandlerCustom{

    private Map<Id, Event> newMap;
    private Map<Id, Event> oldMap;
    private List<Event> newList;
    private List<Event> oldList;
     
    public VisitControl_EventTriggerHandler() {
        this.newMap = (Map<Id, Event>) Trigger.newMap;
        this.oldMap = (Map<Id, Event>) Trigger.oldMap;
        this.newList = (List<Event>) Trigger.new;
        this.oldList = (List<Event>) Trigger.old;
    }
    
    /**
    * Method before insert  
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void beforeInsert(){
        retraceEventOneDay(newList);
        anticipateEventOneDay(newList);
    }
   
    /**
    * Method for retrace an event by one day  
    * Created By: g.martinez.cabral@accenture.com
    * @param List<Event> newList
    * @return List<Id>
    */
    public static void retraceEventOneDay( List<Event> newList){
        
        Set<Id> routes = new Set<Id>();
        
        for( Event eveObj: newList){
            if(eveObj.VisitControl_IsRetraced__c != NULL && eveObj.VisitControl_IsRetraced__c == TRUE ){
                routes.add(eveObj.VisitControl_TourRouteId__c );
            }       
        }
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: GlobalStrings.VisitControl_Retrace]; 
        for( Event eveObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){
                
                DateTime startDatetime = eveObj.StartDateTime;
                Date startDate = date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                
                if (eveObj.VisitControl_TourRouteId__c !=NULL && eveObj.VisitControl_TourRouteId__c  == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && 
                    startDate >= obj.VisitControl_DateToSkip__c){
                        
                 	DateTime startDateTimeOriginal = eveObj.StartDateTime;
                	DateTime endDateTimeOriginal = eveObj.EndDateTime;
                                
                	DateTime startDatTimeRetraced = startDateTimeOriginal.addDays(1);
               	 	DateTime endDateTimeRetraced = endDateTimeOriginal.addDays(1);
                
                	eveObj.StartDateTime = startDatTimeRetraced ;
                	eveObj.EndDateTime = endDateTimeRetraced;
                	eveObj.ActivityDateTime = startDatTimeRetraced;                       
                }
            }
        }
    }
    
    /**
    * Method for anticipe an event by one day  
    * Created By: g.martinez.cabral@accenture.com
    * @param List<Event> newList
    * @return List<Id>
    */
    public static void anticipateEventOneDay(List<Event> newList){
        Set<Id> routes = new Set<Id>();
        
        for( Event eveObj: newList){
            if(eveObj.VisitControl_IsRetraced__c != NULL && eveObj.VisitControl_IsRetraced__c == TRUE ){
                routes.add(eveObj.VisitControl_TourRouteId__c );
            }       
        }
        
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: routes AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: GlobalStrings.VisitControl_Anticipate]; 
        for( Event eveObj: newList){
            
            for (VisitControl_RetraceAndAnticipateRoute__c obj:routesToAnticipate){
                
                DateTime startDatetime = eveObj.StartDateTime;
                Date startDate = date.newinstance(StartDatetime.year(), StartDatetime.month(), StartDatetime.day());
                
                
                if (eveObj.VisitControl_TourRouteId__c !=NULL && eveObj.VisitControl_TourRouteId__c  == obj.VisitControl_Route__c && startDate.toStartOfWeek() == obj.VisitControl_DateToSkip__c.toStartOfWeek() && 
                    startDate <= obj.VisitControl_DateToSkip__c){
                        
                 	DateTime startDateTimeOriginal = eveObj.StartDateTime;
                	DateTime endDateTimeOriginal = eveObj.EndDateTime;
                                
                	DateTime startDatTimeRetraced = startDateTimeOriginal-1;
               	 	DateTime endDateTimeRetraced = endDateTimeOriginal-1;
                
                	eveObj.StartDateTime = startDatTimeRetraced ;
                	eveObj.EndDateTime = endDateTimeRetraced;
                	eveObj.ActivityDateTime = startDatTimeRetraced;                       
                }
            }
        }
        
    } 
}