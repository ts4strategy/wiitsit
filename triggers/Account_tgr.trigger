/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Trigger handler for Events related to Account records

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    22/08/2017 Daniel Peñaloza            Trigger created
*******************************************************************************/

trigger Account_tgr on Account  (after delete, after insert, after update, before delete, before insert, before update)  {
        New V360_AccountTriggerHandler().Run();    
   
       // if (Trigger.isAfter) {
       //     if (Trigger.isUpdate) {
       //         // Filter accounts to remove from Visit Plan and Tour
       //         AccountOperations_cls.validateAccountsToRemove(Trigger.new);
       //     }
       // } 
    
}