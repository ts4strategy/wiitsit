trigger ONTAP_ONCALL_Call on ONTAP__ONCALL_Call__c (after insert,after update,before insert,  before update) {
    new ONTAP_ONCALL_CallTriggerHandler().run();
}