/****************************************************************************************************
    General Information
    -------------------
    author: Andrea Cedillo
    email: acedillo@avanxo.com
    company: Avanxo México
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Indicates when an account been assigned to a visit plan, or when it is not included in a plan.

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       05-07-2017        Andrea Cedillo               Creation Class
****************************************************************************************************/
trigger AccountByVisitPlan_tgr on AccountByVisitPlan__c (after delete, after insert, after update, before delete, before insert, before update) {
    if(trigger.isBefore) {
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByVisitPlan_tgr','BeforeInsert')){
            AccountByVisitPlanOperations_cls.validateServiceModelConfig(trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByVisitPlan_tgr','BeforeInsert');
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByVisitPlan_tgr','BeforeUpdate')){
            AccountByVisitPlanOperations_cls.validateServiceModelConfig(trigger.new);            
            TriggerExecutionControl_cls.setAlreadyDone('AccountByVisitPlan_tgr','BeforeUpdate');
        }
        else if(trigger.isDelete){
            AccountByVisitPlanOperations_cls.manageAggregateAccount(trigger.oldMap, false);
        }
    }
    else if(trigger.isAfter){
             
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('AccountByVisitPlan_tgr','AfterInsert')){
            AccountByVisitPlanOperations_cls.manageAggregateAccount(trigger.newMap, true);
            TriggerExecutionControl_cls.setAlreadyDone('AccountByVisitPlan_tgr','AfterInsert');
        }else if(trigger.isUpdate){
			AccountByVisitPlanOperations_cls.manageAggregateAccount(trigger.newMap, true);
            //TriggerExecutionControl_cls.setAlreadyDone('AccountByVisitPlan_tgr','AfterUpdate');
        }
        else if(trigger.isDelete){
            AccountByVisitPlanOperations_cls.deleteVisits(trigger.old);
        }
    }
}