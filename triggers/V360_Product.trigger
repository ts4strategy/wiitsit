/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_Product.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                  				 Description
* 18/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger which executes after delete, after insert, after update, 
														before delete, before insert, before update on the object ONTAP__Product__c
*/


/**
* Constructor which call the V360_ProductTriggerHandler in the following cases: after delete, after insert, after update, before delete, before insert, before update
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger V360_Product on ONTAP__Product__c (after delete, after insert, after update, before delete, before insert, before update) {
	New V360_ProductTriggerHandler().run();
}