/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control ONTAP_Route__c events

    Information about changes (versions)
    -------------------------------------
    Number  Dates            Author                     Description
    ------  --------          -------------------------------------
    1.0     22-06-2017      Andrés Garrido              Creation Class
    1.1     30-05-2018      Alberto Gómez               Added: assignApplicationIdToRoute method.
    1.2     ?               ?                           Added: filterRoutesByChange, setAccountTeam, setBDRAsAccountOwner methods in after update.
    1.3     19-09-2018      Alberto Gómez               Added: updateAutosalesRecordType method in before insert and before update.
    1.4     03-10-2018      Leopoldo Ortega             Se comentó la linea Route_cls.setBDRAsAccountOwner(lstRoutesToProcessWithChanges, Trigger.newMap); como FIX para actualización del supervisor en equipos de cuentas
****************************************************************************************************/
trigger Route_tgr on ONTAP__Route__c (before delete, before insert, before update, after update) {
    new VisitControl_RouteTriggerHandler().run();
    if(Trigger.isBefore) {
        Route_cls IssM = new Route_cls();
        if(Trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('Route_tgr','BeforeInsert')) {
            //Validate that it will be applied only for Presales and Autosales routes.
            ONTAP__Route__c[] lstRoutesToProcess = IssM.filterRoutes(Trigger.new);

            //Update RecordType for Autosales Routes.
            //AllMobileOnTapRouteHelperClass.updateAutosalesRecordType(Trigger.new);

            //Assign AllMobileApplication Id to the route.
            //AllMobileOnTapRouteHelperClass.assignApplicationIdToRoute(Trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('Route_tgr','BeforeInsert');
        } else if(Trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('Route_tgr','BeforeUpdate')) {
            //Validate that it will be applied only for Presales and Autosales routes.
            /////ONTAP__Route__c[] lstRoutesToProcess = IssM.filterRoutes(Trigger.new);

            //Update RecordType for Autosales Routes.
            /////AllMobileOnTapRouteHelperClass.updateAutosalesRecordType(Trigger.new);

            //Assign AllMobileApplication Id to the route.
            /////AllMobileOnTapRouteHelperClass.assignApplicationIdToRoute(Trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('Route_tgr','BeforeUpdate');
        } else if(Trigger.isDelete) {
            IssM.deleteRelatedAccountsByRoute(Trigger.oldMap.keySet());
        }
    } 
    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            
            /*for (ONTAP__Route__c reg : Trigger.new) {
                ONTAP__Route__c regOld = Trigger.oldMap.get(reg.Id);
                if (reg.Supervisor__c != regOld.Supervisor__c) {
                    Route_cls.AssignBDRToAccounts(Trigger.new);
                }
            }*/
            
            //ONTAP__Route__c[] lstRoutesToProcessWithChanges = Route_cls.filterRoutesByChange(Trigger.new, Trigger.oldMap, new String[]{'Supervisor__c'});
            /*if(!lstRoutesToProcessWithChanges.isEmpty()){
                Route_cls.setAccountTeam(lstRoutesToProcessWithChanges, Trigger.oldMap);
                
            }*/
            //ONTAP__Route__c[] lstRoutesToProcess = Route_cls.filterRoutesBDR(Trigger.new);
            //lstRoutesToProcessWithChanges = Route_cls.filterRoutesByChange(lstRoutesToProcess, Trigger.oldMap, new String[]{'Supervisor__c'});
            
            //System.debug('ROUTE - BANDERA DE CONTROL #1: ' + lstRoutesToProcessWithChanges);
            
            //if(!lstRoutesToProcessWithChanges.isEmpty()){
            for (ONTAP__Route__c reg : Trigger.new) {
                ONTAP__Route__c regOld = Trigger.oldMap.get(reg.Id);
                if (reg.Supervisor__c != regOld.Supervisor__c) {
                	Route_cls.setBDRAsAccountOwner(Trigger.new, Trigger.newMap); // lstRoutesToProcessWithChanges
                }
            }
            //}
                
        }
    }
}