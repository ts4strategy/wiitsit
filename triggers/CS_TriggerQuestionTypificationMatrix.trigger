trigger CS_TriggerQuestionTypificationMatrix on CS_Question_Typification_Matrix__c (after delete, after insert, after update, before delete, before insert, before update) {
	new CS_TriggerQuestionTMHandler().run();
}