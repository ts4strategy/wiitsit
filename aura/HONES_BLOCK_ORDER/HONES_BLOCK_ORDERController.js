({
    doInit : function(cmp, event, helper) {
        var recordId = cmp.get("v.recordId");
        helper.serchRelatedOrders(cmp,recordId,event);
    },
    
    handleLoad: function(cmp, event, helper) {
        cmp.set('v.showSpinner', false);
    },

    handleSubmit: function(cmp, event, helper) {
        event.preventDefault();       // stop the form from submitting
        cmp.set('v.disabled', true);
        cmp.set('v.showSpinner', true);
        var recordId = cmp.get("v.recordId");
        helper.sendOrderToBlock(cmp,recordId,event);
        //var isSuccessSAP = cmp.get('v.sendSAPSucces');
        //console.log('handleSubmit::isSuccessSAP::' + isSuccessSAP );
    },

    handleError: function(cmp, event, helper) {
        // errors are handled by lightning:inputField and lightning:nessages
        // so this just hides the spinnet
        cmp.set('v.showSpinner', false);
    },

    handleSuccess: function(cmp, event, helper) {
        cmp.set('v.showSpinner', false);
        cmp.set('v.saved', true);
    }
});