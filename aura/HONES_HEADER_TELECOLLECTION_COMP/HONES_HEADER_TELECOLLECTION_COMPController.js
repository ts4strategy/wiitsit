({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.totalValue(component,recordId);
        helper.pendingValue(component,recordId);
        helper.incompleteValue(component,recordId);
        helper.completeValue(component,recordId);
    }
})