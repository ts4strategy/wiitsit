({
    loadTypification: function (cmp, event, helper)
    {
      helper.getTypificationN1(cmp);
        
      var urlPage = window.location.href;
      var url = new URL(urlPage);
      var idAccount = url.searchParams.get("def_account_id");
      cmp.set("v.idAccount", idAccount);
      
      helper.getDataAccount(cmp, idAccount);
    },
    
    handleChangeTypificationN1: function(cmp, event, helper)
    {
        var TypificationN1 = cmp.find("SelectTypificationN1").get("v.value");
        cmp.set("v.TypificationN1", TypificationN1)
        
        if(typeof TypificationN1 != undefined && TypificationN1)
        {
            helper.getTypificationN2(cmp, TypificationN1)
        }
        
        cmp.set("v.ShowlistN2", 'False');
        cmp.set("v.ShowlistN3", 'False');
        cmp.set("v.ShowlistN4", 'False'); 

        cmp.set("v.TypificationN2","");
        cmp.set("v.TypificationN3","");
        cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN2: function(cmp, event, helper)
    {
       var TypificationN2 = cmp.find("SelectTypificationN2").get("v.value");
       cmp.set("v.TypificationN2", TypificationN2);
       
       var TypificationN1 = cmp.get("v.TypificationN1");
       if(typeof TypificationN2 != undefined && TypificationN2)
       {
         helper.getTypificationN3(cmp, TypificationN1, TypificationN2);
       }
       cmp.set("v.ShowlistN3", 'False');
       cmp.set("v.ShowlistN4", 'False');

       cmp.set("v.TypificationN3","");
       cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN3: function(cmp, event, helper)
    {
       var TypificationN3 = cmp.find("SelectTypificationN3").get("v.value");
       cmp.set("v.TypificationN3", TypificationN3);
       
       var TypificationN1 = cmp.get("v.TypificationN1");
       var TypificationN2 = cmp.get("v.TypificationN2");
       
       if(typeof TypificationN3 != undefined && TypificationN3)
       {
          helper.getTypificationN4(cmp, TypificationN1, TypificationN2, TypificationN3);
       }
       cmp.set("v.ShowlistN4", 'False');
       cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN4: function(cmp, event, helper)
    {
       var TypificationN3 = cmp.find("SelectTypificationN4").get("v.value");
       cmp.set("v.TypificationN4", TypificationN3);
    },
       
    handleClickButtonSiguiente: function(cmp, event, helper)
    {    
        
        cmp.set("v.showSpinner", true);       
        var inputValid = true;
        var TypificacionN1; 
        var TypificacionN2;
        var TypificacionN3;
        var TypificacionN4;
        var DescripcionCaso;
        var mensajesError = [];
    
        var idAccount = cmp.get("v.idAccount");
        $A.get("$Label.c.CS_Mensaje_T1")
        
        /*Validando que se haya seleccionado un valor en el primer nivel de tipificacion*/
        TypificacionN1 = cmp.get("v.TypificationN1");
        if(typeof TypificacionN1 === undefined || TypificacionN1 === '')
        {
            inputValid = false;
            mensajesError.push($A.get("$Label.c.CS_Mensaje_T1"));
        }
        
        /* Validando que se haya seleccionado un valor en el segundo nivel de tipificacion */
        if(cmp.get("v.ShowlistN2") === 'True')
        {
           TypificacionN2  = cmp.find("SelectTypificationN2").get("v.value");
           if(typeof TypificacionN2 === undefined || TypificacionN2 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T2"));
           }
        }
       
        /* Validando que se haya seleccionado un valor en el tercer nivel de tipificacion */
        if(cmp.get("v.ShowlistN3") === 'True')
        {
           TypificacionN3  = cmp.find("SelectTypificationN3").get("v.value");
           if(typeof TypificacionN3 === undefined || TypificacionN3 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T3"));
           }
        }
        
        /* Validando que se haya seleccionado el cuarto nivel de tipificacion */
        if(cmp.get("v.ShowlistN4") === 'True')
        {
           TypificacionN4  = cmp.find("SelectTypificationN4").get("v.value");
           if(typeof TypificacionN4 === undefined || TypificacionN4 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T4"));
           }
        }
        
        /* Validando que se haya capturado la descripcion del caso */
        DescripcionCaso = cmp.find("textareaDescripcionCaso").get("v.value");
        if(DescripcionCaso === undefined || DescripcionCaso === '')
        {
           inputValid = false;
           mensajesError.push($A.get("$Label.c.CS_Mensaje_Descripcion_Caso"));
        }
        else
            cmp.set("v.CaseDetail", DescripcionCaso);
        
        /* Validando que se tenga una cuenta para la generacion del caso */
        if(idAccount == null || idAccount == '')
        {
           inputValid = false;
           mensajesError.push($A.get("$Label.c.CS_Mensaje_Cliente"));
        }

        if(inputValid)
        {
           helper.generateNewCase(cmp, idAccount);
        }
        else
        {
            cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Campos_Obligatorios"));
            cmp.set("v.mensajesValidacion", mensajesError);
            cmp.set("v.showModal", !inputValid); 
            cmp.set("v.showSpinner", false);          
        }
    },
    
    
    handleClickButtonCloseModal: function(cmp, event, helper)
    {
        cmp.set("v.showModal", false);
    }
})