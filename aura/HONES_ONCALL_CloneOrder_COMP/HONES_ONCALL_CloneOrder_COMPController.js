({
    doInit : function(component, event, helper) {
        //component.set('v.renderCallsAndOrders', false);
        /*var recordId = component.get("v.recordId");
        console.log('call and visit');
        component.set("v.Listaid", []); 
    
        helper.getDataDifferentId(component,recordId);
        */
    },
/*
    getCalls : function(component, event, helper) {
        var action = component.get('c.getCalls');
        var accountId = component.get('v.accountId');
        var callId = component.get('v.orderId');
        var orderId = component.get('v.callId');

        if(accountId !== '' && callId !== '' && orderId !== ''){
            action.setParams({
                "accountId": accountId,
                "callId": callId,
                "orderId": orderId  
            });
            
            action.setCallback(this, function(response){
                if(action.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('result: ', result);
                    
                    if(result){
                        var accountValue = component.get('v.dataModifyForm.accountNumber')
                        component.set('v.accountNumber', accountValue);
                        $A.enqueueAction(component.get('c.search'));
                        component.set("v.truthyModifyForm", false);*-/
                        toastSucces.setParams({"message": "Los registros se guardaron correctamente con la siguiente información" + 
                                               " Id: " + result.Id + 
                                               ", Número de cuenta: " + result.AccountNumber + 
                                               ", Nombre: " + result.Name + 
                                               ", Teléfono: " + result.Phone +
                                               ", Email: " + result.Email__c +
                                               ", Codigo postal: " + result.Postal_code__c +
                                               ", Ciudad: " + result.City__c +
                                               ", Estado: " + result.State__c
                                              }).fire();
                        
                        component.set('v.dataId', '');
                    }
                }else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            $A.enqueueAction(component.get('c.save'));
        }
    },
    */

    renderChange : function(component, event, helper){
        var renderCallsAndOrders = component.get('v.renderCallsAndOrders');
        console.log('renderCallsAndOrders value1', renderCallsAndOrders)
        component.set('v.renderCallsAndOrders', true);
        console.log('renderCallsAndOrders value2', renderCallsAndOrders)
    },

    getCallsJS : function(component, event, helper) {

        component.set('v.columnCalls', [
            {label: 'Id', fieldName: 'Id', type: 'text'},
            {label: 'Name', fieldName: 'Name', type: 'text'}
        ]);

        var action = component.get('c.getCalls');
        var accountValue = component.get('v.dataValues.accountId');
        console.log('accountValue: ', accountValue);
        if(accountValue != '' && accountValue != null){
            action.setParams({
                "accountValue": accountValue
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    /*console.log('action.getState(): ', action.getState());
                    console.log('response.getReturnValue(): ', response.getReturnValue());
                    console.log('response: ', response);
                    console.log('result: ', result);*/
                    if(result){
                        component.set('v.dataCalls', result);
                    }
                    
                }else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            console.log('Debes ingresar una cuenta valida');
        }
    },

    getOrdersJS : function(component, event, helper) {

        component.set('v.columnOrders', [
            {label: 'Id', fieldName: 'Id', type: 'text'},
            {label: 'Name', fieldName: 'Name', type: 'text'}
        ]);

        var action = component.get('c.getOrders');
        var callValue = component.get('v.dataValues.callId');
        console.log('callValue: ', callValue);
        if(callValue != '' && callValue != null){
            action.setParams({
                "callValue": callValue
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    /*console.log('action.getState(): ', action.getState());
                    console.log('response.getReturnValue(): ', response.getReturnValue());
                    console.log('response: ', response);
                    console.log('result: ', result);*/
                    if(result){
                        component.set('v.dataOrders', result);
                    }
                    
                }else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            console.log('Debes ingresar una cuenta valida');
        }
    },

    getItemsJS : function(component, event, helper) {

        component.set('v.columnItems', [
            {label: 'Id', fieldName: 'Id', type: 'text'},
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Description', fieldName: 'ONTAP__Product_Description__c', type: 'text'},
            {label: 'Quantity', fieldName: 'ONTAP__ActualQuantity__c', type: 'text'},
            {label: 'Product Id', fieldName: 'ONTAP__ProductId__c', type: 'text'}
        ]);

        var action = component.get('c.getCalls');
        var orderValue = component.get('v.dataValues.orderId');
        console.log('orderValue: ', orderValue);
        if(orderValue != '' && orderValue != null){
            action.setParams({
                "orderValue": orderValue
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    /*console.log('action.getState(): ', action.getState());
                    console.log('response.getReturnValue(): ', response.getReturnValue());
                    console.log('response: ', response);
                    console.log('result: ', result);*/
                    if(result){
                        component.set('v.dataItems', result);
                    }
                    
                }else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            console.log('Debes ingresar una cuenta valida');
        }
    },
    
})