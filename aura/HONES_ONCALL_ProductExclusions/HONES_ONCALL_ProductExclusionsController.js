({
    init: function (component, event, helper) {
        component.set("v.isLoading", true);
        helper.getPickListSubchannelRequest(component);
        helper.getPickListDistributionCenterRequest(component);
        helper.getPickListKATR3Request(component);
        
        component.set("v.localidadSeleccionada", "--Todos--");
        component.set("v.subcanalSeleccionado", "--Todos--");
        component.set("v.attr3Seleccionado", "--Todos--");
    },
    
    /***** Modified
    EnableLevelsSelection:function(component, event, helper) {
        var localidad = component.get('v.localidadSeleccionada');
        var subcanal = component.get('v.subcanalSeleccionado');
        console.log('bool: ' + component.get('v.localidadBool'));
        
        if(localidad != "--"+$A.get("$Label.c.None")+"--"){ 
            component.set('v.localidadBool', true); 
            if(subcanal != "--"+$A.get("$Label.c.None")+"--"){  
                component.set('v.subcanalBool', true );
            }else{ 
                component.set('v.subcanalBool', false); 
                component.set("v.attr3Seleccionado", "--"+$A.get("$Label.c.None")+"--");
            }
        }else{ 
            component.set('v.localidadBool', false);
            component.set('v.subcanalBool', false); 
            component.set("v.subcanalSeleccionado", "--"+$A.get("$Label.c.None")+"--");
        	component.set("v.attr3Seleccionado", "--"+$A.get("$Label.c.None")+"--");
        }
        
    }, 
    */
    resetBusquedaMasiva:function(component, event, helper) {
        component.set('v.busquedaMasivaBool',false);
    }, 
    
    
    showClient:function(component, event, helper) {
        var len_Id_SAP = component.get('v.Id_SAP_Cliente').length;
        if(len_Id_SAP == 10){ 
            helper.getClient(component);
        }else{ 
            component.set('v.Id_SAP_Bool',false); 
            component.set("v.showErrors",false);
        }
    }, 
    
    buscarClientes:function(component, event, helper) {
        var localidadSeleccionada = component.get('v.localidadSeleccionada');
        var subcanalSeleccionado = component.get('v.subcanalSeleccionado');
        var attr3Seleccionado = component.get('v.attr3Seleccionado');
        component.set("v.isLoading", true);
        component.set('v.busquedaMasivaBool',false);
        var searchCriteria  = localidadSeleccionada + "|" + subcanalSeleccionado + "|" + attr3Seleccionado;
        component.set("v.searchCriteria",searchCriteria);
        helper.getMassiveClients(component);
    }
    
});