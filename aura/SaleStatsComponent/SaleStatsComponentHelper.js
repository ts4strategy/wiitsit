({
     
    getDataSaleSatus : function(cmp,recordId) {
        var action = cmp.get("c.getStats");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var tablesList = response.getReturnValue();
                cmp.set("v.listOfTables", tablesList);               
               // cmp.set("v.showBool1", true);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }
    
})