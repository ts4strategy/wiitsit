({
    loadTypification: function (cmp, event, helper)
    {
        helper.getPickListValues(cmp);   
        helper.getTypificationN1(cmp);
    },
    
    onChange:  function(cmp, event, helper) 
    {
        var qType = cmp.find("selectQuestionType").get("v.value");
        var options = cmp.find("textAreaOptions")
        cmp.set("v.selectedValue", qType);
        
        if(qType === $A.get("$Label.c.CS_Seleccion_Unica"))
            cmp.set("v.showOptions", "True");
        else
        {
            cmp.set("v.showOptions", "False");
            if(options != undefined && options != null)
                options.set("v.value", "");
        }     
    },
    
    handleChangeTypificationN1: function(cmp, event, helper)
    {
        var TypificationN1 = cmp.find("SelectTypificationN1").get("v.value");
        cmp.set("v.TypificationN1", TypificationN1)
        
        if(typeof TypificationN1 != undefined && TypificationN1)
        {
            helper.getTypificationN2(cmp, TypificationN1)
        }
        
        cmp.set("v.ShowlistN2", 'False');
        cmp.set("v.ShowlistN3", 'False');
        cmp.set("v.ShowlistN4", 'False'); 
        
        cmp.set("v.TypificationN2","");
        cmp.set("v.TypificationN3","");
        cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN2: function(cmp, event, helper)
    {
        var TypificationN2 = cmp.find("SelectTypificationN2").get("v.value");
        cmp.set("v.TypificationN2", TypificationN2);
        
        var TypificationN1 = cmp.get("v.TypificationN1");
        if(typeof TypificationN2 != undefined && TypificationN2)
        {
            helper.getTypificationN3(cmp, TypificationN1, TypificationN2);
        }
        cmp.set("v.ShowlistN3", 'False');
        cmp.set("v.ShowlistN4", 'False');
        
        cmp.set("v.TypificationN3","");
        cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN3: function(cmp, event, helper)
    {
        var TypificationN3 = cmp.find("SelectTypificationN3").get("v.value");
        cmp.set("v.TypificationN3", TypificationN3);
        
        var TypificationN1 = cmp.get("v.TypificationN1");
        var TypificationN2 = cmp.get("v.TypificationN2");
        
        if(typeof TypificationN3 != undefined && TypificationN3)
        {
            helper.getTypificationN4(cmp, TypificationN1, TypificationN2, TypificationN3);
        }
        cmp.set("v.ShowlistN4", 'False');
        cmp.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN4: function(cmp, event, helper)
    {
        var TypificationN3 = cmp.find("SelectTypificationN4").get("v.value");
        cmp.set("v.TypificationN4", TypificationN3);
    },
    
    onSave: function(cmp,event,helper)
    {
        cmp.set("v.showSpinner", true); 
        var errors = [];
        var TypificacionN1; 
        var TypificacionN2;
        var TypificacionN3;
        var TypificacionN4;
        var Question;
        var qType;
        var qOptions;         
        var requiredFields = true; 
        
        TypificacionN1 = cmp.get("v.TypificationN1");
        if(TypificacionN1 === '' || typeof TypificacionN1 === undefined)
        {
            requiredFields = false;
            errors.push($A.get("$Label.c.CS_Mensaje_T1"));
        }
        
        if(cmp.get("v.ShowlistN2") === 'True')
        {
            TypificacionN2  = cmp.find("SelectTypificationN2").get("v.value");
            if(TypificacionN2 === '' || typeof TypificacionN2 === undefined)
            {
                requiredFields = false;
                errors.push($A.get("$Label.c.CS_Mensaje_T2"));
            }
        }
        
        if(cmp.get("v.ShowlistN3") === 'True')
        {
            TypificacionN3  = cmp.find("SelectTypificationN3").get("v.value");
            if(TypificacionN3 === '' || typeof TypificacionN3 === undefined)
            {
                requiredFields = false;
                errors.push($A.get("$Label.c.CS_Mensaje_T3"));
            }
        }
        
        if(cmp.get("v.ShowlistN4") === "True")
        {
            TypificacionN4  = cmp.find("SelectTypificationN4").get("v.value");
            if(TypificacionN4 === '' || typeof TypificacionN4 === undefined)
            {
                requiredFields = false;
                errors.push($A.get("$Label.c.CS_Mensaje_T4"));
            }
        }
        
        Question = cmp.find("textQuestion").get("v.value");
        if(Question ==='' || typeof Question === undefined)
        {
            requiredFields = false;
            errors.push($A.get("$Label.c.CS_Error_Pregunta"));			            
        }
        
        qType = cmp.get("v.selectedValue");
        if(qType ==='' || typeof qType === undefined)
        {
            requiredFields = false;
            errors.push($A.get("$Label.c.CS_Error_Tipo_Pregunta"));			            
        }
        
        if(cmp.get("v.showOptions") === "True")
        {
            qOptions = cmp.find("textAreaOptions").get("v.value");
            if(qOptions === undefined || qOptions === '') 
            {
                requiredFields = false;
                errors.push($A.get("$Label.c.CS_Error_Opciones"));
            }
        }
        
        if(requiredFields)
            helper.createQuestion(cmp,TypificacionN1, TypificacionN2, TypificacionN3, TypificacionN4, Question, qType,qOptions);
        else
        {
            cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Campos_Obligatorios"));
            cmp.set("v.mensajesValidacion", errors);
            cmp.set("v.showModal", true); 
            cmp.set("v.showSpinner", false);          
        }
        
    },
    
    handleClickButtonCloseModal: function(cmp, event, helper)
    {
        cmp.set("v.showModal", false);
    }
})