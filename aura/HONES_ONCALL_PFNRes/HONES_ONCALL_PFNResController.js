({
     doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //helper.getpicklist(component,recordId);
    },
    
    selectRecord : function(component, event, helper){      
        // get the selected record from list selectRecord 
        var getSelectRecord = component.get("v.oRecord");
        console.log(getSelectRecord);
        getSelectRecord.quantity = component.get("v.quantity");
        console.log('Entro' + getSelectRecord.quantity);

        if (getSelectRecord.quantity == null){
            
            alert($A.get("$Label.c.You_must_enter_an_amount"));
            
        }else
        { 
            component.get("v.oRecord.listaPFN").forEach( function(item){
                if (item == component.get("v.selectedValue")){
					getSelectRecord.productRecord=component.get("v.selectedValue");
                    console.log('Entro' + getSelectRecord.productRecord);
                  	getSelectRecord.measureCode = component.get("v.selectedValueUnits");
                      console.log('Entro' + getSelectRecord.measureCode);
                      var compEvent = component.getEvent("oSelectedRecordEvent");
                        getSelectRecord.selectedByUser=true;
                    compEvent.setParams({"recordByEvent" : getSelectRecord});  
             		 // fire the event  
             		compEvent.fire();
                    component.find("inputbar").set("v.value", "");  
                }});
        }
        
    },
            
            clear :function(component,event,heplper){
            
            var pillTarget = component.find("lookup-pill");
            var lookUpTarget = component.find("lookupField");  
            
            $A.util.addClass(pillTarget, 'slds-hide');
            $A.util.removeClass(pillTarget, 'slds-show');
            
            $A.util.addClass(lookUpTarget, 'slds-show');
            $A.util.removeClass(lookUpTarget, 'slds-hide');
            component.set("v.SearchKeyWord",null);
            component.set("v.listOfSearchRecords", null );
            component.set("v.selectedRecord", {} );
            component.set("v.search", "slds-input__icon slds-show");
            
            
            }
            })