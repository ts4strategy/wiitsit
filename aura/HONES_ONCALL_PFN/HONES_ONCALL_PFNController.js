({
    doInit: function (component, event, helper) {
        var recordId = component.get("v.idcall");
        helper.gettheID(component,recordId);
        helper.gettheDate(component,recordId);
        
        var recordId2 = component.get("v.recordId");
        helper.getDataAccountDis(component,recordId2);
        helper.searchHelperPFN(component, event, '');
        
    },
    handleContactSearchComplete : function(component, event, helper) {
        var opt = [];
        opt.push(event.getParam("pickval"));  
        component.set("v.selected", opt);
        console.log("1"+event.getParam("pickval"));
        
        
        
    }, pickNameChanged : function(component, event, helper) {
        var newValue = event.getParam("value");
        var oldValue = event.getParam("oldValue");
        component.set("v.newvalue",newValue);
        alert("Expense name changed from '" + oldValue + "' to '" + newValue + "'");
    },
    
    focus: function (component, event, helper) {
        var forOpen = component.find("lookup");
        forOpen.focus();
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
        //helper.searchHelper(component, event, getInputkeyWord);
    },
    
    onfocusPFN: function (component, event, helper) {
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
        //helper.searchHelper(component, event, getInputkeyWord);
    },
    
    keyPressControllerPFN: function (component, event, helper) {
        // get the search Input keyword   
        var typingTimerPFN = component.get("v.typingTimerPFN");    
        //console.log('typingTimer', typingTimer);
        if(typingTimerPFN){clearTimeout(typingTimerPFN);}
        var getInputkeyWordPFN = component.get("v.SearchKeyWordPFN");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if (getInputkeyWordPFN.length > 0) {
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            //helper.searchHelper(component, event, getInputkeyWordPFN);
            typingTimerPFN = setTimeout(
                $A.getCallback(function() {
        			//helper.searchLocalHelperPFN(component, event, getInputkeyWordPFN)
        			helper.searchHelperPFN(component, event, getInputkeyWordPFN);
                }), 300);
            component.set("v.typingTimerPFN", typingTimerPFN);
        } else {
            component.set("v.listOfSearchRecords", null);
            //helper.searchLocalHelperPFN(component, event, getInputkeyWordPFN);
            helper.searchHelperPFN(component, event, getInputkeyWordPFN);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
        
        
    },
    
    // function for clear the Record Selaction 
    clear: function (component, event, helper) {
        
        component.set("v.SearchKeyWordPFN", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", {});
        component.set("v.search", "slds-input__icon slds-show");
        
    }, toggleOptionalTab: function (cmp) {
        cmp.set('v.isPFNVisible', !cmp.get('v.isPFNVisible'));
    },
    toggleOptionalTab2: function (cmp) {
        cmp.set('v.isPBEVisible', !cmp.get('v.isPBEVisible'));
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent: function (component, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        var element = '';
        var validation = true;
        var newElem = true;
        var listSelectedItems  = component.get("v.lookName");
        console.log(selectedAccountGetFromEvent);
        if(listSelectedItems.length>0){ 
            component.get("v.lookName").forEach(function(element) { 
                if(element.productCode ==selectedAccountGetFromEvent.productCode && element.productRecord == selectedAccountGetFromEvent.productRecord && element.measureCode == selectedAccountGetFromEvent.measureCode)
                {
                     newElem=false;
                    
                    alert($A.get("$Label.c.You_have_already_added_this_product"));   
                
                }
         
            });  
        }
        else{
            
            listSelectedItems.push(selectedAccountGetFromEvent);
            component.set("v.lookName",listSelectedItems );  
            component.set("v.showtabOI",true);
            newElem=false;
            component.set("v.showtabOI",true);

        }   
        
        if(newElem== true  && listSelectedItems.length>0){
            
            listSelectedItems.push(selectedAccountGetFromEvent);
            component.set("v.lookName",listSelectedItems );  	
            component.set("v.showtabOI",true);
            
        }
        
    },
    
    //  component.set("v.selectedRecord", selectedAccountGetFromEvent);
    //component.set("v.showtabOI", true);       
    
    
    
    selectRecord: function (component, event, helper) {
        // get the selected record from list  
        var getSelectRecord = component.get("v.oRecord");
        // call the event   
        var compEvent = component.getEvent("oSelectedRecordEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({
            "recordByEvent": getSelectRecord
        });
        // fire the event  
        compEvent.fire();
    },
    
    
    
    inlineEditName: function (component, event, helper) {
        // show the name edit field popup 
        component.set("v.nameEditMode", true);
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("inputId");
        }, 100);
        
        
    },
    
    inlineEditRating: function (component, event, helper) {
        // show the rating edit field popup 
        component.set("v.ratingEditMode", true);
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("accRating").set("v.options", component.get("v.ratingPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("accRating");
        }, 100);
    },
    
    onNameChange: function (component, event, helper) {
        // if edit field value changed and field not equal to blank,
        // then show save and cancel button by set attribute to true
        if (String(event.getSource().get("v.value")).trim() != '') {
            component.set("v.showSaveCancelBtn", true);
        }
    },
    
    onRatingChange: function (component, event, helper) {
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn", true);
    },
    
    closeNameBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'nameEditMode' att. as false   
        component.set("v.nameEditMode", false);
        // check if change/update Name field is blank, then add error class to column -
        // by setting the 'showErrorClass' att. as True , else remove error class by setting it False   
        if (String(event.getSource().get("v.value")).trim() == '') {
            component.set("v.showErrorClass", true);
        } else {
            component.set("v.showErrorClass", false);
        }
    },
    
    closeRatingBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.ratingEditMode", false);
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "Opisen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen", false);
    },
    
    
    deleteProd: function (component, event, helper) {
        
        var foralln = component.get("v.lookName");
        var tempList = [];            
        var i = 0;            
        var index = event.target.dataset.index;           
        
        
        for (i = 0; i < foralln.length; i++) {
            if(i!=index) {
                tempList.push(foralln[i]);  
            }
        }	
        
        if( tempList.length==0){
            component.set("v.showtabOI", false);
        }
        component.set("v.lookName",tempList);
    },
    deleteProm: function (component, event, helper) {
        
        var foralln = component.get("v.promos");
        var tempList = [];            
        var i = 0;            
        var index = event.target.dataset.index;        
        
        for (i = 0; i < foralln.length; i++) {
            if(i!=index) {
                tempList.push(foralln[i]);  
            }
        }	
        
        if( tempList.length==0){
            component.set("v.tablePromotion", false);
        }
        component.set("v.promos",tempList);
    }   ,
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel2: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen2", false);
    },
    
    likenClose2: function(component, event, helper) {
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen2", false);
    },   
    
    closeModel11: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel1", false);
        component.set("v.isOpen", true);
        
    },
    closeModel22: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel2", false);
        component.set("v.isOpen", true);
    },
    
    openModel1: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel1", true);
        component.set("v.isOpen", false);
    },
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel2", true);
        component.set("v.isOpen", false);
    },
    
    
    sendOrder: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        //var product = component.get("v.lookName");
        //var oncall = component.get("v.Idcuenta");
        component.set("v.isOpen", false);
        var recordId = component.get("v.recordId");
        helper.getResponse(component,recordId,event);
        //$A.get('e.force:refreshView').fire();
    },
    
    changeButton: function(component, event, helper,document) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.hiddenButtonCalculate", false);
        component.set("v.hiddenButtonGenerateOrder", true);
        var recordId = component.get("v.recordId");
        helper.getprice(component,recordId);
        
        
        
    }
    
    
    
})